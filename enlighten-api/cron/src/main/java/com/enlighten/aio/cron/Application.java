package com.enlighten.aio.cron;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

/**
 * <p>
 * 应用启动器
 * </p>
 *  ┏┓  ┏┓
 *┏━┛┻━━┛┻━━┓
 *┃         ┃
 *┃    ━    ┃
 *┃ ┳┛   ┗┳ ┃
 *┃         ┃
 *┃    ┻    ┃
 *┃         ┃
 *┗━┓    ┏━━┛
 *  ┃    ┃神兽保佑
 *  ┃    ┃代码无BUG！
 *  ┃    ┗━━━┓
 *  ┃        ┣┓
 *  ┃       ┏┛
 *  ┗┓┓┏━┳┓┏┛
 *   ┃┫┫ ┃┫┫
 *   ┗┻┛ ┗┻┛
 *
 * @author Henry
 * @since 2018-04-09
 */
@SpringBootApplication
@EnableTransactionManagement
@EnableCaching
@MapperScan({"com.enlighten.aio.db.mapper"})
@ComponentScan(value = { "com.enlighten.aio", "com.x" })
public class Application {
	static Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver res = new CommonsMultipartResolver();
		res.setMaxUploadSize(5242880);// =5 M
		res.setMaxInMemorySize(10240);// =10 K
		return res;
	}
	
}