package com.enlighten.aio.db.model.out;

public class InspectOut {
		private Long id;

	    private String surname;

	    private String nationality;

	    private String jiguan;

	    private Long userId;

	    private String dys19;

	    private String dys359;

	    private String dys390;

	    private String dys391;

	    private String dys392;

	    private String dys393;

	    private String dys437;

	    private String dys635;

	    private String dys590;

	    private String tsStrDys19;

	    private String tsStrDys359;

	    private String tsStrDys390;

	    private String tsStrDys391;

	    private String tsStrDys392;

	    private String tsStrDys393;

	    private String tsStrDys437;

	    private String tsStrDys635;

	    private String tsStrDys590A;

	    private String tsStrDys590B;

	    private String tsStrDys590C;

	    private String tsStrDys590D;

	    private String tsStrDys590E;

	    private String txStrDys19;

	    private String txStrDys359;

	    private String txStrDys390;

	    private String txStrDys391;

	    private String txStrDys392;

	    private String txStrDys393;

	    private String txStrDys437;

	    private String txStrDys635;

	    private String txStrDys590;

	    private String otherDys19;

	    private String otherDys359;

	    private String otherDys390;

	    private String otherDys391;

	    private String otherDys392;

	    private String otherDys393;

	    private String otherDys437;

	    private String otherDys635;

	    private String otherDys590;
	    
	    private String nickname;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getSurname() {
			return surname;
		}

		public void setSurname(String surname) {
			this.surname = surname;
		}

		public String getNationality() {
			return nationality;
		}

		public void setNationality(String nationality) {
			this.nationality = nationality;
		}

		public String getJiguan() {
			return jiguan;
		}

		public void setJiguan(String jiguan) {
			this.jiguan = jiguan;
		}

		public Long getUserId() {
			return userId;
		}

		public void setUserId(Long userId) {
			this.userId = userId;
		}

		public String getDys19() {
			return dys19;
		}

		public void setDys19(String dys19) {
			this.dys19 = dys19;
		}

		public String getDys359() {
			return dys359;
		}

		public void setDys359(String dys359) {
			this.dys359 = dys359;
		}

		public String getDys390() {
			return dys390;
		}

		public void setDys390(String dys390) {
			this.dys390 = dys390;
		}

		public String getDys391() {
			return dys391;
		}

		public void setDys391(String dys391) {
			this.dys391 = dys391;
		}

		public String getDys392() {
			return dys392;
		}

		public void setDys392(String dys392) {
			this.dys392 = dys392;
		}

		public String getDys393() {
			return dys393;
		}

		public void setDys393(String dys393) {
			this.dys393 = dys393;
		}

		public String getDys437() {
			return dys437;
		}

		public void setDys437(String dys437) {
			this.dys437 = dys437;
		}

		public String getDys635() {
			return dys635;
		}

		public void setDys635(String dys635) {
			this.dys635 = dys635;
		}

		public String getDys590() {
			return dys590;
		}

		public void setDys590(String dys590) {
			this.dys590 = dys590;
		}

		public String getTsStrDys19() {
			return tsStrDys19;
		}

		public void setTsStrDys19(String tsStrDys19) {
			this.tsStrDys19 = tsStrDys19;
		}

		public String getTsStrDys359() {
			return tsStrDys359;
		}

		public void setTsStrDys359(String tsStrDys359) {
			this.tsStrDys359 = tsStrDys359;
		}

		public String getTsStrDys390() {
			return tsStrDys390;
		}

		public void setTsStrDys390(String tsStrDys390) {
			this.tsStrDys390 = tsStrDys390;
		}

		public String getTsStrDys391() {
			return tsStrDys391;
		}

		public void setTsStrDys391(String tsStrDys391) {
			this.tsStrDys391 = tsStrDys391;
		}

		public String getTsStrDys392() {
			return tsStrDys392;
		}

		public void setTsStrDys392(String tsStrDys392) {
			this.tsStrDys392 = tsStrDys392;
		}

		public String getTsStrDys393() {
			return tsStrDys393;
		}

		public void setTsStrDys393(String tsStrDys393) {
			this.tsStrDys393 = tsStrDys393;
		}

		public String getTsStrDys437() {
			return tsStrDys437;
		}

		public void setTsStrDys437(String tsStrDys437) {
			this.tsStrDys437 = tsStrDys437;
		}

		public String getTsStrDys635() {
			return tsStrDys635;
		}

		public void setTsStrDys635(String tsStrDys635) {
			this.tsStrDys635 = tsStrDys635;
		}

		public String getTsStrDys590A() {
			return tsStrDys590A;
		}

		public void setTsStrDys590A(String tsStrDys590A) {
			this.tsStrDys590A = tsStrDys590A;
		}

		public String getTsStrDys590B() {
			return tsStrDys590B;
		}

		public void setTsStrDys590B(String tsStrDys590B) {
			this.tsStrDys590B = tsStrDys590B;
		}

		public String getTsStrDys590C() {
			return tsStrDys590C;
		}

		public void setTsStrDys590C(String tsStrDys590C) {
			this.tsStrDys590C = tsStrDys590C;
		}

		public String getTsStrDys590D() {
			return tsStrDys590D;
		}

		public void setTsStrDys590D(String tsStrDys590D) {
			this.tsStrDys590D = tsStrDys590D;
		}

		public String getTsStrDys590E() {
			return tsStrDys590E;
		}

		public void setTsStrDys590E(String tsStrDys590E) {
			this.tsStrDys590E = tsStrDys590E;
		}

		public String getTxStrDys19() {
			return txStrDys19;
		}

		public void setTxStrDys19(String txStrDys19) {
			this.txStrDys19 = txStrDys19;
		}

		public String getTxStrDys359() {
			return txStrDys359;
		}

		public void setTxStrDys359(String txStrDys359) {
			this.txStrDys359 = txStrDys359;
		}

		public String getTxStrDys390() {
			return txStrDys390;
		}

		public void setTxStrDys390(String txStrDys390) {
			this.txStrDys390 = txStrDys390;
		}

		public String getTxStrDys391() {
			return txStrDys391;
		}

		public void setTxStrDys391(String txStrDys391) {
			this.txStrDys391 = txStrDys391;
		}

		public String getTxStrDys392() {
			return txStrDys392;
		}

		public void setTxStrDys392(String txStrDys392) {
			this.txStrDys392 = txStrDys392;
		}

		public String getTxStrDys393() {
			return txStrDys393;
		}

		public void setTxStrDys393(String txStrDys393) {
			this.txStrDys393 = txStrDys393;
		}

		public String getTxStrDys437() {
			return txStrDys437;
		}

		public void setTxStrDys437(String txStrDys437) {
			this.txStrDys437 = txStrDys437;
		}

		public String getTxStrDys635() {
			return txStrDys635;
		}

		public void setTxStrDys635(String txStrDys635) {
			this.txStrDys635 = txStrDys635;
		}

		public String getTxStrDys590() {
			return txStrDys590;
		}

		public void setTxStrDys590(String txStrDys590) {
			this.txStrDys590 = txStrDys590;
		}

		public String getOtherDys19() {
			return otherDys19;
		}

		public void setOtherDys19(String otherDys19) {
			this.otherDys19 = otherDys19;
		}

		public String getOtherDys359() {
			return otherDys359;
		}

		public void setOtherDys359(String otherDys359) {
			this.otherDys359 = otherDys359;
		}

		public String getOtherDys390() {
			return otherDys390;
		}

		public void setOtherDys390(String otherDys390) {
			this.otherDys390 = otherDys390;
		}

		public String getOtherDys391() {
			return otherDys391;
		}

		public void setOtherDys391(String otherDys391) {
			this.otherDys391 = otherDys391;
		}

		public String getOtherDys392() {
			return otherDys392;
		}

		public void setOtherDys392(String otherDys392) {
			this.otherDys392 = otherDys392;
		}

		public String getOtherDys393() {
			return otherDys393;
		}

		public void setOtherDys393(String otherDys393) {
			this.otherDys393 = otherDys393;
		}

		public String getOtherDys437() {
			return otherDys437;
		}

		public void setOtherDys437(String otherDys437) {
			this.otherDys437 = otherDys437;
		}

		public String getOtherDys635() {
			return otherDys635;
		}

		public void setOtherDys635(String otherDys635) {
			this.otherDys635 = otherDys635;
		}

		public String getOtherDys590() {
			return otherDys590;
		}

		public void setOtherDys590(String otherDys590) {
			this.otherDys590 = otherDys590;
		}

		public String getNickname() {
			return nickname;
		}

		public void setNickname(String nickname) {
			this.nickname = nickname;
		}
	    
	    
}
