package com.enlighten.aio.db.mapper;

import com.enlighten.aio.db.model.Group;
import com.enlighten.aio.db.model.GroupExample;
import com.enlighten.aio.db.model.GroupWithBLOBs;
import com.enlighten.aio.db.model.out.GroupOut;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface GroupMapper {
    long countByExample(GroupExample example);

    int deleteByExample(GroupExample example);

    int deleteByPrimaryKey(Long id);

    int insert(GroupWithBLOBs record);

    int insertSelective(GroupWithBLOBs record);

    List<GroupWithBLOBs> selectByExampleWithBLOBsWithRowbounds(GroupExample example, RowBounds rowBounds);

    List<GroupWithBLOBs> selectByExampleWithBLOBs(GroupExample example);

    List<Group> selectByExampleWithRowbounds(GroupExample example, RowBounds rowBounds);

    List<Group> selectByExample(GroupExample example);

    GroupWithBLOBs selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") GroupWithBLOBs record, @Param("example") GroupExample example);

    int updateByExampleWithBLOBs(@Param("record") GroupWithBLOBs record, @Param("example") GroupExample example);

    int updateByExample(@Param("record") Group record, @Param("example") GroupExample example);

    int updateByPrimaryKeySelective(GroupWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(GroupWithBLOBs record);

    int updateByPrimaryKey(Group record);

	GroupOut getGroupById(Long id);
}