package com.enlighten.aio.db.model;

public class CommentWithBLOBs extends Comment {
    private String content;

    private String data;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data == null ? null : data.trim();
    }
}