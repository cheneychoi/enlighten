package com.enlighten.aio.db.model;

import com.x.share.db.model.ModelWithBLOBs;

public class Inspect extends ModelWithBLOBs {
    private Long id;

    private String surname;

    private String nationality;

    private String jiguan;

    private Long userId;

    private String dys19;

    private String dys359;

    private String dys390;

    private String dys391;

    private String dys392;

    private String dys393;

    private String dys437;

    private String dys635;

    private String dys590;

    private String tsStrDys19;

    private String tsStrDys359;

    private String tsStrDys390;

    private String tsStrDys391;

    private String tsStrDys392;

    private String tsStrDys393;

    private String tsStrDys437;

    private String tsStrDys635;

    private String tsStrDys590A;

    private String tsStrDys590B;

    private String tsStrDys590C;

    private String tsStrDys590D;

    private String tsStrDys590E;

    private String txStrDys19;

    private String txStrDys359;

    private String txStrDys390;

    private String txStrDys391;

    private String txStrDys392;

    private String txStrDys393;

    private String txStrDys437;

    private String txStrDys635;

    private String txStrDys590;

    private String otherDys19;

    private String otherDys359;

    private String otherDys390;

    private String otherDys391;

    private String otherDys392;

    private String otherDys393;

    private String otherDys437;

    private String otherDys635;

    private String otherDys590;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname == null ? null : surname.trim();
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality == null ? null : nationality.trim();
    }

    public String getJiguan() {
        return jiguan;
    }

    public void setJiguan(String jiguan) {
        this.jiguan = jiguan == null ? null : jiguan.trim();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getDys19() {
        return dys19;
    }

    public void setDys19(String dys19) {
        this.dys19 = dys19 == null ? null : dys19.trim();
    }

    public String getDys359() {
        return dys359;
    }

    public void setDys359(String dys359) {
        this.dys359 = dys359 == null ? null : dys359.trim();
    }

    public String getDys390() {
        return dys390;
    }

    public void setDys390(String dys390) {
        this.dys390 = dys390 == null ? null : dys390.trim();
    }

    public String getDys391() {
        return dys391;
    }

    public void setDys391(String dys391) {
        this.dys391 = dys391 == null ? null : dys391.trim();
    }

    public String getDys392() {
        return dys392;
    }

    public void setDys392(String dys392) {
        this.dys392 = dys392 == null ? null : dys392.trim();
    }

    public String getDys393() {
        return dys393;
    }

    public void setDys393(String dys393) {
        this.dys393 = dys393 == null ? null : dys393.trim();
    }

    public String getDys437() {
        return dys437;
    }

    public void setDys437(String dys437) {
        this.dys437 = dys437 == null ? null : dys437.trim();
    }

    public String getDys635() {
        return dys635;
    }

    public void setDys635(String dys635) {
        this.dys635 = dys635 == null ? null : dys635.trim();
    }

    public String getDys590() {
        return dys590;
    }

    public void setDys590(String dys590) {
        this.dys590 = dys590 == null ? null : dys590.trim();
    }

    public String getTsStrDys19() {
        return tsStrDys19;
    }

    public void setTsStrDys19(String tsStrDys19) {
        this.tsStrDys19 = tsStrDys19 == null ? null : tsStrDys19.trim();
    }

    public String getTsStrDys359() {
        return tsStrDys359;
    }

    public void setTsStrDys359(String tsStrDys359) {
        this.tsStrDys359 = tsStrDys359 == null ? null : tsStrDys359.trim();
    }

    public String getTsStrDys390() {
        return tsStrDys390;
    }

    public void setTsStrDys390(String tsStrDys390) {
        this.tsStrDys390 = tsStrDys390 == null ? null : tsStrDys390.trim();
    }

    public String getTsStrDys391() {
        return tsStrDys391;
    }

    public void setTsStrDys391(String tsStrDys391) {
        this.tsStrDys391 = tsStrDys391 == null ? null : tsStrDys391.trim();
    }

    public String getTsStrDys392() {
        return tsStrDys392;
    }

    public void setTsStrDys392(String tsStrDys392) {
        this.tsStrDys392 = tsStrDys392 == null ? null : tsStrDys392.trim();
    }

    public String getTsStrDys393() {
        return tsStrDys393;
    }

    public void setTsStrDys393(String tsStrDys393) {
        this.tsStrDys393 = tsStrDys393 == null ? null : tsStrDys393.trim();
    }

    public String getTsStrDys437() {
        return tsStrDys437;
    }

    public void setTsStrDys437(String tsStrDys437) {
        this.tsStrDys437 = tsStrDys437 == null ? null : tsStrDys437.trim();
    }

    public String getTsStrDys635() {
        return tsStrDys635;
    }

    public void setTsStrDys635(String tsStrDys635) {
        this.tsStrDys635 = tsStrDys635 == null ? null : tsStrDys635.trim();
    }

    public String getTsStrDys590A() {
        return tsStrDys590A;
    }

    public void setTsStrDys590A(String tsStrDys590A) {
        this.tsStrDys590A = tsStrDys590A == null ? null : tsStrDys590A.trim();
    }

    public String getTsStrDys590B() {
        return tsStrDys590B;
    }

    public void setTsStrDys590B(String tsStrDys590B) {
        this.tsStrDys590B = tsStrDys590B == null ? null : tsStrDys590B.trim();
    }

    public String getTsStrDys590C() {
        return tsStrDys590C;
    }

    public void setTsStrDys590C(String tsStrDys590C) {
        this.tsStrDys590C = tsStrDys590C == null ? null : tsStrDys590C.trim();
    }

    public String getTsStrDys590D() {
        return tsStrDys590D;
    }

    public void setTsStrDys590D(String tsStrDys590D) {
        this.tsStrDys590D = tsStrDys590D == null ? null : tsStrDys590D.trim();
    }

    public String getTsStrDys590E() {
        return tsStrDys590E;
    }

    public void setTsStrDys590E(String tsStrDys590E) {
        this.tsStrDys590E = tsStrDys590E == null ? null : tsStrDys590E.trim();
    }

    public String getTxStrDys19() {
        return txStrDys19;
    }

    public void setTxStrDys19(String txStrDys19) {
        this.txStrDys19 = txStrDys19 == null ? null : txStrDys19.trim();
    }

    public String getTxStrDys359() {
        return txStrDys359;
    }

    public void setTxStrDys359(String txStrDys359) {
        this.txStrDys359 = txStrDys359 == null ? null : txStrDys359.trim();
    }

    public String getTxStrDys390() {
        return txStrDys390;
    }

    public void setTxStrDys390(String txStrDys390) {
        this.txStrDys390 = txStrDys390 == null ? null : txStrDys390.trim();
    }

    public String getTxStrDys391() {
        return txStrDys391;
    }

    public void setTxStrDys391(String txStrDys391) {
        this.txStrDys391 = txStrDys391 == null ? null : txStrDys391.trim();
    }

    public String getTxStrDys392() {
        return txStrDys392;
    }

    public void setTxStrDys392(String txStrDys392) {
        this.txStrDys392 = txStrDys392 == null ? null : txStrDys392.trim();
    }

    public String getTxStrDys393() {
        return txStrDys393;
    }

    public void setTxStrDys393(String txStrDys393) {
        this.txStrDys393 = txStrDys393 == null ? null : txStrDys393.trim();
    }

    public String getTxStrDys437() {
        return txStrDys437;
    }

    public void setTxStrDys437(String txStrDys437) {
        this.txStrDys437 = txStrDys437 == null ? null : txStrDys437.trim();
    }

    public String getTxStrDys635() {
        return txStrDys635;
    }

    public void setTxStrDys635(String txStrDys635) {
        this.txStrDys635 = txStrDys635 == null ? null : txStrDys635.trim();
    }

    public String getTxStrDys590() {
        return txStrDys590;
    }

    public void setTxStrDys590(String txStrDys590) {
        this.txStrDys590 = txStrDys590 == null ? null : txStrDys590.trim();
    }

    public String getOtherDys19() {
        return otherDys19;
    }

    public void setOtherDys19(String otherDys19) {
        this.otherDys19 = otherDys19 == null ? null : otherDys19.trim();
    }

    public String getOtherDys359() {
        return otherDys359;
    }

    public void setOtherDys359(String otherDys359) {
        this.otherDys359 = otherDys359 == null ? null : otherDys359.trim();
    }

    public String getOtherDys390() {
        return otherDys390;
    }

    public void setOtherDys390(String otherDys390) {
        this.otherDys390 = otherDys390 == null ? null : otherDys390.trim();
    }

    public String getOtherDys391() {
        return otherDys391;
    }

    public void setOtherDys391(String otherDys391) {
        this.otherDys391 = otherDys391 == null ? null : otherDys391.trim();
    }

    public String getOtherDys392() {
        return otherDys392;
    }

    public void setOtherDys392(String otherDys392) {
        this.otherDys392 = otherDys392 == null ? null : otherDys392.trim();
    }

    public String getOtherDys393() {
        return otherDys393;
    }

    public void setOtherDys393(String otherDys393) {
        this.otherDys393 = otherDys393 == null ? null : otherDys393.trim();
    }

    public String getOtherDys437() {
        return otherDys437;
    }

    public void setOtherDys437(String otherDys437) {
        this.otherDys437 = otherDys437 == null ? null : otherDys437.trim();
    }

    public String getOtherDys635() {
        return otherDys635;
    }

    public void setOtherDys635(String otherDys635) {
        this.otherDys635 = otherDys635 == null ? null : otherDys635.trim();
    }

    public String getOtherDys590() {
        return otherDys590;
    }

    public void setOtherDys590(String otherDys590) {
        this.otherDys590 = otherDys590 == null ? null : otherDys590.trim();
    }

	@Override
	public String getData() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setData(String data) {
		// TODO Auto-generated method stub
		
	}
}