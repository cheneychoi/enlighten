package com.enlighten.aio.db.mapper;

import com.enlighten.aio.db.model.Favorite;
import com.enlighten.aio.db.model.FavoriteExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface FavoriteMapper {
    long countByExample(FavoriteExample example);

    int deleteByExample(FavoriteExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Favorite record);

    int insertSelective(Favorite record);

    List<Favorite> selectByExampleWithBLOBsWithRowbounds(FavoriteExample example, RowBounds rowBounds);

    List<Favorite> selectByExampleWithBLOBs(FavoriteExample example);

    List<Favorite> selectByExampleWithRowbounds(FavoriteExample example, RowBounds rowBounds);

    List<Favorite> selectByExample(FavoriteExample example);

    Favorite selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Favorite record, @Param("example") FavoriteExample example);

    int updateByExampleWithBLOBs(@Param("record") Favorite record, @Param("example") FavoriteExample example);

    int updateByExample(@Param("record") Favorite record, @Param("example") FavoriteExample example);

    int updateByPrimaryKeySelective(Favorite record);

    int updateByPrimaryKeyWithBLOBs(Favorite record);

    int updateByPrimaryKey(Favorite record);
}