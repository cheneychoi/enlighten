package com.enlighten.aio.db.mapper;

import com.enlighten.aio.db.model.Talk;
import com.enlighten.aio.db.model.TalkExample;
import com.enlighten.aio.db.model.TalkWithBLOBs;
import com.enlighten.aio.db.model.out.TalkOut;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface TalkMapper {
    long countByExample(TalkExample example);

    int deleteByExample(TalkExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TalkWithBLOBs record);

    int insertSelective(TalkWithBLOBs record);

    List<TalkWithBLOBs> selectByExampleWithBLOBsWithRowbounds(TalkExample example, RowBounds rowBounds);

    List<TalkWithBLOBs> selectByExampleWithBLOBs(TalkExample example);

    List<Talk> selectByExampleWithRowbounds(TalkExample example, RowBounds rowBounds);

    List<Talk> selectByExample(TalkExample example);

    TalkWithBLOBs selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TalkWithBLOBs record, @Param("example") TalkExample example);

    int updateByExampleWithBLOBs(@Param("record") TalkWithBLOBs record, @Param("example") TalkExample example);

    int updateByExample(@Param("record") Talk record, @Param("example") TalkExample example);

    int updateByPrimaryKeySelective(TalkWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(TalkWithBLOBs record);

    int updateByPrimaryKey(Talk record);
    
    int readTalk(@Param("sendId")Long sendId,@Param("receiveId")Long receiveId);
    
    List<TalkOut> getTalk(Long receiveId);
}