package com.enlighten.aio.db.model.out;

import java.util.Date;
public class UserOut {
    private Long id;
    private String surname;
    private String name;
    private String branch;
    private String avator;
    private String jiguan;
    private Date birthday;
    private String birthPlace;
	private String dialect;
    private String fatherNationality;
    private String fatherLanguage;
	private String fatherCountry;
	private String motherCoutry;
    private String motherJiguan;
    private String motherNationality;
    private String motherLanguage;
    private String ySnp;
	private Integer state;
    private String sequence;
    private String yStr;
    private String mtdna;
	private String isAgree;
	private String research;
    private Date updateDate;
    private String documentName;
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAvator() {
		return avator;
	}
	public void setAvator(String avator) {
		this.avator = avator;
	}
	public String getJiguan() {
		return jiguan;
	}
	public void setJiguan(String jiguan) {
		this.jiguan = jiguan;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getBirthPlace() {
		return birthPlace;
	}
	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}
	public String getDialect() {
		return dialect;
	}
	public void setDialect(String dialect) {
		this.dialect = dialect;
	}
	public String getFatherNationality() {
		return fatherNationality;
	}
	public void setFatherNationality(String fatherNationality) {
		this.fatherNationality = fatherNationality;
	}
	public String getFatherLanguage() {
		return fatherLanguage;
	}
	public void setFatherLanguage(String fatherLanguage) {
		this.fatherLanguage = fatherLanguage;
	}
	public String getFatherCountry() {
		return fatherCountry;
	}
	public void setFatherCountry(String fatherCountry) {
		this.fatherCountry = fatherCountry;
	}
	public String getMotherCoutry() {
		return motherCoutry;
	}
	public void setMotherCoutry(String motherCoutry) {
		this.motherCoutry = motherCoutry;
	}
	public String getMotherJiguan() {
		return motherJiguan;
	}
	public void setMotherJiguan(String motherJiguan) {
		this.motherJiguan = motherJiguan;
	}
	public String getMotherNationality() {
		return motherNationality;
	}
	public void setMotherNationality(String motherNationality) {
		this.motherNationality = motherNationality;
	}
	public String getMotherLanguage() {
		return motherLanguage;
	}
	public void setMotherLanguage(String motherLanguage) {
		this.motherLanguage = motherLanguage;
	}
	public String getySnp() {
		return ySnp;
	}
	public void setySnp(String ySnp) {
		this.ySnp = ySnp;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public String getyStr() {
		return yStr;
	}
	public void setyStr(String yStr) {
		this.yStr = yStr;
	}
	public String getMtdna() {
		return mtdna;
	}
	public void setMtdna(String mtdna) {
		this.mtdna = mtdna;
	}
	public String getIsAgree() {
		return isAgree;
	}
	public void setIsAgree(String isAgree) {
		this.isAgree = isAgree;
	}
	public String getResearch() {
		return research;
	}
	public void setResearch(String research) {
		this.research = research;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
}
