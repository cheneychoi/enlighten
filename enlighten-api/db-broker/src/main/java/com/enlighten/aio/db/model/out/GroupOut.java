package com.enlighten.aio.db.model.out;

import java.util.Date;

public class GroupOut {
	 private Long id;

	    private Long userId;
	    
	    private String reason;

	    private String groupName;

	    private String background;

	    private String administrator;

	    private String adminEmail;

	    private String teamPlayer;

	    private String result;
	    
	    private String qq;
	    
	    private String wx;

	    private Long pictureId;

	    private Date createDate;

	    private Date updateDate;

	    private String remarks;
	    
	    private String pictureUrl;

		public String getQq() {
			return qq;
		}

		public void setQq(String qq) {
			this.qq = qq;
		}

		public String getWx() {
			return wx;
		}

		public void setWx(String wx) {
			this.wx = wx;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Long getUserId() {
			return userId;
		}

		public void setUserId(Long userId) {
			this.userId = userId;
		}

		public String getReason() {
			return reason;
		}

		public void setReason(String reason) {
			this.reason = reason;
		}

		public String getGroupName() {
			return groupName;
		}

		public void setGroupName(String groupName) {
			this.groupName = groupName;
		}

		public String getBackground() {
			return background;
		}

		public void setBackground(String background) {
			this.background = background;
		}

		public String getAdministrator() {
			return administrator;
		}

		public void setAdministrator(String administrator) {
			this.administrator = administrator;
		}

		public String getAdminEmail() {
			return adminEmail;
		}

		public void setAdminEmail(String adminEmail) {
			this.adminEmail = adminEmail;
		}

		public String getTeamPlayer() {
			return teamPlayer;
		}

		public void setTeamPlayer(String teamPlayer) {
			this.teamPlayer = teamPlayer;
		}

		public String getResult() {
			return result;
		}

		public void setResult(String result) {
			this.result = result;
		}

		public Long getPictureId() {
			return pictureId;
		}

		public void setPictureId(Long pictureId) {
			this.pictureId = pictureId;
		}

		public Date getCreateDate() {
			return createDate;
		}

		public void setCreateDate(Date createDate) {
			this.createDate = createDate;
		}

		public Date getUpdateDate() {
			return updateDate;
		}

		public void setUpdateDate(Date updateDate) {
			this.updateDate = updateDate;
		}

		public String getRemarks() {
			return remarks;
		}

		public void setRemarks(String remarks) {
			this.remarks = remarks;
		}

		public String getPictureUrl() {
			return pictureUrl;
		}

		public void setPictureUrl(String pictureUrl) {
			this.pictureUrl = pictureUrl;
		}

}
