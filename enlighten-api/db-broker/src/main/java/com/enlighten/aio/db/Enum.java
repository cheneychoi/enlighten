package com.enlighten.aio.db;

public interface Enum {

	public enum OrderStatus {

		NEW("待付款", 0), CANCEL("已取消", 100), PAID("待发货", 200), REFUND_APPLY("未发货申请退款", 201), DELIVER_UNDERWAY("待收货",
				300), RETURN_GOODS("退货申请", 301), CLOSED("订单关闭(退款或退货结束后关闭)", 399), CONFIRM_RECEIPT("确认收货-待评价",
						400), FINISHED("完成", 500), REMOVED("删除", -1);

		private String value;
		private Integer code;

		OrderStatus(String value, Integer code) {
			this.value = value;
			this.code = code;
		}

		public String getValue() {
			return value;
		}

		public Integer getCode() {
			return code;
		}
	}
	
	/**
	 * 支付状态
	 * 
	 * @author chenheng
	 */
	public enum PaymentStatus {
		NEW("待支付", 0), CANCEL("取消", 100), COD_APPLY("货到付款", 199), PAID("已支付", 200), CONFIRM("已确认", 201), CLOSED("已关闭",
				399);

		private String value;
		private Integer code;

		PaymentStatus(String value, Integer code) {
			this.value = value;
			this.code = code;
		}

		public String getValue() {
			return value;
		}

		public Integer getCode() {
			return code;
		}
	}
	
	/**
	 * 支付供应商
	 * 
	 * @author chenheng
	 */
	public enum PaymentVendor {
		ALI_PAY("支付宝支付", "ALI_PAY"), WX_PAY("微信支付", "WX_PAY"), OFFLINE_PAY("线下支付", "OFFLINE_PAY");

		private String value;
		private String code;

		PaymentVendor(String value, String code) {
			this.value = value;
			this.code = code;
		}

		public String getValue() {
			return value;
		}

		public String getCode() {
			return code;
		}
	}
}
