package com.enlighten.aio.db.mapper;

import com.enlighten.aio.db.model.Friend;
import com.enlighten.aio.db.model.FriendExample;
import com.enlighten.aio.db.model.out.FriendOut;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface FriendMapper {
    long countByExample(FriendExample example);

    int deleteByExample(FriendExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Friend record);

    int insertSelective(Friend record);

    List<Friend> selectByExampleWithBLOBsWithRowbounds(FriendExample example, RowBounds rowBounds);

    List<Friend> selectByExampleWithBLOBs(FriendExample example);

    List<Friend> selectByExampleWithRowbounds(FriendExample example, RowBounds rowBounds);

    List<Friend> selectByExample(FriendExample example);

    Friend selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Friend record, @Param("example") FriendExample example);

    int updateByExampleWithBLOBs(@Param("record") Friend record, @Param("example") FriendExample example);

    int updateByExample(@Param("record") Friend record, @Param("example") FriendExample example);

    int updateByPrimaryKeySelective(Friend record);

    int updateByPrimaryKeyWithBLOBs(Friend record);

    int updateByPrimaryKey(Friend record);
    /**
     * 查看我的好友列表
     * Lu
     *2018年8月28日
     */
    List<FriendOut> getAllFriend(Long userId);
    /**
     * 查看关注我的好友列表
     * Lu
     *2018年8月28日
     */
    List<FriendOut> getAllAttentionMy(Long userId);
    /**
     * 查看我关注的列表
     * Lu
     *2018年8月28日
     */
    List<FriendOut> getAllMyAttention(Long userId);
    /**
     * 是否同意加好友
     * Lu
     *2018年8月31日
     */
    int updateFriend(@Param("isFriend")Integer isFriend,@Param("userId")Long userId,@Param("friendId")Long friendId);
}