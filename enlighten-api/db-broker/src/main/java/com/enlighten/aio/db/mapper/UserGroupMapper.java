package com.enlighten.aio.db.mapper;

import com.enlighten.aio.db.model.UserGroup;
import com.enlighten.aio.db.model.UserGroupExample;
import com.enlighten.aio.db.model.UserGroupOut;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface UserGroupMapper {
    long countByExample(UserGroupExample example);

    int deleteByExample(UserGroupExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserGroup record);

    int insertSelective(UserGroup record);

    List<UserGroup> selectByExampleWithBLOBsWithRowbounds(UserGroupExample example, RowBounds rowBounds);

    List<UserGroup> selectByExampleWithBLOBs(UserGroupExample example);

    List<UserGroup> selectByExampleWithRowbounds(UserGroupExample example, RowBounds rowBounds);

    List<UserGroup> selectByExample(UserGroupExample example);

    UserGroup selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserGroup record, @Param("example") UserGroupExample example);

    int updateByExampleWithBLOBs(@Param("record") UserGroup record, @Param("example") UserGroupExample example);

    int updateByExample(@Param("record") UserGroup record, @Param("example") UserGroupExample example);

    int updateByPrimaryKeySelective(UserGroup record);

    int updateByPrimaryKeyWithBLOBs(UserGroup record);

    int updateByPrimaryKey(UserGroup record);
    
    List<UserGroupOut> getUserGroup(Long userId);
}