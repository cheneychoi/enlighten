package com.enlighten.aio.db.mapper;

import com.enlighten.aio.db.model.EnArea;
import com.enlighten.aio.db.model.EnAreaExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface EnAreaMapper {
    long countByExample(EnAreaExample example);

    int deleteByExample(EnAreaExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(EnArea record);

    int insertSelective(EnArea record);

    List<EnArea> selectByExampleWithRowbounds(EnAreaExample example, RowBounds rowBounds);

    List<EnArea> selectByExample(EnAreaExample example);

    EnArea selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") EnArea record, @Param("example") EnAreaExample example);

    int updateByExample(@Param("record") EnArea record, @Param("example") EnAreaExample example);

    int updateByPrimaryKeySelective(EnArea record);

    int updateByPrimaryKey(EnArea record);
}