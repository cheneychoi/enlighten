package com.enlighten.aio.db.mapper;

import com.enlighten.aio.db.model.EnCity;
import com.enlighten.aio.db.model.EnCityExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface EnCityMapper {
    long countByExample(EnCityExample example);

    int deleteByExample(EnCityExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(EnCity record);

    int insertSelective(EnCity record);

    List<EnCity> selectByExampleWithRowbounds(EnCityExample example, RowBounds rowBounds);

    List<EnCity> selectByExample(EnCityExample example);

    EnCity selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") EnCity record, @Param("example") EnCityExample example);

    int updateByExample(@Param("record") EnCity record, @Param("example") EnCityExample example);

    int updateByPrimaryKeySelective(EnCity record);

    int updateByPrimaryKey(EnCity record);
}