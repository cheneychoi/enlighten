package com.enlighten.aio.db.mapper;

import com.enlighten.aio.db.model.Inspect;
import com.enlighten.aio.db.model.InspectExample;
import com.enlighten.aio.db.model.out.InspectOut;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface InspectMapper {
    long countByExample(InspectExample example);

    int deleteByExample(InspectExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Inspect record);

    int insertSelective(Inspect record);

    List<Inspect> selectByExampleWithRowbounds(InspectExample example, RowBounds rowBounds);

    List<Inspect> selectByExample(InspectExample example);

    Inspect selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Inspect record, @Param("example") InspectExample example);

    int updateByExample(@Param("record") Inspect record, @Param("example") InspectExample example);

    int updateByPrimaryKeySelective(Inspect record);

    int updateByPrimaryKey(Inspect record);
    /**
     * 后台看用户录入检测数据
     * Lu
     *2018年9月3日
     */
    List<InspectOut> getInspect();
}