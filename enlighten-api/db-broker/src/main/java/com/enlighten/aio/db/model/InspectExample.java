package com.enlighten.aio.db.model;

import java.util.ArrayList;
import java.util.List;

public class InspectExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public InspectExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andSurnameIsNull() {
            addCriterion("surname is null");
            return (Criteria) this;
        }

        public Criteria andSurnameIsNotNull() {
            addCriterion("surname is not null");
            return (Criteria) this;
        }

        public Criteria andSurnameEqualTo(String value) {
            addCriterion("surname =", value, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameNotEqualTo(String value) {
            addCriterion("surname <>", value, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameGreaterThan(String value) {
            addCriterion("surname >", value, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameGreaterThanOrEqualTo(String value) {
            addCriterion("surname >=", value, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameLessThan(String value) {
            addCriterion("surname <", value, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameLessThanOrEqualTo(String value) {
            addCriterion("surname <=", value, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameLike(String value) {
            addCriterion("surname like", value, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameNotLike(String value) {
            addCriterion("surname not like", value, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameIn(List<String> values) {
            addCriterion("surname in", values, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameNotIn(List<String> values) {
            addCriterion("surname not in", values, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameBetween(String value1, String value2) {
            addCriterion("surname between", value1, value2, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameNotBetween(String value1, String value2) {
            addCriterion("surname not between", value1, value2, "surname");
            return (Criteria) this;
        }

        public Criteria andNationalityIsNull() {
            addCriterion("nationality is null");
            return (Criteria) this;
        }

        public Criteria andNationalityIsNotNull() {
            addCriterion("nationality is not null");
            return (Criteria) this;
        }

        public Criteria andNationalityEqualTo(String value) {
            addCriterion("nationality =", value, "nationality");
            return (Criteria) this;
        }

        public Criteria andNationalityNotEqualTo(String value) {
            addCriterion("nationality <>", value, "nationality");
            return (Criteria) this;
        }

        public Criteria andNationalityGreaterThan(String value) {
            addCriterion("nationality >", value, "nationality");
            return (Criteria) this;
        }

        public Criteria andNationalityGreaterThanOrEqualTo(String value) {
            addCriterion("nationality >=", value, "nationality");
            return (Criteria) this;
        }

        public Criteria andNationalityLessThan(String value) {
            addCriterion("nationality <", value, "nationality");
            return (Criteria) this;
        }

        public Criteria andNationalityLessThanOrEqualTo(String value) {
            addCriterion("nationality <=", value, "nationality");
            return (Criteria) this;
        }

        public Criteria andNationalityLike(String value) {
            addCriterion("nationality like", value, "nationality");
            return (Criteria) this;
        }

        public Criteria andNationalityNotLike(String value) {
            addCriterion("nationality not like", value, "nationality");
            return (Criteria) this;
        }

        public Criteria andNationalityIn(List<String> values) {
            addCriterion("nationality in", values, "nationality");
            return (Criteria) this;
        }

        public Criteria andNationalityNotIn(List<String> values) {
            addCriterion("nationality not in", values, "nationality");
            return (Criteria) this;
        }

        public Criteria andNationalityBetween(String value1, String value2) {
            addCriterion("nationality between", value1, value2, "nationality");
            return (Criteria) this;
        }

        public Criteria andNationalityNotBetween(String value1, String value2) {
            addCriterion("nationality not between", value1, value2, "nationality");
            return (Criteria) this;
        }

        public Criteria andJiguanIsNull() {
            addCriterion("jiguan is null");
            return (Criteria) this;
        }

        public Criteria andJiguanIsNotNull() {
            addCriterion("jiguan is not null");
            return (Criteria) this;
        }

        public Criteria andJiguanEqualTo(String value) {
            addCriterion("jiguan =", value, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanNotEqualTo(String value) {
            addCriterion("jiguan <>", value, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanGreaterThan(String value) {
            addCriterion("jiguan >", value, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanGreaterThanOrEqualTo(String value) {
            addCriterion("jiguan >=", value, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanLessThan(String value) {
            addCriterion("jiguan <", value, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanLessThanOrEqualTo(String value) {
            addCriterion("jiguan <=", value, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanLike(String value) {
            addCriterion("jiguan like", value, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanNotLike(String value) {
            addCriterion("jiguan not like", value, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanIn(List<String> values) {
            addCriterion("jiguan in", values, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanNotIn(List<String> values) {
            addCriterion("jiguan not in", values, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanBetween(String value1, String value2) {
            addCriterion("jiguan between", value1, value2, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanNotBetween(String value1, String value2) {
            addCriterion("jiguan not between", value1, value2, "jiguan");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Long value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Long value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Long value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Long value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Long value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Long value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Long> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Long> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Long value1, Long value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Long value1, Long value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andDys19IsNull() {
            addCriterion("dys19 is null");
            return (Criteria) this;
        }

        public Criteria andDys19IsNotNull() {
            addCriterion("dys19 is not null");
            return (Criteria) this;
        }

        public Criteria andDys19EqualTo(String value) {
            addCriterion("dys19 =", value, "dys19");
            return (Criteria) this;
        }

        public Criteria andDys19NotEqualTo(String value) {
            addCriterion("dys19 <>", value, "dys19");
            return (Criteria) this;
        }

        public Criteria andDys19GreaterThan(String value) {
            addCriterion("dys19 >", value, "dys19");
            return (Criteria) this;
        }

        public Criteria andDys19GreaterThanOrEqualTo(String value) {
            addCriterion("dys19 >=", value, "dys19");
            return (Criteria) this;
        }

        public Criteria andDys19LessThan(String value) {
            addCriterion("dys19 <", value, "dys19");
            return (Criteria) this;
        }

        public Criteria andDys19LessThanOrEqualTo(String value) {
            addCriterion("dys19 <=", value, "dys19");
            return (Criteria) this;
        }

        public Criteria andDys19Like(String value) {
            addCriterion("dys19 like", value, "dys19");
            return (Criteria) this;
        }

        public Criteria andDys19NotLike(String value) {
            addCriterion("dys19 not like", value, "dys19");
            return (Criteria) this;
        }

        public Criteria andDys19In(List<String> values) {
            addCriterion("dys19 in", values, "dys19");
            return (Criteria) this;
        }

        public Criteria andDys19NotIn(List<String> values) {
            addCriterion("dys19 not in", values, "dys19");
            return (Criteria) this;
        }

        public Criteria andDys19Between(String value1, String value2) {
            addCriterion("dys19 between", value1, value2, "dys19");
            return (Criteria) this;
        }

        public Criteria andDys19NotBetween(String value1, String value2) {
            addCriterion("dys19 not between", value1, value2, "dys19");
            return (Criteria) this;
        }

        public Criteria andDys359IsNull() {
            addCriterion("dys359 is null");
            return (Criteria) this;
        }

        public Criteria andDys359IsNotNull() {
            addCriterion("dys359 is not null");
            return (Criteria) this;
        }

        public Criteria andDys359EqualTo(String value) {
            addCriterion("dys359 =", value, "dys359");
            return (Criteria) this;
        }

        public Criteria andDys359NotEqualTo(String value) {
            addCriterion("dys359 <>", value, "dys359");
            return (Criteria) this;
        }

        public Criteria andDys359GreaterThan(String value) {
            addCriterion("dys359 >", value, "dys359");
            return (Criteria) this;
        }

        public Criteria andDys359GreaterThanOrEqualTo(String value) {
            addCriterion("dys359 >=", value, "dys359");
            return (Criteria) this;
        }

        public Criteria andDys359LessThan(String value) {
            addCriterion("dys359 <", value, "dys359");
            return (Criteria) this;
        }

        public Criteria andDys359LessThanOrEqualTo(String value) {
            addCriterion("dys359 <=", value, "dys359");
            return (Criteria) this;
        }

        public Criteria andDys359Like(String value) {
            addCriterion("dys359 like", value, "dys359");
            return (Criteria) this;
        }

        public Criteria andDys359NotLike(String value) {
            addCriterion("dys359 not like", value, "dys359");
            return (Criteria) this;
        }

        public Criteria andDys359In(List<String> values) {
            addCriterion("dys359 in", values, "dys359");
            return (Criteria) this;
        }

        public Criteria andDys359NotIn(List<String> values) {
            addCriterion("dys359 not in", values, "dys359");
            return (Criteria) this;
        }

        public Criteria andDys359Between(String value1, String value2) {
            addCriterion("dys359 between", value1, value2, "dys359");
            return (Criteria) this;
        }

        public Criteria andDys359NotBetween(String value1, String value2) {
            addCriterion("dys359 not between", value1, value2, "dys359");
            return (Criteria) this;
        }

        public Criteria andDys390IsNull() {
            addCriterion("dys390 is null");
            return (Criteria) this;
        }

        public Criteria andDys390IsNotNull() {
            addCriterion("dys390 is not null");
            return (Criteria) this;
        }

        public Criteria andDys390EqualTo(String value) {
            addCriterion("dys390 =", value, "dys390");
            return (Criteria) this;
        }

        public Criteria andDys390NotEqualTo(String value) {
            addCriterion("dys390 <>", value, "dys390");
            return (Criteria) this;
        }

        public Criteria andDys390GreaterThan(String value) {
            addCriterion("dys390 >", value, "dys390");
            return (Criteria) this;
        }

        public Criteria andDys390GreaterThanOrEqualTo(String value) {
            addCriterion("dys390 >=", value, "dys390");
            return (Criteria) this;
        }

        public Criteria andDys390LessThan(String value) {
            addCriterion("dys390 <", value, "dys390");
            return (Criteria) this;
        }

        public Criteria andDys390LessThanOrEqualTo(String value) {
            addCriterion("dys390 <=", value, "dys390");
            return (Criteria) this;
        }

        public Criteria andDys390Like(String value) {
            addCriterion("dys390 like", value, "dys390");
            return (Criteria) this;
        }

        public Criteria andDys390NotLike(String value) {
            addCriterion("dys390 not like", value, "dys390");
            return (Criteria) this;
        }

        public Criteria andDys390In(List<String> values) {
            addCriterion("dys390 in", values, "dys390");
            return (Criteria) this;
        }

        public Criteria andDys390NotIn(List<String> values) {
            addCriterion("dys390 not in", values, "dys390");
            return (Criteria) this;
        }

        public Criteria andDys390Between(String value1, String value2) {
            addCriterion("dys390 between", value1, value2, "dys390");
            return (Criteria) this;
        }

        public Criteria andDys390NotBetween(String value1, String value2) {
            addCriterion("dys390 not between", value1, value2, "dys390");
            return (Criteria) this;
        }

        public Criteria andDys391IsNull() {
            addCriterion("dys391 is null");
            return (Criteria) this;
        }

        public Criteria andDys391IsNotNull() {
            addCriterion("dys391 is not null");
            return (Criteria) this;
        }

        public Criteria andDys391EqualTo(String value) {
            addCriterion("dys391 =", value, "dys391");
            return (Criteria) this;
        }

        public Criteria andDys391NotEqualTo(String value) {
            addCriterion("dys391 <>", value, "dys391");
            return (Criteria) this;
        }

        public Criteria andDys391GreaterThan(String value) {
            addCriterion("dys391 >", value, "dys391");
            return (Criteria) this;
        }

        public Criteria andDys391GreaterThanOrEqualTo(String value) {
            addCriterion("dys391 >=", value, "dys391");
            return (Criteria) this;
        }

        public Criteria andDys391LessThan(String value) {
            addCriterion("dys391 <", value, "dys391");
            return (Criteria) this;
        }

        public Criteria andDys391LessThanOrEqualTo(String value) {
            addCriterion("dys391 <=", value, "dys391");
            return (Criteria) this;
        }

        public Criteria andDys391Like(String value) {
            addCriterion("dys391 like", value, "dys391");
            return (Criteria) this;
        }

        public Criteria andDys391NotLike(String value) {
            addCriterion("dys391 not like", value, "dys391");
            return (Criteria) this;
        }

        public Criteria andDys391In(List<String> values) {
            addCriterion("dys391 in", values, "dys391");
            return (Criteria) this;
        }

        public Criteria andDys391NotIn(List<String> values) {
            addCriterion("dys391 not in", values, "dys391");
            return (Criteria) this;
        }

        public Criteria andDys391Between(String value1, String value2) {
            addCriterion("dys391 between", value1, value2, "dys391");
            return (Criteria) this;
        }

        public Criteria andDys391NotBetween(String value1, String value2) {
            addCriterion("dys391 not between", value1, value2, "dys391");
            return (Criteria) this;
        }

        public Criteria andDys392IsNull() {
            addCriterion("dys392 is null");
            return (Criteria) this;
        }

        public Criteria andDys392IsNotNull() {
            addCriterion("dys392 is not null");
            return (Criteria) this;
        }

        public Criteria andDys392EqualTo(String value) {
            addCriterion("dys392 =", value, "dys392");
            return (Criteria) this;
        }

        public Criteria andDys392NotEqualTo(String value) {
            addCriterion("dys392 <>", value, "dys392");
            return (Criteria) this;
        }

        public Criteria andDys392GreaterThan(String value) {
            addCriterion("dys392 >", value, "dys392");
            return (Criteria) this;
        }

        public Criteria andDys392GreaterThanOrEqualTo(String value) {
            addCriterion("dys392 >=", value, "dys392");
            return (Criteria) this;
        }

        public Criteria andDys392LessThan(String value) {
            addCriterion("dys392 <", value, "dys392");
            return (Criteria) this;
        }

        public Criteria andDys392LessThanOrEqualTo(String value) {
            addCriterion("dys392 <=", value, "dys392");
            return (Criteria) this;
        }

        public Criteria andDys392Like(String value) {
            addCriterion("dys392 like", value, "dys392");
            return (Criteria) this;
        }

        public Criteria andDys392NotLike(String value) {
            addCriterion("dys392 not like", value, "dys392");
            return (Criteria) this;
        }

        public Criteria andDys392In(List<String> values) {
            addCriterion("dys392 in", values, "dys392");
            return (Criteria) this;
        }

        public Criteria andDys392NotIn(List<String> values) {
            addCriterion("dys392 not in", values, "dys392");
            return (Criteria) this;
        }

        public Criteria andDys392Between(String value1, String value2) {
            addCriterion("dys392 between", value1, value2, "dys392");
            return (Criteria) this;
        }

        public Criteria andDys392NotBetween(String value1, String value2) {
            addCriterion("dys392 not between", value1, value2, "dys392");
            return (Criteria) this;
        }

        public Criteria andDys393IsNull() {
            addCriterion("dys393 is null");
            return (Criteria) this;
        }

        public Criteria andDys393IsNotNull() {
            addCriterion("dys393 is not null");
            return (Criteria) this;
        }

        public Criteria andDys393EqualTo(String value) {
            addCriterion("dys393 =", value, "dys393");
            return (Criteria) this;
        }

        public Criteria andDys393NotEqualTo(String value) {
            addCriterion("dys393 <>", value, "dys393");
            return (Criteria) this;
        }

        public Criteria andDys393GreaterThan(String value) {
            addCriterion("dys393 >", value, "dys393");
            return (Criteria) this;
        }

        public Criteria andDys393GreaterThanOrEqualTo(String value) {
            addCriterion("dys393 >=", value, "dys393");
            return (Criteria) this;
        }

        public Criteria andDys393LessThan(String value) {
            addCriterion("dys393 <", value, "dys393");
            return (Criteria) this;
        }

        public Criteria andDys393LessThanOrEqualTo(String value) {
            addCriterion("dys393 <=", value, "dys393");
            return (Criteria) this;
        }

        public Criteria andDys393Like(String value) {
            addCriterion("dys393 like", value, "dys393");
            return (Criteria) this;
        }

        public Criteria andDys393NotLike(String value) {
            addCriterion("dys393 not like", value, "dys393");
            return (Criteria) this;
        }

        public Criteria andDys393In(List<String> values) {
            addCriterion("dys393 in", values, "dys393");
            return (Criteria) this;
        }

        public Criteria andDys393NotIn(List<String> values) {
            addCriterion("dys393 not in", values, "dys393");
            return (Criteria) this;
        }

        public Criteria andDys393Between(String value1, String value2) {
            addCriterion("dys393 between", value1, value2, "dys393");
            return (Criteria) this;
        }

        public Criteria andDys393NotBetween(String value1, String value2) {
            addCriterion("dys393 not between", value1, value2, "dys393");
            return (Criteria) this;
        }

        public Criteria andDys437IsNull() {
            addCriterion("dys437 is null");
            return (Criteria) this;
        }

        public Criteria andDys437IsNotNull() {
            addCriterion("dys437 is not null");
            return (Criteria) this;
        }

        public Criteria andDys437EqualTo(String value) {
            addCriterion("dys437 =", value, "dys437");
            return (Criteria) this;
        }

        public Criteria andDys437NotEqualTo(String value) {
            addCriterion("dys437 <>", value, "dys437");
            return (Criteria) this;
        }

        public Criteria andDys437GreaterThan(String value) {
            addCriterion("dys437 >", value, "dys437");
            return (Criteria) this;
        }

        public Criteria andDys437GreaterThanOrEqualTo(String value) {
            addCriterion("dys437 >=", value, "dys437");
            return (Criteria) this;
        }

        public Criteria andDys437LessThan(String value) {
            addCriterion("dys437 <", value, "dys437");
            return (Criteria) this;
        }

        public Criteria andDys437LessThanOrEqualTo(String value) {
            addCriterion("dys437 <=", value, "dys437");
            return (Criteria) this;
        }

        public Criteria andDys437Like(String value) {
            addCriterion("dys437 like", value, "dys437");
            return (Criteria) this;
        }

        public Criteria andDys437NotLike(String value) {
            addCriterion("dys437 not like", value, "dys437");
            return (Criteria) this;
        }

        public Criteria andDys437In(List<String> values) {
            addCriterion("dys437 in", values, "dys437");
            return (Criteria) this;
        }

        public Criteria andDys437NotIn(List<String> values) {
            addCriterion("dys437 not in", values, "dys437");
            return (Criteria) this;
        }

        public Criteria andDys437Between(String value1, String value2) {
            addCriterion("dys437 between", value1, value2, "dys437");
            return (Criteria) this;
        }

        public Criteria andDys437NotBetween(String value1, String value2) {
            addCriterion("dys437 not between", value1, value2, "dys437");
            return (Criteria) this;
        }

        public Criteria andDys635IsNull() {
            addCriterion("dys635 is null");
            return (Criteria) this;
        }

        public Criteria andDys635IsNotNull() {
            addCriterion("dys635 is not null");
            return (Criteria) this;
        }

        public Criteria andDys635EqualTo(String value) {
            addCriterion("dys635 =", value, "dys635");
            return (Criteria) this;
        }

        public Criteria andDys635NotEqualTo(String value) {
            addCriterion("dys635 <>", value, "dys635");
            return (Criteria) this;
        }

        public Criteria andDys635GreaterThan(String value) {
            addCriterion("dys635 >", value, "dys635");
            return (Criteria) this;
        }

        public Criteria andDys635GreaterThanOrEqualTo(String value) {
            addCriterion("dys635 >=", value, "dys635");
            return (Criteria) this;
        }

        public Criteria andDys635LessThan(String value) {
            addCriterion("dys635 <", value, "dys635");
            return (Criteria) this;
        }

        public Criteria andDys635LessThanOrEqualTo(String value) {
            addCriterion("dys635 <=", value, "dys635");
            return (Criteria) this;
        }

        public Criteria andDys635Like(String value) {
            addCriterion("dys635 like", value, "dys635");
            return (Criteria) this;
        }

        public Criteria andDys635NotLike(String value) {
            addCriterion("dys635 not like", value, "dys635");
            return (Criteria) this;
        }

        public Criteria andDys635In(List<String> values) {
            addCriterion("dys635 in", values, "dys635");
            return (Criteria) this;
        }

        public Criteria andDys635NotIn(List<String> values) {
            addCriterion("dys635 not in", values, "dys635");
            return (Criteria) this;
        }

        public Criteria andDys635Between(String value1, String value2) {
            addCriterion("dys635 between", value1, value2, "dys635");
            return (Criteria) this;
        }

        public Criteria andDys635NotBetween(String value1, String value2) {
            addCriterion("dys635 not between", value1, value2, "dys635");
            return (Criteria) this;
        }

        public Criteria andDys590IsNull() {
            addCriterion("dys590 is null");
            return (Criteria) this;
        }

        public Criteria andDys590IsNotNull() {
            addCriterion("dys590 is not null");
            return (Criteria) this;
        }

        public Criteria andDys590EqualTo(String value) {
            addCriterion("dys590 =", value, "dys590");
            return (Criteria) this;
        }

        public Criteria andDys590NotEqualTo(String value) {
            addCriterion("dys590 <>", value, "dys590");
            return (Criteria) this;
        }

        public Criteria andDys590GreaterThan(String value) {
            addCriterion("dys590 >", value, "dys590");
            return (Criteria) this;
        }

        public Criteria andDys590GreaterThanOrEqualTo(String value) {
            addCriterion("dys590 >=", value, "dys590");
            return (Criteria) this;
        }

        public Criteria andDys590LessThan(String value) {
            addCriterion("dys590 <", value, "dys590");
            return (Criteria) this;
        }

        public Criteria andDys590LessThanOrEqualTo(String value) {
            addCriterion("dys590 <=", value, "dys590");
            return (Criteria) this;
        }

        public Criteria andDys590Like(String value) {
            addCriterion("dys590 like", value, "dys590");
            return (Criteria) this;
        }

        public Criteria andDys590NotLike(String value) {
            addCriterion("dys590 not like", value, "dys590");
            return (Criteria) this;
        }

        public Criteria andDys590In(List<String> values) {
            addCriterion("dys590 in", values, "dys590");
            return (Criteria) this;
        }

        public Criteria andDys590NotIn(List<String> values) {
            addCriterion("dys590 not in", values, "dys590");
            return (Criteria) this;
        }

        public Criteria andDys590Between(String value1, String value2) {
            addCriterion("dys590 between", value1, value2, "dys590");
            return (Criteria) this;
        }

        public Criteria andDys590NotBetween(String value1, String value2) {
            addCriterion("dys590 not between", value1, value2, "dys590");
            return (Criteria) this;
        }

        public Criteria andTsStrDys19IsNull() {
            addCriterion("ts_str_dys19 is null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys19IsNotNull() {
            addCriterion("ts_str_dys19 is not null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys19EqualTo(String value) {
            addCriterion("ts_str_dys19 =", value, "tsStrDys19");
            return (Criteria) this;
        }

        public Criteria andTsStrDys19NotEqualTo(String value) {
            addCriterion("ts_str_dys19 <>", value, "tsStrDys19");
            return (Criteria) this;
        }

        public Criteria andTsStrDys19GreaterThan(String value) {
            addCriterion("ts_str_dys19 >", value, "tsStrDys19");
            return (Criteria) this;
        }

        public Criteria andTsStrDys19GreaterThanOrEqualTo(String value) {
            addCriterion("ts_str_dys19 >=", value, "tsStrDys19");
            return (Criteria) this;
        }

        public Criteria andTsStrDys19LessThan(String value) {
            addCriterion("ts_str_dys19 <", value, "tsStrDys19");
            return (Criteria) this;
        }

        public Criteria andTsStrDys19LessThanOrEqualTo(String value) {
            addCriterion("ts_str_dys19 <=", value, "tsStrDys19");
            return (Criteria) this;
        }

        public Criteria andTsStrDys19Like(String value) {
            addCriterion("ts_str_dys19 like", value, "tsStrDys19");
            return (Criteria) this;
        }

        public Criteria andTsStrDys19NotLike(String value) {
            addCriterion("ts_str_dys19 not like", value, "tsStrDys19");
            return (Criteria) this;
        }

        public Criteria andTsStrDys19In(List<String> values) {
            addCriterion("ts_str_dys19 in", values, "tsStrDys19");
            return (Criteria) this;
        }

        public Criteria andTsStrDys19NotIn(List<String> values) {
            addCriterion("ts_str_dys19 not in", values, "tsStrDys19");
            return (Criteria) this;
        }

        public Criteria andTsStrDys19Between(String value1, String value2) {
            addCriterion("ts_str_dys19 between", value1, value2, "tsStrDys19");
            return (Criteria) this;
        }

        public Criteria andTsStrDys19NotBetween(String value1, String value2) {
            addCriterion("ts_str_dys19 not between", value1, value2, "tsStrDys19");
            return (Criteria) this;
        }

        public Criteria andTsStrDys359IsNull() {
            addCriterion("ts_str_dys359 is null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys359IsNotNull() {
            addCriterion("ts_str_dys359 is not null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys359EqualTo(String value) {
            addCriterion("ts_str_dys359 =", value, "tsStrDys359");
            return (Criteria) this;
        }

        public Criteria andTsStrDys359NotEqualTo(String value) {
            addCriterion("ts_str_dys359 <>", value, "tsStrDys359");
            return (Criteria) this;
        }

        public Criteria andTsStrDys359GreaterThan(String value) {
            addCriterion("ts_str_dys359 >", value, "tsStrDys359");
            return (Criteria) this;
        }

        public Criteria andTsStrDys359GreaterThanOrEqualTo(String value) {
            addCriterion("ts_str_dys359 >=", value, "tsStrDys359");
            return (Criteria) this;
        }

        public Criteria andTsStrDys359LessThan(String value) {
            addCriterion("ts_str_dys359 <", value, "tsStrDys359");
            return (Criteria) this;
        }

        public Criteria andTsStrDys359LessThanOrEqualTo(String value) {
            addCriterion("ts_str_dys359 <=", value, "tsStrDys359");
            return (Criteria) this;
        }

        public Criteria andTsStrDys359Like(String value) {
            addCriterion("ts_str_dys359 like", value, "tsStrDys359");
            return (Criteria) this;
        }

        public Criteria andTsStrDys359NotLike(String value) {
            addCriterion("ts_str_dys359 not like", value, "tsStrDys359");
            return (Criteria) this;
        }

        public Criteria andTsStrDys359In(List<String> values) {
            addCriterion("ts_str_dys359 in", values, "tsStrDys359");
            return (Criteria) this;
        }

        public Criteria andTsStrDys359NotIn(List<String> values) {
            addCriterion("ts_str_dys359 not in", values, "tsStrDys359");
            return (Criteria) this;
        }

        public Criteria andTsStrDys359Between(String value1, String value2) {
            addCriterion("ts_str_dys359 between", value1, value2, "tsStrDys359");
            return (Criteria) this;
        }

        public Criteria andTsStrDys359NotBetween(String value1, String value2) {
            addCriterion("ts_str_dys359 not between", value1, value2, "tsStrDys359");
            return (Criteria) this;
        }

        public Criteria andTsStrDys390IsNull() {
            addCriterion("ts_str_dys390 is null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys390IsNotNull() {
            addCriterion("ts_str_dys390 is not null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys390EqualTo(String value) {
            addCriterion("ts_str_dys390 =", value, "tsStrDys390");
            return (Criteria) this;
        }

        public Criteria andTsStrDys390NotEqualTo(String value) {
            addCriterion("ts_str_dys390 <>", value, "tsStrDys390");
            return (Criteria) this;
        }

        public Criteria andTsStrDys390GreaterThan(String value) {
            addCriterion("ts_str_dys390 >", value, "tsStrDys390");
            return (Criteria) this;
        }

        public Criteria andTsStrDys390GreaterThanOrEqualTo(String value) {
            addCriterion("ts_str_dys390 >=", value, "tsStrDys390");
            return (Criteria) this;
        }

        public Criteria andTsStrDys390LessThan(String value) {
            addCriterion("ts_str_dys390 <", value, "tsStrDys390");
            return (Criteria) this;
        }

        public Criteria andTsStrDys390LessThanOrEqualTo(String value) {
            addCriterion("ts_str_dys390 <=", value, "tsStrDys390");
            return (Criteria) this;
        }

        public Criteria andTsStrDys390Like(String value) {
            addCriterion("ts_str_dys390 like", value, "tsStrDys390");
            return (Criteria) this;
        }

        public Criteria andTsStrDys390NotLike(String value) {
            addCriterion("ts_str_dys390 not like", value, "tsStrDys390");
            return (Criteria) this;
        }

        public Criteria andTsStrDys390In(List<String> values) {
            addCriterion("ts_str_dys390 in", values, "tsStrDys390");
            return (Criteria) this;
        }

        public Criteria andTsStrDys390NotIn(List<String> values) {
            addCriterion("ts_str_dys390 not in", values, "tsStrDys390");
            return (Criteria) this;
        }

        public Criteria andTsStrDys390Between(String value1, String value2) {
            addCriterion("ts_str_dys390 between", value1, value2, "tsStrDys390");
            return (Criteria) this;
        }

        public Criteria andTsStrDys390NotBetween(String value1, String value2) {
            addCriterion("ts_str_dys390 not between", value1, value2, "tsStrDys390");
            return (Criteria) this;
        }

        public Criteria andTsStrDys391IsNull() {
            addCriterion("ts_str_dys391 is null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys391IsNotNull() {
            addCriterion("ts_str_dys391 is not null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys391EqualTo(String value) {
            addCriterion("ts_str_dys391 =", value, "tsStrDys391");
            return (Criteria) this;
        }

        public Criteria andTsStrDys391NotEqualTo(String value) {
            addCriterion("ts_str_dys391 <>", value, "tsStrDys391");
            return (Criteria) this;
        }

        public Criteria andTsStrDys391GreaterThan(String value) {
            addCriterion("ts_str_dys391 >", value, "tsStrDys391");
            return (Criteria) this;
        }

        public Criteria andTsStrDys391GreaterThanOrEqualTo(String value) {
            addCriterion("ts_str_dys391 >=", value, "tsStrDys391");
            return (Criteria) this;
        }

        public Criteria andTsStrDys391LessThan(String value) {
            addCriterion("ts_str_dys391 <", value, "tsStrDys391");
            return (Criteria) this;
        }

        public Criteria andTsStrDys391LessThanOrEqualTo(String value) {
            addCriterion("ts_str_dys391 <=", value, "tsStrDys391");
            return (Criteria) this;
        }

        public Criteria andTsStrDys391Like(String value) {
            addCriterion("ts_str_dys391 like", value, "tsStrDys391");
            return (Criteria) this;
        }

        public Criteria andTsStrDys391NotLike(String value) {
            addCriterion("ts_str_dys391 not like", value, "tsStrDys391");
            return (Criteria) this;
        }

        public Criteria andTsStrDys391In(List<String> values) {
            addCriterion("ts_str_dys391 in", values, "tsStrDys391");
            return (Criteria) this;
        }

        public Criteria andTsStrDys391NotIn(List<String> values) {
            addCriterion("ts_str_dys391 not in", values, "tsStrDys391");
            return (Criteria) this;
        }

        public Criteria andTsStrDys391Between(String value1, String value2) {
            addCriterion("ts_str_dys391 between", value1, value2, "tsStrDys391");
            return (Criteria) this;
        }

        public Criteria andTsStrDys391NotBetween(String value1, String value2) {
            addCriterion("ts_str_dys391 not between", value1, value2, "tsStrDys391");
            return (Criteria) this;
        }

        public Criteria andTsStrDys392IsNull() {
            addCriterion("ts_str_dys392 is null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys392IsNotNull() {
            addCriterion("ts_str_dys392 is not null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys392EqualTo(String value) {
            addCriterion("ts_str_dys392 =", value, "tsStrDys392");
            return (Criteria) this;
        }

        public Criteria andTsStrDys392NotEqualTo(String value) {
            addCriterion("ts_str_dys392 <>", value, "tsStrDys392");
            return (Criteria) this;
        }

        public Criteria andTsStrDys392GreaterThan(String value) {
            addCriterion("ts_str_dys392 >", value, "tsStrDys392");
            return (Criteria) this;
        }

        public Criteria andTsStrDys392GreaterThanOrEqualTo(String value) {
            addCriterion("ts_str_dys392 >=", value, "tsStrDys392");
            return (Criteria) this;
        }

        public Criteria andTsStrDys392LessThan(String value) {
            addCriterion("ts_str_dys392 <", value, "tsStrDys392");
            return (Criteria) this;
        }

        public Criteria andTsStrDys392LessThanOrEqualTo(String value) {
            addCriterion("ts_str_dys392 <=", value, "tsStrDys392");
            return (Criteria) this;
        }

        public Criteria andTsStrDys392Like(String value) {
            addCriterion("ts_str_dys392 like", value, "tsStrDys392");
            return (Criteria) this;
        }

        public Criteria andTsStrDys392NotLike(String value) {
            addCriterion("ts_str_dys392 not like", value, "tsStrDys392");
            return (Criteria) this;
        }

        public Criteria andTsStrDys392In(List<String> values) {
            addCriterion("ts_str_dys392 in", values, "tsStrDys392");
            return (Criteria) this;
        }

        public Criteria andTsStrDys392NotIn(List<String> values) {
            addCriterion("ts_str_dys392 not in", values, "tsStrDys392");
            return (Criteria) this;
        }

        public Criteria andTsStrDys392Between(String value1, String value2) {
            addCriterion("ts_str_dys392 between", value1, value2, "tsStrDys392");
            return (Criteria) this;
        }

        public Criteria andTsStrDys392NotBetween(String value1, String value2) {
            addCriterion("ts_str_dys392 not between", value1, value2, "tsStrDys392");
            return (Criteria) this;
        }

        public Criteria andTsStrDys393IsNull() {
            addCriterion("ts_str_dys393 is null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys393IsNotNull() {
            addCriterion("ts_str_dys393 is not null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys393EqualTo(String value) {
            addCriterion("ts_str_dys393 =", value, "tsStrDys393");
            return (Criteria) this;
        }

        public Criteria andTsStrDys393NotEqualTo(String value) {
            addCriterion("ts_str_dys393 <>", value, "tsStrDys393");
            return (Criteria) this;
        }

        public Criteria andTsStrDys393GreaterThan(String value) {
            addCriterion("ts_str_dys393 >", value, "tsStrDys393");
            return (Criteria) this;
        }

        public Criteria andTsStrDys393GreaterThanOrEqualTo(String value) {
            addCriterion("ts_str_dys393 >=", value, "tsStrDys393");
            return (Criteria) this;
        }

        public Criteria andTsStrDys393LessThan(String value) {
            addCriterion("ts_str_dys393 <", value, "tsStrDys393");
            return (Criteria) this;
        }

        public Criteria andTsStrDys393LessThanOrEqualTo(String value) {
            addCriterion("ts_str_dys393 <=", value, "tsStrDys393");
            return (Criteria) this;
        }

        public Criteria andTsStrDys393Like(String value) {
            addCriterion("ts_str_dys393 like", value, "tsStrDys393");
            return (Criteria) this;
        }

        public Criteria andTsStrDys393NotLike(String value) {
            addCriterion("ts_str_dys393 not like", value, "tsStrDys393");
            return (Criteria) this;
        }

        public Criteria andTsStrDys393In(List<String> values) {
            addCriterion("ts_str_dys393 in", values, "tsStrDys393");
            return (Criteria) this;
        }

        public Criteria andTsStrDys393NotIn(List<String> values) {
            addCriterion("ts_str_dys393 not in", values, "tsStrDys393");
            return (Criteria) this;
        }

        public Criteria andTsStrDys393Between(String value1, String value2) {
            addCriterion("ts_str_dys393 between", value1, value2, "tsStrDys393");
            return (Criteria) this;
        }

        public Criteria andTsStrDys393NotBetween(String value1, String value2) {
            addCriterion("ts_str_dys393 not between", value1, value2, "tsStrDys393");
            return (Criteria) this;
        }

        public Criteria andTsStrDys437IsNull() {
            addCriterion("ts_str_dys437 is null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys437IsNotNull() {
            addCriterion("ts_str_dys437 is not null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys437EqualTo(String value) {
            addCriterion("ts_str_dys437 =", value, "tsStrDys437");
            return (Criteria) this;
        }

        public Criteria andTsStrDys437NotEqualTo(String value) {
            addCriterion("ts_str_dys437 <>", value, "tsStrDys437");
            return (Criteria) this;
        }

        public Criteria andTsStrDys437GreaterThan(String value) {
            addCriterion("ts_str_dys437 >", value, "tsStrDys437");
            return (Criteria) this;
        }

        public Criteria andTsStrDys437GreaterThanOrEqualTo(String value) {
            addCriterion("ts_str_dys437 >=", value, "tsStrDys437");
            return (Criteria) this;
        }

        public Criteria andTsStrDys437LessThan(String value) {
            addCriterion("ts_str_dys437 <", value, "tsStrDys437");
            return (Criteria) this;
        }

        public Criteria andTsStrDys437LessThanOrEqualTo(String value) {
            addCriterion("ts_str_dys437 <=", value, "tsStrDys437");
            return (Criteria) this;
        }

        public Criteria andTsStrDys437Like(String value) {
            addCriterion("ts_str_dys437 like", value, "tsStrDys437");
            return (Criteria) this;
        }

        public Criteria andTsStrDys437NotLike(String value) {
            addCriterion("ts_str_dys437 not like", value, "tsStrDys437");
            return (Criteria) this;
        }

        public Criteria andTsStrDys437In(List<String> values) {
            addCriterion("ts_str_dys437 in", values, "tsStrDys437");
            return (Criteria) this;
        }

        public Criteria andTsStrDys437NotIn(List<String> values) {
            addCriterion("ts_str_dys437 not in", values, "tsStrDys437");
            return (Criteria) this;
        }

        public Criteria andTsStrDys437Between(String value1, String value2) {
            addCriterion("ts_str_dys437 between", value1, value2, "tsStrDys437");
            return (Criteria) this;
        }

        public Criteria andTsStrDys437NotBetween(String value1, String value2) {
            addCriterion("ts_str_dys437 not between", value1, value2, "tsStrDys437");
            return (Criteria) this;
        }

        public Criteria andTsStrDys635IsNull() {
            addCriterion("ts_str_dys635 is null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys635IsNotNull() {
            addCriterion("ts_str_dys635 is not null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys635EqualTo(String value) {
            addCriterion("ts_str_dys635 =", value, "tsStrDys635");
            return (Criteria) this;
        }

        public Criteria andTsStrDys635NotEqualTo(String value) {
            addCriterion("ts_str_dys635 <>", value, "tsStrDys635");
            return (Criteria) this;
        }

        public Criteria andTsStrDys635GreaterThan(String value) {
            addCriterion("ts_str_dys635 >", value, "tsStrDys635");
            return (Criteria) this;
        }

        public Criteria andTsStrDys635GreaterThanOrEqualTo(String value) {
            addCriterion("ts_str_dys635 >=", value, "tsStrDys635");
            return (Criteria) this;
        }

        public Criteria andTsStrDys635LessThan(String value) {
            addCriterion("ts_str_dys635 <", value, "tsStrDys635");
            return (Criteria) this;
        }

        public Criteria andTsStrDys635LessThanOrEqualTo(String value) {
            addCriterion("ts_str_dys635 <=", value, "tsStrDys635");
            return (Criteria) this;
        }

        public Criteria andTsStrDys635Like(String value) {
            addCriterion("ts_str_dys635 like", value, "tsStrDys635");
            return (Criteria) this;
        }

        public Criteria andTsStrDys635NotLike(String value) {
            addCriterion("ts_str_dys635 not like", value, "tsStrDys635");
            return (Criteria) this;
        }

        public Criteria andTsStrDys635In(List<String> values) {
            addCriterion("ts_str_dys635 in", values, "tsStrDys635");
            return (Criteria) this;
        }

        public Criteria andTsStrDys635NotIn(List<String> values) {
            addCriterion("ts_str_dys635 not in", values, "tsStrDys635");
            return (Criteria) this;
        }

        public Criteria andTsStrDys635Between(String value1, String value2) {
            addCriterion("ts_str_dys635 between", value1, value2, "tsStrDys635");
            return (Criteria) this;
        }

        public Criteria andTsStrDys635NotBetween(String value1, String value2) {
            addCriterion("ts_str_dys635 not between", value1, value2, "tsStrDys635");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590AIsNull() {
            addCriterion("ts_str_dys590_a is null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590AIsNotNull() {
            addCriterion("ts_str_dys590_a is not null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590AEqualTo(String value) {
            addCriterion("ts_str_dys590_a =", value, "tsStrDys590A");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590ANotEqualTo(String value) {
            addCriterion("ts_str_dys590_a <>", value, "tsStrDys590A");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590AGreaterThan(String value) {
            addCriterion("ts_str_dys590_a >", value, "tsStrDys590A");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590AGreaterThanOrEqualTo(String value) {
            addCriterion("ts_str_dys590_a >=", value, "tsStrDys590A");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590ALessThan(String value) {
            addCriterion("ts_str_dys590_a <", value, "tsStrDys590A");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590ALessThanOrEqualTo(String value) {
            addCriterion("ts_str_dys590_a <=", value, "tsStrDys590A");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590ALike(String value) {
            addCriterion("ts_str_dys590_a like", value, "tsStrDys590A");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590ANotLike(String value) {
            addCriterion("ts_str_dys590_a not like", value, "tsStrDys590A");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590AIn(List<String> values) {
            addCriterion("ts_str_dys590_a in", values, "tsStrDys590A");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590ANotIn(List<String> values) {
            addCriterion("ts_str_dys590_a not in", values, "tsStrDys590A");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590ABetween(String value1, String value2) {
            addCriterion("ts_str_dys590_a between", value1, value2, "tsStrDys590A");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590ANotBetween(String value1, String value2) {
            addCriterion("ts_str_dys590_a not between", value1, value2, "tsStrDys590A");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590BIsNull() {
            addCriterion("ts_str_dys590_b is null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590BIsNotNull() {
            addCriterion("ts_str_dys590_b is not null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590BEqualTo(String value) {
            addCriterion("ts_str_dys590_b =", value, "tsStrDys590B");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590BNotEqualTo(String value) {
            addCriterion("ts_str_dys590_b <>", value, "tsStrDys590B");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590BGreaterThan(String value) {
            addCriterion("ts_str_dys590_b >", value, "tsStrDys590B");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590BGreaterThanOrEqualTo(String value) {
            addCriterion("ts_str_dys590_b >=", value, "tsStrDys590B");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590BLessThan(String value) {
            addCriterion("ts_str_dys590_b <", value, "tsStrDys590B");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590BLessThanOrEqualTo(String value) {
            addCriterion("ts_str_dys590_b <=", value, "tsStrDys590B");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590BLike(String value) {
            addCriterion("ts_str_dys590_b like", value, "tsStrDys590B");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590BNotLike(String value) {
            addCriterion("ts_str_dys590_b not like", value, "tsStrDys590B");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590BIn(List<String> values) {
            addCriterion("ts_str_dys590_b in", values, "tsStrDys590B");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590BNotIn(List<String> values) {
            addCriterion("ts_str_dys590_b not in", values, "tsStrDys590B");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590BBetween(String value1, String value2) {
            addCriterion("ts_str_dys590_b between", value1, value2, "tsStrDys590B");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590BNotBetween(String value1, String value2) {
            addCriterion("ts_str_dys590_b not between", value1, value2, "tsStrDys590B");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590CIsNull() {
            addCriterion("ts_str_dys590_c is null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590CIsNotNull() {
            addCriterion("ts_str_dys590_c is not null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590CEqualTo(String value) {
            addCriterion("ts_str_dys590_c =", value, "tsStrDys590C");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590CNotEqualTo(String value) {
            addCriterion("ts_str_dys590_c <>", value, "tsStrDys590C");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590CGreaterThan(String value) {
            addCriterion("ts_str_dys590_c >", value, "tsStrDys590C");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590CGreaterThanOrEqualTo(String value) {
            addCriterion("ts_str_dys590_c >=", value, "tsStrDys590C");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590CLessThan(String value) {
            addCriterion("ts_str_dys590_c <", value, "tsStrDys590C");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590CLessThanOrEqualTo(String value) {
            addCriterion("ts_str_dys590_c <=", value, "tsStrDys590C");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590CLike(String value) {
            addCriterion("ts_str_dys590_c like", value, "tsStrDys590C");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590CNotLike(String value) {
            addCriterion("ts_str_dys590_c not like", value, "tsStrDys590C");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590CIn(List<String> values) {
            addCriterion("ts_str_dys590_c in", values, "tsStrDys590C");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590CNotIn(List<String> values) {
            addCriterion("ts_str_dys590_c not in", values, "tsStrDys590C");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590CBetween(String value1, String value2) {
            addCriterion("ts_str_dys590_c between", value1, value2, "tsStrDys590C");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590CNotBetween(String value1, String value2) {
            addCriterion("ts_str_dys590_c not between", value1, value2, "tsStrDys590C");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590DIsNull() {
            addCriterion("ts_str_dys590_d is null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590DIsNotNull() {
            addCriterion("ts_str_dys590_d is not null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590DEqualTo(String value) {
            addCriterion("ts_str_dys590_d =", value, "tsStrDys590D");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590DNotEqualTo(String value) {
            addCriterion("ts_str_dys590_d <>", value, "tsStrDys590D");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590DGreaterThan(String value) {
            addCriterion("ts_str_dys590_d >", value, "tsStrDys590D");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590DGreaterThanOrEqualTo(String value) {
            addCriterion("ts_str_dys590_d >=", value, "tsStrDys590D");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590DLessThan(String value) {
            addCriterion("ts_str_dys590_d <", value, "tsStrDys590D");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590DLessThanOrEqualTo(String value) {
            addCriterion("ts_str_dys590_d <=", value, "tsStrDys590D");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590DLike(String value) {
            addCriterion("ts_str_dys590_d like", value, "tsStrDys590D");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590DNotLike(String value) {
            addCriterion("ts_str_dys590_d not like", value, "tsStrDys590D");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590DIn(List<String> values) {
            addCriterion("ts_str_dys590_d in", values, "tsStrDys590D");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590DNotIn(List<String> values) {
            addCriterion("ts_str_dys590_d not in", values, "tsStrDys590D");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590DBetween(String value1, String value2) {
            addCriterion("ts_str_dys590_d between", value1, value2, "tsStrDys590D");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590DNotBetween(String value1, String value2) {
            addCriterion("ts_str_dys590_d not between", value1, value2, "tsStrDys590D");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590EIsNull() {
            addCriterion("ts_str_dys590_e is null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590EIsNotNull() {
            addCriterion("ts_str_dys590_e is not null");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590EEqualTo(String value) {
            addCriterion("ts_str_dys590_e =", value, "tsStrDys590E");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590ENotEqualTo(String value) {
            addCriterion("ts_str_dys590_e <>", value, "tsStrDys590E");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590EGreaterThan(String value) {
            addCriterion("ts_str_dys590_e >", value, "tsStrDys590E");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590EGreaterThanOrEqualTo(String value) {
            addCriterion("ts_str_dys590_e >=", value, "tsStrDys590E");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590ELessThan(String value) {
            addCriterion("ts_str_dys590_e <", value, "tsStrDys590E");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590ELessThanOrEqualTo(String value) {
            addCriterion("ts_str_dys590_e <=", value, "tsStrDys590E");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590ELike(String value) {
            addCriterion("ts_str_dys590_e like", value, "tsStrDys590E");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590ENotLike(String value) {
            addCriterion("ts_str_dys590_e not like", value, "tsStrDys590E");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590EIn(List<String> values) {
            addCriterion("ts_str_dys590_e in", values, "tsStrDys590E");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590ENotIn(List<String> values) {
            addCriterion("ts_str_dys590_e not in", values, "tsStrDys590E");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590EBetween(String value1, String value2) {
            addCriterion("ts_str_dys590_e between", value1, value2, "tsStrDys590E");
            return (Criteria) this;
        }

        public Criteria andTsStrDys590ENotBetween(String value1, String value2) {
            addCriterion("ts_str_dys590_e not between", value1, value2, "tsStrDys590E");
            return (Criteria) this;
        }

        public Criteria andTxStrDys19IsNull() {
            addCriterion("tx_str_dys19 is null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys19IsNotNull() {
            addCriterion("tx_str_dys19 is not null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys19EqualTo(String value) {
            addCriterion("tx_str_dys19 =", value, "txStrDys19");
            return (Criteria) this;
        }

        public Criteria andTxStrDys19NotEqualTo(String value) {
            addCriterion("tx_str_dys19 <>", value, "txStrDys19");
            return (Criteria) this;
        }

        public Criteria andTxStrDys19GreaterThan(String value) {
            addCriterion("tx_str_dys19 >", value, "txStrDys19");
            return (Criteria) this;
        }

        public Criteria andTxStrDys19GreaterThanOrEqualTo(String value) {
            addCriterion("tx_str_dys19 >=", value, "txStrDys19");
            return (Criteria) this;
        }

        public Criteria andTxStrDys19LessThan(String value) {
            addCriterion("tx_str_dys19 <", value, "txStrDys19");
            return (Criteria) this;
        }

        public Criteria andTxStrDys19LessThanOrEqualTo(String value) {
            addCriterion("tx_str_dys19 <=", value, "txStrDys19");
            return (Criteria) this;
        }

        public Criteria andTxStrDys19Like(String value) {
            addCriterion("tx_str_dys19 like", value, "txStrDys19");
            return (Criteria) this;
        }

        public Criteria andTxStrDys19NotLike(String value) {
            addCriterion("tx_str_dys19 not like", value, "txStrDys19");
            return (Criteria) this;
        }

        public Criteria andTxStrDys19In(List<String> values) {
            addCriterion("tx_str_dys19 in", values, "txStrDys19");
            return (Criteria) this;
        }

        public Criteria andTxStrDys19NotIn(List<String> values) {
            addCriterion("tx_str_dys19 not in", values, "txStrDys19");
            return (Criteria) this;
        }

        public Criteria andTxStrDys19Between(String value1, String value2) {
            addCriterion("tx_str_dys19 between", value1, value2, "txStrDys19");
            return (Criteria) this;
        }

        public Criteria andTxStrDys19NotBetween(String value1, String value2) {
            addCriterion("tx_str_dys19 not between", value1, value2, "txStrDys19");
            return (Criteria) this;
        }

        public Criteria andTxStrDys359IsNull() {
            addCriterion("tx_str_dys359 is null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys359IsNotNull() {
            addCriterion("tx_str_dys359 is not null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys359EqualTo(String value) {
            addCriterion("tx_str_dys359 =", value, "txStrDys359");
            return (Criteria) this;
        }

        public Criteria andTxStrDys359NotEqualTo(String value) {
            addCriterion("tx_str_dys359 <>", value, "txStrDys359");
            return (Criteria) this;
        }

        public Criteria andTxStrDys359GreaterThan(String value) {
            addCriterion("tx_str_dys359 >", value, "txStrDys359");
            return (Criteria) this;
        }

        public Criteria andTxStrDys359GreaterThanOrEqualTo(String value) {
            addCriterion("tx_str_dys359 >=", value, "txStrDys359");
            return (Criteria) this;
        }

        public Criteria andTxStrDys359LessThan(String value) {
            addCriterion("tx_str_dys359 <", value, "txStrDys359");
            return (Criteria) this;
        }

        public Criteria andTxStrDys359LessThanOrEqualTo(String value) {
            addCriterion("tx_str_dys359 <=", value, "txStrDys359");
            return (Criteria) this;
        }

        public Criteria andTxStrDys359Like(String value) {
            addCriterion("tx_str_dys359 like", value, "txStrDys359");
            return (Criteria) this;
        }

        public Criteria andTxStrDys359NotLike(String value) {
            addCriterion("tx_str_dys359 not like", value, "txStrDys359");
            return (Criteria) this;
        }

        public Criteria andTxStrDys359In(List<String> values) {
            addCriterion("tx_str_dys359 in", values, "txStrDys359");
            return (Criteria) this;
        }

        public Criteria andTxStrDys359NotIn(List<String> values) {
            addCriterion("tx_str_dys359 not in", values, "txStrDys359");
            return (Criteria) this;
        }

        public Criteria andTxStrDys359Between(String value1, String value2) {
            addCriterion("tx_str_dys359 between", value1, value2, "txStrDys359");
            return (Criteria) this;
        }

        public Criteria andTxStrDys359NotBetween(String value1, String value2) {
            addCriterion("tx_str_dys359 not between", value1, value2, "txStrDys359");
            return (Criteria) this;
        }

        public Criteria andTxStrDys390IsNull() {
            addCriterion("tx_str_dys390 is null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys390IsNotNull() {
            addCriterion("tx_str_dys390 is not null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys390EqualTo(String value) {
            addCriterion("tx_str_dys390 =", value, "txStrDys390");
            return (Criteria) this;
        }

        public Criteria andTxStrDys390NotEqualTo(String value) {
            addCriterion("tx_str_dys390 <>", value, "txStrDys390");
            return (Criteria) this;
        }

        public Criteria andTxStrDys390GreaterThan(String value) {
            addCriterion("tx_str_dys390 >", value, "txStrDys390");
            return (Criteria) this;
        }

        public Criteria andTxStrDys390GreaterThanOrEqualTo(String value) {
            addCriterion("tx_str_dys390 >=", value, "txStrDys390");
            return (Criteria) this;
        }

        public Criteria andTxStrDys390LessThan(String value) {
            addCriterion("tx_str_dys390 <", value, "txStrDys390");
            return (Criteria) this;
        }

        public Criteria andTxStrDys390LessThanOrEqualTo(String value) {
            addCriterion("tx_str_dys390 <=", value, "txStrDys390");
            return (Criteria) this;
        }

        public Criteria andTxStrDys390Like(String value) {
            addCriterion("tx_str_dys390 like", value, "txStrDys390");
            return (Criteria) this;
        }

        public Criteria andTxStrDys390NotLike(String value) {
            addCriterion("tx_str_dys390 not like", value, "txStrDys390");
            return (Criteria) this;
        }

        public Criteria andTxStrDys390In(List<String> values) {
            addCriterion("tx_str_dys390 in", values, "txStrDys390");
            return (Criteria) this;
        }

        public Criteria andTxStrDys390NotIn(List<String> values) {
            addCriterion("tx_str_dys390 not in", values, "txStrDys390");
            return (Criteria) this;
        }

        public Criteria andTxStrDys390Between(String value1, String value2) {
            addCriterion("tx_str_dys390 between", value1, value2, "txStrDys390");
            return (Criteria) this;
        }

        public Criteria andTxStrDys390NotBetween(String value1, String value2) {
            addCriterion("tx_str_dys390 not between", value1, value2, "txStrDys390");
            return (Criteria) this;
        }

        public Criteria andTxStrDys391IsNull() {
            addCriterion("tx_str_dys391 is null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys391IsNotNull() {
            addCriterion("tx_str_dys391 is not null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys391EqualTo(String value) {
            addCriterion("tx_str_dys391 =", value, "txStrDys391");
            return (Criteria) this;
        }

        public Criteria andTxStrDys391NotEqualTo(String value) {
            addCriterion("tx_str_dys391 <>", value, "txStrDys391");
            return (Criteria) this;
        }

        public Criteria andTxStrDys391GreaterThan(String value) {
            addCriterion("tx_str_dys391 >", value, "txStrDys391");
            return (Criteria) this;
        }

        public Criteria andTxStrDys391GreaterThanOrEqualTo(String value) {
            addCriterion("tx_str_dys391 >=", value, "txStrDys391");
            return (Criteria) this;
        }

        public Criteria andTxStrDys391LessThan(String value) {
            addCriterion("tx_str_dys391 <", value, "txStrDys391");
            return (Criteria) this;
        }

        public Criteria andTxStrDys391LessThanOrEqualTo(String value) {
            addCriterion("tx_str_dys391 <=", value, "txStrDys391");
            return (Criteria) this;
        }

        public Criteria andTxStrDys391Like(String value) {
            addCriterion("tx_str_dys391 like", value, "txStrDys391");
            return (Criteria) this;
        }

        public Criteria andTxStrDys391NotLike(String value) {
            addCriterion("tx_str_dys391 not like", value, "txStrDys391");
            return (Criteria) this;
        }

        public Criteria andTxStrDys391In(List<String> values) {
            addCriterion("tx_str_dys391 in", values, "txStrDys391");
            return (Criteria) this;
        }

        public Criteria andTxStrDys391NotIn(List<String> values) {
            addCriterion("tx_str_dys391 not in", values, "txStrDys391");
            return (Criteria) this;
        }

        public Criteria andTxStrDys391Between(String value1, String value2) {
            addCriterion("tx_str_dys391 between", value1, value2, "txStrDys391");
            return (Criteria) this;
        }

        public Criteria andTxStrDys391NotBetween(String value1, String value2) {
            addCriterion("tx_str_dys391 not between", value1, value2, "txStrDys391");
            return (Criteria) this;
        }

        public Criteria andTxStrDys392IsNull() {
            addCriterion("tx_str_dys392 is null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys392IsNotNull() {
            addCriterion("tx_str_dys392 is not null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys392EqualTo(String value) {
            addCriterion("tx_str_dys392 =", value, "txStrDys392");
            return (Criteria) this;
        }

        public Criteria andTxStrDys392NotEqualTo(String value) {
            addCriterion("tx_str_dys392 <>", value, "txStrDys392");
            return (Criteria) this;
        }

        public Criteria andTxStrDys392GreaterThan(String value) {
            addCriterion("tx_str_dys392 >", value, "txStrDys392");
            return (Criteria) this;
        }

        public Criteria andTxStrDys392GreaterThanOrEqualTo(String value) {
            addCriterion("tx_str_dys392 >=", value, "txStrDys392");
            return (Criteria) this;
        }

        public Criteria andTxStrDys392LessThan(String value) {
            addCriterion("tx_str_dys392 <", value, "txStrDys392");
            return (Criteria) this;
        }

        public Criteria andTxStrDys392LessThanOrEqualTo(String value) {
            addCriterion("tx_str_dys392 <=", value, "txStrDys392");
            return (Criteria) this;
        }

        public Criteria andTxStrDys392Like(String value) {
            addCriterion("tx_str_dys392 like", value, "txStrDys392");
            return (Criteria) this;
        }

        public Criteria andTxStrDys392NotLike(String value) {
            addCriterion("tx_str_dys392 not like", value, "txStrDys392");
            return (Criteria) this;
        }

        public Criteria andTxStrDys392In(List<String> values) {
            addCriterion("tx_str_dys392 in", values, "txStrDys392");
            return (Criteria) this;
        }

        public Criteria andTxStrDys392NotIn(List<String> values) {
            addCriterion("tx_str_dys392 not in", values, "txStrDys392");
            return (Criteria) this;
        }

        public Criteria andTxStrDys392Between(String value1, String value2) {
            addCriterion("tx_str_dys392 between", value1, value2, "txStrDys392");
            return (Criteria) this;
        }

        public Criteria andTxStrDys392NotBetween(String value1, String value2) {
            addCriterion("tx_str_dys392 not between", value1, value2, "txStrDys392");
            return (Criteria) this;
        }

        public Criteria andTxStrDys393IsNull() {
            addCriterion("tx_str_dys393 is null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys393IsNotNull() {
            addCriterion("tx_str_dys393 is not null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys393EqualTo(String value) {
            addCriterion("tx_str_dys393 =", value, "txStrDys393");
            return (Criteria) this;
        }

        public Criteria andTxStrDys393NotEqualTo(String value) {
            addCriterion("tx_str_dys393 <>", value, "txStrDys393");
            return (Criteria) this;
        }

        public Criteria andTxStrDys393GreaterThan(String value) {
            addCriterion("tx_str_dys393 >", value, "txStrDys393");
            return (Criteria) this;
        }

        public Criteria andTxStrDys393GreaterThanOrEqualTo(String value) {
            addCriterion("tx_str_dys393 >=", value, "txStrDys393");
            return (Criteria) this;
        }

        public Criteria andTxStrDys393LessThan(String value) {
            addCriterion("tx_str_dys393 <", value, "txStrDys393");
            return (Criteria) this;
        }

        public Criteria andTxStrDys393LessThanOrEqualTo(String value) {
            addCriterion("tx_str_dys393 <=", value, "txStrDys393");
            return (Criteria) this;
        }

        public Criteria andTxStrDys393Like(String value) {
            addCriterion("tx_str_dys393 like", value, "txStrDys393");
            return (Criteria) this;
        }

        public Criteria andTxStrDys393NotLike(String value) {
            addCriterion("tx_str_dys393 not like", value, "txStrDys393");
            return (Criteria) this;
        }

        public Criteria andTxStrDys393In(List<String> values) {
            addCriterion("tx_str_dys393 in", values, "txStrDys393");
            return (Criteria) this;
        }

        public Criteria andTxStrDys393NotIn(List<String> values) {
            addCriterion("tx_str_dys393 not in", values, "txStrDys393");
            return (Criteria) this;
        }

        public Criteria andTxStrDys393Between(String value1, String value2) {
            addCriterion("tx_str_dys393 between", value1, value2, "txStrDys393");
            return (Criteria) this;
        }

        public Criteria andTxStrDys393NotBetween(String value1, String value2) {
            addCriterion("tx_str_dys393 not between", value1, value2, "txStrDys393");
            return (Criteria) this;
        }

        public Criteria andTxStrDys437IsNull() {
            addCriterion("tx_str_dys437 is null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys437IsNotNull() {
            addCriterion("tx_str_dys437 is not null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys437EqualTo(String value) {
            addCriterion("tx_str_dys437 =", value, "txStrDys437");
            return (Criteria) this;
        }

        public Criteria andTxStrDys437NotEqualTo(String value) {
            addCriterion("tx_str_dys437 <>", value, "txStrDys437");
            return (Criteria) this;
        }

        public Criteria andTxStrDys437GreaterThan(String value) {
            addCriterion("tx_str_dys437 >", value, "txStrDys437");
            return (Criteria) this;
        }

        public Criteria andTxStrDys437GreaterThanOrEqualTo(String value) {
            addCriterion("tx_str_dys437 >=", value, "txStrDys437");
            return (Criteria) this;
        }

        public Criteria andTxStrDys437LessThan(String value) {
            addCriterion("tx_str_dys437 <", value, "txStrDys437");
            return (Criteria) this;
        }

        public Criteria andTxStrDys437LessThanOrEqualTo(String value) {
            addCriterion("tx_str_dys437 <=", value, "txStrDys437");
            return (Criteria) this;
        }

        public Criteria andTxStrDys437Like(String value) {
            addCriterion("tx_str_dys437 like", value, "txStrDys437");
            return (Criteria) this;
        }

        public Criteria andTxStrDys437NotLike(String value) {
            addCriterion("tx_str_dys437 not like", value, "txStrDys437");
            return (Criteria) this;
        }

        public Criteria andTxStrDys437In(List<String> values) {
            addCriterion("tx_str_dys437 in", values, "txStrDys437");
            return (Criteria) this;
        }

        public Criteria andTxStrDys437NotIn(List<String> values) {
            addCriterion("tx_str_dys437 not in", values, "txStrDys437");
            return (Criteria) this;
        }

        public Criteria andTxStrDys437Between(String value1, String value2) {
            addCriterion("tx_str_dys437 between", value1, value2, "txStrDys437");
            return (Criteria) this;
        }

        public Criteria andTxStrDys437NotBetween(String value1, String value2) {
            addCriterion("tx_str_dys437 not between", value1, value2, "txStrDys437");
            return (Criteria) this;
        }

        public Criteria andTxStrDys635IsNull() {
            addCriterion("tx_str_dys635 is null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys635IsNotNull() {
            addCriterion("tx_str_dys635 is not null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys635EqualTo(String value) {
            addCriterion("tx_str_dys635 =", value, "txStrDys635");
            return (Criteria) this;
        }

        public Criteria andTxStrDys635NotEqualTo(String value) {
            addCriterion("tx_str_dys635 <>", value, "txStrDys635");
            return (Criteria) this;
        }

        public Criteria andTxStrDys635GreaterThan(String value) {
            addCriterion("tx_str_dys635 >", value, "txStrDys635");
            return (Criteria) this;
        }

        public Criteria andTxStrDys635GreaterThanOrEqualTo(String value) {
            addCriterion("tx_str_dys635 >=", value, "txStrDys635");
            return (Criteria) this;
        }

        public Criteria andTxStrDys635LessThan(String value) {
            addCriterion("tx_str_dys635 <", value, "txStrDys635");
            return (Criteria) this;
        }

        public Criteria andTxStrDys635LessThanOrEqualTo(String value) {
            addCriterion("tx_str_dys635 <=", value, "txStrDys635");
            return (Criteria) this;
        }

        public Criteria andTxStrDys635Like(String value) {
            addCriterion("tx_str_dys635 like", value, "txStrDys635");
            return (Criteria) this;
        }

        public Criteria andTxStrDys635NotLike(String value) {
            addCriterion("tx_str_dys635 not like", value, "txStrDys635");
            return (Criteria) this;
        }

        public Criteria andTxStrDys635In(List<String> values) {
            addCriterion("tx_str_dys635 in", values, "txStrDys635");
            return (Criteria) this;
        }

        public Criteria andTxStrDys635NotIn(List<String> values) {
            addCriterion("tx_str_dys635 not in", values, "txStrDys635");
            return (Criteria) this;
        }

        public Criteria andTxStrDys635Between(String value1, String value2) {
            addCriterion("tx_str_dys635 between", value1, value2, "txStrDys635");
            return (Criteria) this;
        }

        public Criteria andTxStrDys635NotBetween(String value1, String value2) {
            addCriterion("tx_str_dys635 not between", value1, value2, "txStrDys635");
            return (Criteria) this;
        }

        public Criteria andTxStrDys590IsNull() {
            addCriterion("tx_str_dys590 is null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys590IsNotNull() {
            addCriterion("tx_str_dys590 is not null");
            return (Criteria) this;
        }

        public Criteria andTxStrDys590EqualTo(String value) {
            addCriterion("tx_str_dys590 =", value, "txStrDys590");
            return (Criteria) this;
        }

        public Criteria andTxStrDys590NotEqualTo(String value) {
            addCriterion("tx_str_dys590 <>", value, "txStrDys590");
            return (Criteria) this;
        }

        public Criteria andTxStrDys590GreaterThan(String value) {
            addCriterion("tx_str_dys590 >", value, "txStrDys590");
            return (Criteria) this;
        }

        public Criteria andTxStrDys590GreaterThanOrEqualTo(String value) {
            addCriterion("tx_str_dys590 >=", value, "txStrDys590");
            return (Criteria) this;
        }

        public Criteria andTxStrDys590LessThan(String value) {
            addCriterion("tx_str_dys590 <", value, "txStrDys590");
            return (Criteria) this;
        }

        public Criteria andTxStrDys590LessThanOrEqualTo(String value) {
            addCriterion("tx_str_dys590 <=", value, "txStrDys590");
            return (Criteria) this;
        }

        public Criteria andTxStrDys590Like(String value) {
            addCriterion("tx_str_dys590 like", value, "txStrDys590");
            return (Criteria) this;
        }

        public Criteria andTxStrDys590NotLike(String value) {
            addCriterion("tx_str_dys590 not like", value, "txStrDys590");
            return (Criteria) this;
        }

        public Criteria andTxStrDys590In(List<String> values) {
            addCriterion("tx_str_dys590 in", values, "txStrDys590");
            return (Criteria) this;
        }

        public Criteria andTxStrDys590NotIn(List<String> values) {
            addCriterion("tx_str_dys590 not in", values, "txStrDys590");
            return (Criteria) this;
        }

        public Criteria andTxStrDys590Between(String value1, String value2) {
            addCriterion("tx_str_dys590 between", value1, value2, "txStrDys590");
            return (Criteria) this;
        }

        public Criteria andTxStrDys590NotBetween(String value1, String value2) {
            addCriterion("tx_str_dys590 not between", value1, value2, "txStrDys590");
            return (Criteria) this;
        }

        public Criteria andOtherDys19IsNull() {
            addCriterion("other_dys19 is null");
            return (Criteria) this;
        }

        public Criteria andOtherDys19IsNotNull() {
            addCriterion("other_dys19 is not null");
            return (Criteria) this;
        }

        public Criteria andOtherDys19EqualTo(String value) {
            addCriterion("other_dys19 =", value, "otherDys19");
            return (Criteria) this;
        }

        public Criteria andOtherDys19NotEqualTo(String value) {
            addCriterion("other_dys19 <>", value, "otherDys19");
            return (Criteria) this;
        }

        public Criteria andOtherDys19GreaterThan(String value) {
            addCriterion("other_dys19 >", value, "otherDys19");
            return (Criteria) this;
        }

        public Criteria andOtherDys19GreaterThanOrEqualTo(String value) {
            addCriterion("other_dys19 >=", value, "otherDys19");
            return (Criteria) this;
        }

        public Criteria andOtherDys19LessThan(String value) {
            addCriterion("other_dys19 <", value, "otherDys19");
            return (Criteria) this;
        }

        public Criteria andOtherDys19LessThanOrEqualTo(String value) {
            addCriterion("other_dys19 <=", value, "otherDys19");
            return (Criteria) this;
        }

        public Criteria andOtherDys19Like(String value) {
            addCriterion("other_dys19 like", value, "otherDys19");
            return (Criteria) this;
        }

        public Criteria andOtherDys19NotLike(String value) {
            addCriterion("other_dys19 not like", value, "otherDys19");
            return (Criteria) this;
        }

        public Criteria andOtherDys19In(List<String> values) {
            addCriterion("other_dys19 in", values, "otherDys19");
            return (Criteria) this;
        }

        public Criteria andOtherDys19NotIn(List<String> values) {
            addCriterion("other_dys19 not in", values, "otherDys19");
            return (Criteria) this;
        }

        public Criteria andOtherDys19Between(String value1, String value2) {
            addCriterion("other_dys19 between", value1, value2, "otherDys19");
            return (Criteria) this;
        }

        public Criteria andOtherDys19NotBetween(String value1, String value2) {
            addCriterion("other_dys19 not between", value1, value2, "otherDys19");
            return (Criteria) this;
        }

        public Criteria andOtherDys359IsNull() {
            addCriterion("other_dys359 is null");
            return (Criteria) this;
        }

        public Criteria andOtherDys359IsNotNull() {
            addCriterion("other_dys359 is not null");
            return (Criteria) this;
        }

        public Criteria andOtherDys359EqualTo(String value) {
            addCriterion("other_dys359 =", value, "otherDys359");
            return (Criteria) this;
        }

        public Criteria andOtherDys359NotEqualTo(String value) {
            addCriterion("other_dys359 <>", value, "otherDys359");
            return (Criteria) this;
        }

        public Criteria andOtherDys359GreaterThan(String value) {
            addCriterion("other_dys359 >", value, "otherDys359");
            return (Criteria) this;
        }

        public Criteria andOtherDys359GreaterThanOrEqualTo(String value) {
            addCriterion("other_dys359 >=", value, "otherDys359");
            return (Criteria) this;
        }

        public Criteria andOtherDys359LessThan(String value) {
            addCriterion("other_dys359 <", value, "otherDys359");
            return (Criteria) this;
        }

        public Criteria andOtherDys359LessThanOrEqualTo(String value) {
            addCriterion("other_dys359 <=", value, "otherDys359");
            return (Criteria) this;
        }

        public Criteria andOtherDys359Like(String value) {
            addCriterion("other_dys359 like", value, "otherDys359");
            return (Criteria) this;
        }

        public Criteria andOtherDys359NotLike(String value) {
            addCriterion("other_dys359 not like", value, "otherDys359");
            return (Criteria) this;
        }

        public Criteria andOtherDys359In(List<String> values) {
            addCriterion("other_dys359 in", values, "otherDys359");
            return (Criteria) this;
        }

        public Criteria andOtherDys359NotIn(List<String> values) {
            addCriterion("other_dys359 not in", values, "otherDys359");
            return (Criteria) this;
        }

        public Criteria andOtherDys359Between(String value1, String value2) {
            addCriterion("other_dys359 between", value1, value2, "otherDys359");
            return (Criteria) this;
        }

        public Criteria andOtherDys359NotBetween(String value1, String value2) {
            addCriterion("other_dys359 not between", value1, value2, "otherDys359");
            return (Criteria) this;
        }

        public Criteria andOtherDys390IsNull() {
            addCriterion("other_dys390 is null");
            return (Criteria) this;
        }

        public Criteria andOtherDys390IsNotNull() {
            addCriterion("other_dys390 is not null");
            return (Criteria) this;
        }

        public Criteria andOtherDys390EqualTo(String value) {
            addCriterion("other_dys390 =", value, "otherDys390");
            return (Criteria) this;
        }

        public Criteria andOtherDys390NotEqualTo(String value) {
            addCriterion("other_dys390 <>", value, "otherDys390");
            return (Criteria) this;
        }

        public Criteria andOtherDys390GreaterThan(String value) {
            addCriterion("other_dys390 >", value, "otherDys390");
            return (Criteria) this;
        }

        public Criteria andOtherDys390GreaterThanOrEqualTo(String value) {
            addCriterion("other_dys390 >=", value, "otherDys390");
            return (Criteria) this;
        }

        public Criteria andOtherDys390LessThan(String value) {
            addCriterion("other_dys390 <", value, "otherDys390");
            return (Criteria) this;
        }

        public Criteria andOtherDys390LessThanOrEqualTo(String value) {
            addCriterion("other_dys390 <=", value, "otherDys390");
            return (Criteria) this;
        }

        public Criteria andOtherDys390Like(String value) {
            addCriterion("other_dys390 like", value, "otherDys390");
            return (Criteria) this;
        }

        public Criteria andOtherDys390NotLike(String value) {
            addCriterion("other_dys390 not like", value, "otherDys390");
            return (Criteria) this;
        }

        public Criteria andOtherDys390In(List<String> values) {
            addCriterion("other_dys390 in", values, "otherDys390");
            return (Criteria) this;
        }

        public Criteria andOtherDys390NotIn(List<String> values) {
            addCriterion("other_dys390 not in", values, "otherDys390");
            return (Criteria) this;
        }

        public Criteria andOtherDys390Between(String value1, String value2) {
            addCriterion("other_dys390 between", value1, value2, "otherDys390");
            return (Criteria) this;
        }

        public Criteria andOtherDys390NotBetween(String value1, String value2) {
            addCriterion("other_dys390 not between", value1, value2, "otherDys390");
            return (Criteria) this;
        }

        public Criteria andOtherDys391IsNull() {
            addCriterion("other_dys391 is null");
            return (Criteria) this;
        }

        public Criteria andOtherDys391IsNotNull() {
            addCriterion("other_dys391 is not null");
            return (Criteria) this;
        }

        public Criteria andOtherDys391EqualTo(String value) {
            addCriterion("other_dys391 =", value, "otherDys391");
            return (Criteria) this;
        }

        public Criteria andOtherDys391NotEqualTo(String value) {
            addCriterion("other_dys391 <>", value, "otherDys391");
            return (Criteria) this;
        }

        public Criteria andOtherDys391GreaterThan(String value) {
            addCriterion("other_dys391 >", value, "otherDys391");
            return (Criteria) this;
        }

        public Criteria andOtherDys391GreaterThanOrEqualTo(String value) {
            addCriterion("other_dys391 >=", value, "otherDys391");
            return (Criteria) this;
        }

        public Criteria andOtherDys391LessThan(String value) {
            addCriterion("other_dys391 <", value, "otherDys391");
            return (Criteria) this;
        }

        public Criteria andOtherDys391LessThanOrEqualTo(String value) {
            addCriterion("other_dys391 <=", value, "otherDys391");
            return (Criteria) this;
        }

        public Criteria andOtherDys391Like(String value) {
            addCriterion("other_dys391 like", value, "otherDys391");
            return (Criteria) this;
        }

        public Criteria andOtherDys391NotLike(String value) {
            addCriterion("other_dys391 not like", value, "otherDys391");
            return (Criteria) this;
        }

        public Criteria andOtherDys391In(List<String> values) {
            addCriterion("other_dys391 in", values, "otherDys391");
            return (Criteria) this;
        }

        public Criteria andOtherDys391NotIn(List<String> values) {
            addCriterion("other_dys391 not in", values, "otherDys391");
            return (Criteria) this;
        }

        public Criteria andOtherDys391Between(String value1, String value2) {
            addCriterion("other_dys391 between", value1, value2, "otherDys391");
            return (Criteria) this;
        }

        public Criteria andOtherDys391NotBetween(String value1, String value2) {
            addCriterion("other_dys391 not between", value1, value2, "otherDys391");
            return (Criteria) this;
        }

        public Criteria andOtherDys392IsNull() {
            addCriterion("other_dys392 is null");
            return (Criteria) this;
        }

        public Criteria andOtherDys392IsNotNull() {
            addCriterion("other_dys392 is not null");
            return (Criteria) this;
        }

        public Criteria andOtherDys392EqualTo(String value) {
            addCriterion("other_dys392 =", value, "otherDys392");
            return (Criteria) this;
        }

        public Criteria andOtherDys392NotEqualTo(String value) {
            addCriterion("other_dys392 <>", value, "otherDys392");
            return (Criteria) this;
        }

        public Criteria andOtherDys392GreaterThan(String value) {
            addCriterion("other_dys392 >", value, "otherDys392");
            return (Criteria) this;
        }

        public Criteria andOtherDys392GreaterThanOrEqualTo(String value) {
            addCriterion("other_dys392 >=", value, "otherDys392");
            return (Criteria) this;
        }

        public Criteria andOtherDys392LessThan(String value) {
            addCriterion("other_dys392 <", value, "otherDys392");
            return (Criteria) this;
        }

        public Criteria andOtherDys392LessThanOrEqualTo(String value) {
            addCriterion("other_dys392 <=", value, "otherDys392");
            return (Criteria) this;
        }

        public Criteria andOtherDys392Like(String value) {
            addCriterion("other_dys392 like", value, "otherDys392");
            return (Criteria) this;
        }

        public Criteria andOtherDys392NotLike(String value) {
            addCriterion("other_dys392 not like", value, "otherDys392");
            return (Criteria) this;
        }

        public Criteria andOtherDys392In(List<String> values) {
            addCriterion("other_dys392 in", values, "otherDys392");
            return (Criteria) this;
        }

        public Criteria andOtherDys392NotIn(List<String> values) {
            addCriterion("other_dys392 not in", values, "otherDys392");
            return (Criteria) this;
        }

        public Criteria andOtherDys392Between(String value1, String value2) {
            addCriterion("other_dys392 between", value1, value2, "otherDys392");
            return (Criteria) this;
        }

        public Criteria andOtherDys392NotBetween(String value1, String value2) {
            addCriterion("other_dys392 not between", value1, value2, "otherDys392");
            return (Criteria) this;
        }

        public Criteria andOtherDys393IsNull() {
            addCriterion("other_dys393 is null");
            return (Criteria) this;
        }

        public Criteria andOtherDys393IsNotNull() {
            addCriterion("other_dys393 is not null");
            return (Criteria) this;
        }

        public Criteria andOtherDys393EqualTo(String value) {
            addCriterion("other_dys393 =", value, "otherDys393");
            return (Criteria) this;
        }

        public Criteria andOtherDys393NotEqualTo(String value) {
            addCriterion("other_dys393 <>", value, "otherDys393");
            return (Criteria) this;
        }

        public Criteria andOtherDys393GreaterThan(String value) {
            addCriterion("other_dys393 >", value, "otherDys393");
            return (Criteria) this;
        }

        public Criteria andOtherDys393GreaterThanOrEqualTo(String value) {
            addCriterion("other_dys393 >=", value, "otherDys393");
            return (Criteria) this;
        }

        public Criteria andOtherDys393LessThan(String value) {
            addCriterion("other_dys393 <", value, "otherDys393");
            return (Criteria) this;
        }

        public Criteria andOtherDys393LessThanOrEqualTo(String value) {
            addCriterion("other_dys393 <=", value, "otherDys393");
            return (Criteria) this;
        }

        public Criteria andOtherDys393Like(String value) {
            addCriterion("other_dys393 like", value, "otherDys393");
            return (Criteria) this;
        }

        public Criteria andOtherDys393NotLike(String value) {
            addCriterion("other_dys393 not like", value, "otherDys393");
            return (Criteria) this;
        }

        public Criteria andOtherDys393In(List<String> values) {
            addCriterion("other_dys393 in", values, "otherDys393");
            return (Criteria) this;
        }

        public Criteria andOtherDys393NotIn(List<String> values) {
            addCriterion("other_dys393 not in", values, "otherDys393");
            return (Criteria) this;
        }

        public Criteria andOtherDys393Between(String value1, String value2) {
            addCriterion("other_dys393 between", value1, value2, "otherDys393");
            return (Criteria) this;
        }

        public Criteria andOtherDys393NotBetween(String value1, String value2) {
            addCriterion("other_dys393 not between", value1, value2, "otherDys393");
            return (Criteria) this;
        }

        public Criteria andOtherDys437IsNull() {
            addCriterion("other_dys437 is null");
            return (Criteria) this;
        }

        public Criteria andOtherDys437IsNotNull() {
            addCriterion("other_dys437 is not null");
            return (Criteria) this;
        }

        public Criteria andOtherDys437EqualTo(String value) {
            addCriterion("other_dys437 =", value, "otherDys437");
            return (Criteria) this;
        }

        public Criteria andOtherDys437NotEqualTo(String value) {
            addCriterion("other_dys437 <>", value, "otherDys437");
            return (Criteria) this;
        }

        public Criteria andOtherDys437GreaterThan(String value) {
            addCriterion("other_dys437 >", value, "otherDys437");
            return (Criteria) this;
        }

        public Criteria andOtherDys437GreaterThanOrEqualTo(String value) {
            addCriterion("other_dys437 >=", value, "otherDys437");
            return (Criteria) this;
        }

        public Criteria andOtherDys437LessThan(String value) {
            addCriterion("other_dys437 <", value, "otherDys437");
            return (Criteria) this;
        }

        public Criteria andOtherDys437LessThanOrEqualTo(String value) {
            addCriterion("other_dys437 <=", value, "otherDys437");
            return (Criteria) this;
        }

        public Criteria andOtherDys437Like(String value) {
            addCriterion("other_dys437 like", value, "otherDys437");
            return (Criteria) this;
        }

        public Criteria andOtherDys437NotLike(String value) {
            addCriterion("other_dys437 not like", value, "otherDys437");
            return (Criteria) this;
        }

        public Criteria andOtherDys437In(List<String> values) {
            addCriterion("other_dys437 in", values, "otherDys437");
            return (Criteria) this;
        }

        public Criteria andOtherDys437NotIn(List<String> values) {
            addCriterion("other_dys437 not in", values, "otherDys437");
            return (Criteria) this;
        }

        public Criteria andOtherDys437Between(String value1, String value2) {
            addCriterion("other_dys437 between", value1, value2, "otherDys437");
            return (Criteria) this;
        }

        public Criteria andOtherDys437NotBetween(String value1, String value2) {
            addCriterion("other_dys437 not between", value1, value2, "otherDys437");
            return (Criteria) this;
        }

        public Criteria andOtherDys635IsNull() {
            addCriterion("other_dys635 is null");
            return (Criteria) this;
        }

        public Criteria andOtherDys635IsNotNull() {
            addCriterion("other_dys635 is not null");
            return (Criteria) this;
        }

        public Criteria andOtherDys635EqualTo(String value) {
            addCriterion("other_dys635 =", value, "otherDys635");
            return (Criteria) this;
        }

        public Criteria andOtherDys635NotEqualTo(String value) {
            addCriterion("other_dys635 <>", value, "otherDys635");
            return (Criteria) this;
        }

        public Criteria andOtherDys635GreaterThan(String value) {
            addCriterion("other_dys635 >", value, "otherDys635");
            return (Criteria) this;
        }

        public Criteria andOtherDys635GreaterThanOrEqualTo(String value) {
            addCriterion("other_dys635 >=", value, "otherDys635");
            return (Criteria) this;
        }

        public Criteria andOtherDys635LessThan(String value) {
            addCriterion("other_dys635 <", value, "otherDys635");
            return (Criteria) this;
        }

        public Criteria andOtherDys635LessThanOrEqualTo(String value) {
            addCriterion("other_dys635 <=", value, "otherDys635");
            return (Criteria) this;
        }

        public Criteria andOtherDys635Like(String value) {
            addCriterion("other_dys635 like", value, "otherDys635");
            return (Criteria) this;
        }

        public Criteria andOtherDys635NotLike(String value) {
            addCriterion("other_dys635 not like", value, "otherDys635");
            return (Criteria) this;
        }

        public Criteria andOtherDys635In(List<String> values) {
            addCriterion("other_dys635 in", values, "otherDys635");
            return (Criteria) this;
        }

        public Criteria andOtherDys635NotIn(List<String> values) {
            addCriterion("other_dys635 not in", values, "otherDys635");
            return (Criteria) this;
        }

        public Criteria andOtherDys635Between(String value1, String value2) {
            addCriterion("other_dys635 between", value1, value2, "otherDys635");
            return (Criteria) this;
        }

        public Criteria andOtherDys635NotBetween(String value1, String value2) {
            addCriterion("other_dys635 not between", value1, value2, "otherDys635");
            return (Criteria) this;
        }

        public Criteria andOtherDys590IsNull() {
            addCriterion("other_dys590 is null");
            return (Criteria) this;
        }

        public Criteria andOtherDys590IsNotNull() {
            addCriterion("other_dys590 is not null");
            return (Criteria) this;
        }

        public Criteria andOtherDys590EqualTo(String value) {
            addCriterion("other_dys590 =", value, "otherDys590");
            return (Criteria) this;
        }

        public Criteria andOtherDys590NotEqualTo(String value) {
            addCriterion("other_dys590 <>", value, "otherDys590");
            return (Criteria) this;
        }

        public Criteria andOtherDys590GreaterThan(String value) {
            addCriterion("other_dys590 >", value, "otherDys590");
            return (Criteria) this;
        }

        public Criteria andOtherDys590GreaterThanOrEqualTo(String value) {
            addCriterion("other_dys590 >=", value, "otherDys590");
            return (Criteria) this;
        }

        public Criteria andOtherDys590LessThan(String value) {
            addCriterion("other_dys590 <", value, "otherDys590");
            return (Criteria) this;
        }

        public Criteria andOtherDys590LessThanOrEqualTo(String value) {
            addCriterion("other_dys590 <=", value, "otherDys590");
            return (Criteria) this;
        }

        public Criteria andOtherDys590Like(String value) {
            addCriterion("other_dys590 like", value, "otherDys590");
            return (Criteria) this;
        }

        public Criteria andOtherDys590NotLike(String value) {
            addCriterion("other_dys590 not like", value, "otherDys590");
            return (Criteria) this;
        }

        public Criteria andOtherDys590In(List<String> values) {
            addCriterion("other_dys590 in", values, "otherDys590");
            return (Criteria) this;
        }

        public Criteria andOtherDys590NotIn(List<String> values) {
            addCriterion("other_dys590 not in", values, "otherDys590");
            return (Criteria) this;
        }

        public Criteria andOtherDys590Between(String value1, String value2) {
            addCriterion("other_dys590 between", value1, value2, "otherDys590");
            return (Criteria) this;
        }

        public Criteria andOtherDys590NotBetween(String value1, String value2) {
            addCriterion("other_dys590 not between", value1, value2, "otherDys590");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}