package com.enlighten.aio.db.model;

public class TalkWithBLOBs extends Talk {
    /**
	 * Lu
	2018年8月24日
	 */
	private static final long serialVersionUID = 1L;

	private String message;

    private String data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message == null ? null : message.trim();
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data == null ? null : data.trim();
    }
}