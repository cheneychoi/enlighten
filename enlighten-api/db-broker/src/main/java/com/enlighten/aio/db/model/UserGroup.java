package com.enlighten.aio.db.model;

import com.x.share.db.model.ModelWithBLOBs;

public class UserGroup extends ModelWithBLOBs {
	private Long id;
	
	private Long userId;

    private Long groupId;

    private String data;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    
}