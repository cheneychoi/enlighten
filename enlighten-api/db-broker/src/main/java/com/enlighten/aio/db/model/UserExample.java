package com.enlighten.aio.db.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andSurnameIsNull() {
            addCriterion("surname is null");
            return (Criteria) this;
        }

        public Criteria andSurnameIsNotNull() {
            addCriterion("surname is not null");
            return (Criteria) this;
        }

        public Criteria andSurnameEqualTo(String value) {
            addCriterion("surname =", value, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameNotEqualTo(String value) {
            addCriterion("surname <>", value, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameGreaterThan(String value) {
            addCriterion("surname >", value, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameGreaterThanOrEqualTo(String value) {
            addCriterion("surname >=", value, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameLessThan(String value) {
            addCriterion("surname <", value, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameLessThanOrEqualTo(String value) {
            addCriterion("surname <=", value, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameLike(String value) {
            addCriterion("surname like", value, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameNotLike(String value) {
            addCriterion("surname not like", value, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameIn(List<String> values) {
            addCriterion("surname in", values, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameNotIn(List<String> values) {
            addCriterion("surname not in", values, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameBetween(String value1, String value2) {
            addCriterion("surname between", value1, value2, "surname");
            return (Criteria) this;
        }

        public Criteria andSurnameNotBetween(String value1, String value2) {
            addCriterion("surname not between", value1, value2, "surname");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andBranchIsNull() {
            addCriterion("branch is null");
            return (Criteria) this;
        }

        public Criteria andBranchIsNotNull() {
            addCriterion("branch is not null");
            return (Criteria) this;
        }

        public Criteria andBranchEqualTo(String value) {
            addCriterion("branch =", value, "branch");
            return (Criteria) this;
        }

        public Criteria andBranchNotEqualTo(String value) {
            addCriterion("branch <>", value, "branch");
            return (Criteria) this;
        }

        public Criteria andBranchGreaterThan(String value) {
            addCriterion("branch >", value, "branch");
            return (Criteria) this;
        }

        public Criteria andBranchGreaterThanOrEqualTo(String value) {
            addCriterion("branch >=", value, "branch");
            return (Criteria) this;
        }

        public Criteria andBranchLessThan(String value) {
            addCriterion("branch <", value, "branch");
            return (Criteria) this;
        }

        public Criteria andBranchLessThanOrEqualTo(String value) {
            addCriterion("branch <=", value, "branch");
            return (Criteria) this;
        }

        public Criteria andBranchLike(String value) {
            addCriterion("branch like", value, "branch");
            return (Criteria) this;
        }

        public Criteria andBranchNotLike(String value) {
            addCriterion("branch not like", value, "branch");
            return (Criteria) this;
        }

        public Criteria andBranchIn(List<String> values) {
            addCriterion("branch in", values, "branch");
            return (Criteria) this;
        }

        public Criteria andBranchNotIn(List<String> values) {
            addCriterion("branch not in", values, "branch");
            return (Criteria) this;
        }

        public Criteria andBranchBetween(String value1, String value2) {
            addCriterion("branch between", value1, value2, "branch");
            return (Criteria) this;
        }

        public Criteria andBranchNotBetween(String value1, String value2) {
            addCriterion("branch not between", value1, value2, "branch");
            return (Criteria) this;
        }

        public Criteria andNicknameIsNull() {
            addCriterion("nickname is null");
            return (Criteria) this;
        }

        public Criteria andNicknameIsNotNull() {
            addCriterion("nickname is not null");
            return (Criteria) this;
        }

        public Criteria andNicknameEqualTo(String value) {
            addCriterion("nickname =", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotEqualTo(String value) {
            addCriterion("nickname <>", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameGreaterThan(String value) {
            addCriterion("nickname >", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameGreaterThanOrEqualTo(String value) {
            addCriterion("nickname >=", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLessThan(String value) {
            addCriterion("nickname <", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLessThanOrEqualTo(String value) {
            addCriterion("nickname <=", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLike(String value) {
            addCriterion("nickname like", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotLike(String value) {
            addCriterion("nickname not like", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameIn(List<String> values) {
            addCriterion("nickname in", values, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotIn(List<String> values) {
            addCriterion("nickname not in", values, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameBetween(String value1, String value2) {
            addCriterion("nickname between", value1, value2, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotBetween(String value1, String value2) {
            addCriterion("nickname not between", value1, value2, "nickname");
            return (Criteria) this;
        }

        public Criteria andAvatorIsNull() {
            addCriterion("avator is null");
            return (Criteria) this;
        }

        public Criteria andAvatorIsNotNull() {
            addCriterion("avator is not null");
            return (Criteria) this;
        }

        public Criteria andAvatorEqualTo(String value) {
            addCriterion("avator =", value, "avator");
            return (Criteria) this;
        }

        public Criteria andAvatorNotEqualTo(String value) {
            addCriterion("avator <>", value, "avator");
            return (Criteria) this;
        }

        public Criteria andAvatorGreaterThan(String value) {
            addCriterion("avator >", value, "avator");
            return (Criteria) this;
        }

        public Criteria andAvatorGreaterThanOrEqualTo(String value) {
            addCriterion("avator >=", value, "avator");
            return (Criteria) this;
        }

        public Criteria andAvatorLessThan(String value) {
            addCriterion("avator <", value, "avator");
            return (Criteria) this;
        }

        public Criteria andAvatorLessThanOrEqualTo(String value) {
            addCriterion("avator <=", value, "avator");
            return (Criteria) this;
        }

        public Criteria andAvatorLike(String value) {
            addCriterion("avator like", value, "avator");
            return (Criteria) this;
        }

        public Criteria andAvatorNotLike(String value) {
            addCriterion("avator not like", value, "avator");
            return (Criteria) this;
        }

        public Criteria andAvatorIn(List<String> values) {
            addCriterion("avator in", values, "avator");
            return (Criteria) this;
        }

        public Criteria andAvatorNotIn(List<String> values) {
            addCriterion("avator not in", values, "avator");
            return (Criteria) this;
        }

        public Criteria andAvatorBetween(String value1, String value2) {
            addCriterion("avator between", value1, value2, "avator");
            return (Criteria) this;
        }

        public Criteria andAvatorNotBetween(String value1, String value2) {
            addCriterion("avator not between", value1, value2, "avator");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("email is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("email is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("email =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("email <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("email >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("email >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("email <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("email <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("email like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("email not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("email in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("email not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("email between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("email not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNull() {
            addCriterion("password is null");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNotNull() {
            addCriterion("password is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordEqualTo(String value) {
            addCriterion("password =", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotEqualTo(String value) {
            addCriterion("password <>", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThan(String value) {
            addCriterion("password >", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("password >=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThan(String value) {
            addCriterion("password <", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThanOrEqualTo(String value) {
            addCriterion("password <=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLike(String value) {
            addCriterion("password like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotLike(String value) {
            addCriterion("password not like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordIn(List<String> values) {
            addCriterion("password in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotIn(List<String> values) {
            addCriterion("password not in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordBetween(String value1, String value2) {
            addCriterion("password between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotBetween(String value1, String value2) {
            addCriterion("password not between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andJiguanIsNull() {
            addCriterion("jiguan is null");
            return (Criteria) this;
        }

        public Criteria andJiguanIsNotNull() {
            addCriterion("jiguan is not null");
            return (Criteria) this;
        }

        public Criteria andJiguanEqualTo(String value) {
            addCriterion("jiguan =", value, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanNotEqualTo(String value) {
            addCriterion("jiguan <>", value, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanGreaterThan(String value) {
            addCriterion("jiguan >", value, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanGreaterThanOrEqualTo(String value) {
            addCriterion("jiguan >=", value, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanLessThan(String value) {
            addCriterion("jiguan <", value, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanLessThanOrEqualTo(String value) {
            addCriterion("jiguan <=", value, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanLike(String value) {
            addCriterion("jiguan like", value, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanNotLike(String value) {
            addCriterion("jiguan not like", value, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanIn(List<String> values) {
            addCriterion("jiguan in", values, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanNotIn(List<String> values) {
            addCriterion("jiguan not in", values, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanBetween(String value1, String value2) {
            addCriterion("jiguan between", value1, value2, "jiguan");
            return (Criteria) this;
        }

        public Criteria andJiguanNotBetween(String value1, String value2) {
            addCriterion("jiguan not between", value1, value2, "jiguan");
            return (Criteria) this;
        }

        public Criteria andBirthPlaceIsNull() {
            addCriterion("birth_place is null");
            return (Criteria) this;
        }

        public Criteria andBirthPlaceIsNotNull() {
            addCriterion("birth_place is not null");
            return (Criteria) this;
        }

        public Criteria andBirthPlaceEqualTo(String value) {
            addCriterion("birth_place =", value, "birthPlace");
            return (Criteria) this;
        }

        public Criteria andBirthPlaceNotEqualTo(String value) {
            addCriterion("birth_place <>", value, "birthPlace");
            return (Criteria) this;
        }

        public Criteria andBirthPlaceGreaterThan(String value) {
            addCriterion("birth_place >", value, "birthPlace");
            return (Criteria) this;
        }

        public Criteria andBirthPlaceGreaterThanOrEqualTo(String value) {
            addCriterion("birth_place >=", value, "birthPlace");
            return (Criteria) this;
        }

        public Criteria andBirthPlaceLessThan(String value) {
            addCriterion("birth_place <", value, "birthPlace");
            return (Criteria) this;
        }

        public Criteria andBirthPlaceLessThanOrEqualTo(String value) {
            addCriterion("birth_place <=", value, "birthPlace");
            return (Criteria) this;
        }

        public Criteria andBirthPlaceLike(String value) {
            addCriterion("birth_place like", value, "birthPlace");
            return (Criteria) this;
        }

        public Criteria andBirthPlaceNotLike(String value) {
            addCriterion("birth_place not like", value, "birthPlace");
            return (Criteria) this;
        }

        public Criteria andBirthPlaceIn(List<String> values) {
            addCriterion("birth_place in", values, "birthPlace");
            return (Criteria) this;
        }

        public Criteria andBirthPlaceNotIn(List<String> values) {
            addCriterion("birth_place not in", values, "birthPlace");
            return (Criteria) this;
        }

        public Criteria andBirthPlaceBetween(String value1, String value2) {
            addCriterion("birth_place between", value1, value2, "birthPlace");
            return (Criteria) this;
        }

        public Criteria andBirthPlaceNotBetween(String value1, String value2) {
            addCriterion("birth_place not between", value1, value2, "birthPlace");
            return (Criteria) this;
        }

        public Criteria andBirthdayIsNull() {
            addCriterion("birthday is null");
            return (Criteria) this;
        }

        public Criteria andBirthdayIsNotNull() {
            addCriterion("birthday is not null");
            return (Criteria) this;
        }

        public Criteria andBirthdayEqualTo(Date value) {
            addCriterion("birthday =", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayNotEqualTo(Date value) {
            addCriterion("birthday <>", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayGreaterThan(Date value) {
            addCriterion("birthday >", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayGreaterThanOrEqualTo(Date value) {
            addCriterion("birthday >=", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayLessThan(Date value) {
            addCriterion("birthday <", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayLessThanOrEqualTo(Date value) {
            addCriterion("birthday <=", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayIn(List<Date> values) {
            addCriterion("birthday in", values, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayNotIn(List<Date> values) {
            addCriterion("birthday not in", values, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayBetween(Date value1, Date value2) {
            addCriterion("birthday between", value1, value2, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayNotBetween(Date value1, Date value2) {
            addCriterion("birthday not between", value1, value2, "birthday");
            return (Criteria) this;
        }

        public Criteria andDialectIsNull() {
            addCriterion("dialect is null");
            return (Criteria) this;
        }

        public Criteria andDialectIsNotNull() {
            addCriterion("dialect is not null");
            return (Criteria) this;
        }

        public Criteria andDialectEqualTo(String value) {
            addCriterion("dialect =", value, "dialect");
            return (Criteria) this;
        }

        public Criteria andDialectNotEqualTo(String value) {
            addCriterion("dialect <>", value, "dialect");
            return (Criteria) this;
        }

        public Criteria andDialectGreaterThan(String value) {
            addCriterion("dialect >", value, "dialect");
            return (Criteria) this;
        }

        public Criteria andDialectGreaterThanOrEqualTo(String value) {
            addCriterion("dialect >=", value, "dialect");
            return (Criteria) this;
        }

        public Criteria andDialectLessThan(String value) {
            addCriterion("dialect <", value, "dialect");
            return (Criteria) this;
        }

        public Criteria andDialectLessThanOrEqualTo(String value) {
            addCriterion("dialect <=", value, "dialect");
            return (Criteria) this;
        }

        public Criteria andDialectLike(String value) {
            addCriterion("dialect like", value, "dialect");
            return (Criteria) this;
        }

        public Criteria andDialectNotLike(String value) {
            addCriterion("dialect not like", value, "dialect");
            return (Criteria) this;
        }

        public Criteria andDialectIn(List<String> values) {
            addCriterion("dialect in", values, "dialect");
            return (Criteria) this;
        }

        public Criteria andDialectNotIn(List<String> values) {
            addCriterion("dialect not in", values, "dialect");
            return (Criteria) this;
        }

        public Criteria andDialectBetween(String value1, String value2) {
            addCriterion("dialect between", value1, value2, "dialect");
            return (Criteria) this;
        }

        public Criteria andDialectNotBetween(String value1, String value2) {
            addCriterion("dialect not between", value1, value2, "dialect");
            return (Criteria) this;
        }

        public Criteria andFatherNationalityIsNull() {
            addCriterion("father_nationality is null");
            return (Criteria) this;
        }

        public Criteria andFatherNationalityIsNotNull() {
            addCriterion("father_nationality is not null");
            return (Criteria) this;
        }

        public Criteria andFatherNationalityEqualTo(String value) {
            addCriterion("father_nationality =", value, "fatherNationality");
            return (Criteria) this;
        }

        public Criteria andFatherNationalityNotEqualTo(String value) {
            addCriterion("father_nationality <>", value, "fatherNationality");
            return (Criteria) this;
        }

        public Criteria andFatherNationalityGreaterThan(String value) {
            addCriterion("father_nationality >", value, "fatherNationality");
            return (Criteria) this;
        }

        public Criteria andFatherNationalityGreaterThanOrEqualTo(String value) {
            addCriterion("father_nationality >=", value, "fatherNationality");
            return (Criteria) this;
        }

        public Criteria andFatherNationalityLessThan(String value) {
            addCriterion("father_nationality <", value, "fatherNationality");
            return (Criteria) this;
        }

        public Criteria andFatherNationalityLessThanOrEqualTo(String value) {
            addCriterion("father_nationality <=", value, "fatherNationality");
            return (Criteria) this;
        }

        public Criteria andFatherNationalityLike(String value) {
            addCriterion("father_nationality like", value, "fatherNationality");
            return (Criteria) this;
        }

        public Criteria andFatherNationalityNotLike(String value) {
            addCriterion("father_nationality not like", value, "fatherNationality");
            return (Criteria) this;
        }

        public Criteria andFatherNationalityIn(List<String> values) {
            addCriterion("father_nationality in", values, "fatherNationality");
            return (Criteria) this;
        }

        public Criteria andFatherNationalityNotIn(List<String> values) {
            addCriterion("father_nationality not in", values, "fatherNationality");
            return (Criteria) this;
        }

        public Criteria andFatherNationalityBetween(String value1, String value2) {
            addCriterion("father_nationality between", value1, value2, "fatherNationality");
            return (Criteria) this;
        }

        public Criteria andFatherNationalityNotBetween(String value1, String value2) {
            addCriterion("father_nationality not between", value1, value2, "fatherNationality");
            return (Criteria) this;
        }

        public Criteria andFatherCountryIsNull() {
            addCriterion("father_country is null");
            return (Criteria) this;
        }

        public Criteria andFatherCountryIsNotNull() {
            addCriterion("father_country is not null");
            return (Criteria) this;
        }

        public Criteria andFatherCountryEqualTo(String value) {
            addCriterion("father_country =", value, "fatherCountry");
            return (Criteria) this;
        }

        public Criteria andFatherCountryNotEqualTo(String value) {
            addCriterion("father_country <>", value, "fatherCountry");
            return (Criteria) this;
        }

        public Criteria andFatherCountryGreaterThan(String value) {
            addCriterion("father_country >", value, "fatherCountry");
            return (Criteria) this;
        }

        public Criteria andFatherCountryGreaterThanOrEqualTo(String value) {
            addCriterion("father_country >=", value, "fatherCountry");
            return (Criteria) this;
        }

        public Criteria andFatherCountryLessThan(String value) {
            addCriterion("father_country <", value, "fatherCountry");
            return (Criteria) this;
        }

        public Criteria andFatherCountryLessThanOrEqualTo(String value) {
            addCriterion("father_country <=", value, "fatherCountry");
            return (Criteria) this;
        }

        public Criteria andFatherCountryLike(String value) {
            addCriterion("father_country like", value, "fatherCountry");
            return (Criteria) this;
        }

        public Criteria andFatherCountryNotLike(String value) {
            addCriterion("father_country not like", value, "fatherCountry");
            return (Criteria) this;
        }

        public Criteria andFatherCountryIn(List<String> values) {
            addCriterion("father_country in", values, "fatherCountry");
            return (Criteria) this;
        }

        public Criteria andFatherCountryNotIn(List<String> values) {
            addCriterion("father_country not in", values, "fatherCountry");
            return (Criteria) this;
        }

        public Criteria andFatherCountryBetween(String value1, String value2) {
            addCriterion("father_country between", value1, value2, "fatherCountry");
            return (Criteria) this;
        }

        public Criteria andFatherCountryNotBetween(String value1, String value2) {
            addCriterion("father_country not between", value1, value2, "fatherCountry");
            return (Criteria) this;
        }

        public Criteria andFatherLanguageIsNull() {
            addCriterion("father_language is null");
            return (Criteria) this;
        }

        public Criteria andFatherLanguageIsNotNull() {
            addCriterion("father_language is not null");
            return (Criteria) this;
        }

        public Criteria andFatherLanguageEqualTo(String value) {
            addCriterion("father_language =", value, "fatherLanguage");
            return (Criteria) this;
        }

        public Criteria andFatherLanguageNotEqualTo(String value) {
            addCriterion("father_language <>", value, "fatherLanguage");
            return (Criteria) this;
        }

        public Criteria andFatherLanguageGreaterThan(String value) {
            addCriterion("father_language >", value, "fatherLanguage");
            return (Criteria) this;
        }

        public Criteria andFatherLanguageGreaterThanOrEqualTo(String value) {
            addCriterion("father_language >=", value, "fatherLanguage");
            return (Criteria) this;
        }

        public Criteria andFatherLanguageLessThan(String value) {
            addCriterion("father_language <", value, "fatherLanguage");
            return (Criteria) this;
        }

        public Criteria andFatherLanguageLessThanOrEqualTo(String value) {
            addCriterion("father_language <=", value, "fatherLanguage");
            return (Criteria) this;
        }

        public Criteria andFatherLanguageLike(String value) {
            addCriterion("father_language like", value, "fatherLanguage");
            return (Criteria) this;
        }

        public Criteria andFatherLanguageNotLike(String value) {
            addCriterion("father_language not like", value, "fatherLanguage");
            return (Criteria) this;
        }

        public Criteria andFatherLanguageIn(List<String> values) {
            addCriterion("father_language in", values, "fatherLanguage");
            return (Criteria) this;
        }

        public Criteria andFatherLanguageNotIn(List<String> values) {
            addCriterion("father_language not in", values, "fatherLanguage");
            return (Criteria) this;
        }

        public Criteria andFatherLanguageBetween(String value1, String value2) {
            addCriterion("father_language between", value1, value2, "fatherLanguage");
            return (Criteria) this;
        }

        public Criteria andFatherLanguageNotBetween(String value1, String value2) {
            addCriterion("father_language not between", value1, value2, "fatherLanguage");
            return (Criteria) this;
        }

        public Criteria andMotherNationalityIsNull() {
            addCriterion("mother_nationality is null");
            return (Criteria) this;
        }

        public Criteria andMotherNationalityIsNotNull() {
            addCriterion("mother_nationality is not null");
            return (Criteria) this;
        }

        public Criteria andMotherNationalityEqualTo(String value) {
            addCriterion("mother_nationality =", value, "motherNationality");
            return (Criteria) this;
        }

        public Criteria andMotherNationalityNotEqualTo(String value) {
            addCriterion("mother_nationality <>", value, "motherNationality");
            return (Criteria) this;
        }

        public Criteria andMotherNationalityGreaterThan(String value) {
            addCriterion("mother_nationality >", value, "motherNationality");
            return (Criteria) this;
        }

        public Criteria andMotherNationalityGreaterThanOrEqualTo(String value) {
            addCriterion("mother_nationality >=", value, "motherNationality");
            return (Criteria) this;
        }

        public Criteria andMotherNationalityLessThan(String value) {
            addCriterion("mother_nationality <", value, "motherNationality");
            return (Criteria) this;
        }

        public Criteria andMotherNationalityLessThanOrEqualTo(String value) {
            addCriterion("mother_nationality <=", value, "motherNationality");
            return (Criteria) this;
        }

        public Criteria andMotherNationalityLike(String value) {
            addCriterion("mother_nationality like", value, "motherNationality");
            return (Criteria) this;
        }

        public Criteria andMotherNationalityNotLike(String value) {
            addCriterion("mother_nationality not like", value, "motherNationality");
            return (Criteria) this;
        }

        public Criteria andMotherNationalityIn(List<String> values) {
            addCriterion("mother_nationality in", values, "motherNationality");
            return (Criteria) this;
        }

        public Criteria andMotherNationalityNotIn(List<String> values) {
            addCriterion("mother_nationality not in", values, "motherNationality");
            return (Criteria) this;
        }

        public Criteria andMotherNationalityBetween(String value1, String value2) {
            addCriterion("mother_nationality between", value1, value2, "motherNationality");
            return (Criteria) this;
        }

        public Criteria andMotherNationalityNotBetween(String value1, String value2) {
            addCriterion("mother_nationality not between", value1, value2, "motherNationality");
            return (Criteria) this;
        }

        public Criteria andMotherCountryIsNull() {
            addCriterion("mother_country is null");
            return (Criteria) this;
        }

        public Criteria andMotherCountryIsNotNull() {
            addCriterion("mother_country is not null");
            return (Criteria) this;
        }

        public Criteria andMotherCountryEqualTo(String value) {
            addCriterion("mother_country =", value, "motherCountry");
            return (Criteria) this;
        }

        public Criteria andMotherCountryNotEqualTo(String value) {
            addCriterion("mother_country <>", value, "motherCountry");
            return (Criteria) this;
        }

        public Criteria andMotherCountryGreaterThan(String value) {
            addCriterion("mother_country >", value, "motherCountry");
            return (Criteria) this;
        }

        public Criteria andMotherCountryGreaterThanOrEqualTo(String value) {
            addCriterion("mother_country >=", value, "motherCountry");
            return (Criteria) this;
        }

        public Criteria andMotherCountryLessThan(String value) {
            addCriterion("mother_country <", value, "motherCountry");
            return (Criteria) this;
        }

        public Criteria andMotherCountryLessThanOrEqualTo(String value) {
            addCriterion("mother_country <=", value, "motherCountry");
            return (Criteria) this;
        }

        public Criteria andMotherCountryLike(String value) {
            addCriterion("mother_country like", value, "motherCountry");
            return (Criteria) this;
        }

        public Criteria andMotherCountryNotLike(String value) {
            addCriterion("mother_country not like", value, "motherCountry");
            return (Criteria) this;
        }

        public Criteria andMotherCountryIn(List<String> values) {
            addCriterion("mother_country in", values, "motherCountry");
            return (Criteria) this;
        }

        public Criteria andMotherCountryNotIn(List<String> values) {
            addCriterion("mother_country not in", values, "motherCountry");
            return (Criteria) this;
        }

        public Criteria andMotherCountryBetween(String value1, String value2) {
            addCriterion("mother_country between", value1, value2, "motherCountry");
            return (Criteria) this;
        }

        public Criteria andMotherCountryNotBetween(String value1, String value2) {
            addCriterion("mother_country not between", value1, value2, "motherCountry");
            return (Criteria) this;
        }

        public Criteria andMotherLanguageIsNull() {
            addCriterion("mother_language is null");
            return (Criteria) this;
        }

        public Criteria andMotherLanguageIsNotNull() {
            addCriterion("mother_language is not null");
            return (Criteria) this;
        }

        public Criteria andMotherLanguageEqualTo(String value) {
            addCriterion("mother_language =", value, "motherLanguage");
            return (Criteria) this;
        }

        public Criteria andMotherLanguageNotEqualTo(String value) {
            addCriterion("mother_language <>", value, "motherLanguage");
            return (Criteria) this;
        }

        public Criteria andMotherLanguageGreaterThan(String value) {
            addCriterion("mother_language >", value, "motherLanguage");
            return (Criteria) this;
        }

        public Criteria andMotherLanguageGreaterThanOrEqualTo(String value) {
            addCriterion("mother_language >=", value, "motherLanguage");
            return (Criteria) this;
        }

        public Criteria andMotherLanguageLessThan(String value) {
            addCriterion("mother_language <", value, "motherLanguage");
            return (Criteria) this;
        }

        public Criteria andMotherLanguageLessThanOrEqualTo(String value) {
            addCriterion("mother_language <=", value, "motherLanguage");
            return (Criteria) this;
        }

        public Criteria andMotherLanguageLike(String value) {
            addCriterion("mother_language like", value, "motherLanguage");
            return (Criteria) this;
        }

        public Criteria andMotherLanguageNotLike(String value) {
            addCriterion("mother_language not like", value, "motherLanguage");
            return (Criteria) this;
        }

        public Criteria andMotherLanguageIn(List<String> values) {
            addCriterion("mother_language in", values, "motherLanguage");
            return (Criteria) this;
        }

        public Criteria andMotherLanguageNotIn(List<String> values) {
            addCriterion("mother_language not in", values, "motherLanguage");
            return (Criteria) this;
        }

        public Criteria andMotherLanguageBetween(String value1, String value2) {
            addCriterion("mother_language between", value1, value2, "motherLanguage");
            return (Criteria) this;
        }

        public Criteria andMotherLanguageNotBetween(String value1, String value2) {
            addCriterion("mother_language not between", value1, value2, "motherLanguage");
            return (Criteria) this;
        }

        public Criteria andMotherJiguanIsNull() {
            addCriterion("mother_jiguan is null");
            return (Criteria) this;
        }

        public Criteria andMotherJiguanIsNotNull() {
            addCriterion("mother_jiguan is not null");
            return (Criteria) this;
        }

        public Criteria andMotherJiguanEqualTo(String value) {
            addCriterion("mother_jiguan =", value, "motherJiguan");
            return (Criteria) this;
        }

        public Criteria andMotherJiguanNotEqualTo(String value) {
            addCriterion("mother_jiguan <>", value, "motherJiguan");
            return (Criteria) this;
        }

        public Criteria andMotherJiguanGreaterThan(String value) {
            addCriterion("mother_jiguan >", value, "motherJiguan");
            return (Criteria) this;
        }

        public Criteria andMotherJiguanGreaterThanOrEqualTo(String value) {
            addCriterion("mother_jiguan >=", value, "motherJiguan");
            return (Criteria) this;
        }

        public Criteria andMotherJiguanLessThan(String value) {
            addCriterion("mother_jiguan <", value, "motherJiguan");
            return (Criteria) this;
        }

        public Criteria andMotherJiguanLessThanOrEqualTo(String value) {
            addCriterion("mother_jiguan <=", value, "motherJiguan");
            return (Criteria) this;
        }

        public Criteria andMotherJiguanLike(String value) {
            addCriterion("mother_jiguan like", value, "motherJiguan");
            return (Criteria) this;
        }

        public Criteria andMotherJiguanNotLike(String value) {
            addCriterion("mother_jiguan not like", value, "motherJiguan");
            return (Criteria) this;
        }

        public Criteria andMotherJiguanIn(List<String> values) {
            addCriterion("mother_jiguan in", values, "motherJiguan");
            return (Criteria) this;
        }

        public Criteria andMotherJiguanNotIn(List<String> values) {
            addCriterion("mother_jiguan not in", values, "motherJiguan");
            return (Criteria) this;
        }

        public Criteria andMotherJiguanBetween(String value1, String value2) {
            addCriterion("mother_jiguan between", value1, value2, "motherJiguan");
            return (Criteria) this;
        }

        public Criteria andMotherJiguanNotBetween(String value1, String value2) {
            addCriterion("mother_jiguan not between", value1, value2, "motherJiguan");
            return (Criteria) this;
        }

        public Criteria andYSnpIsNull() {
            addCriterion("Y_SNP is null");
            return (Criteria) this;
        }

        public Criteria andYSnpIsNotNull() {
            addCriterion("Y_SNP is not null");
            return (Criteria) this;
        }

        public Criteria andYSnpEqualTo(String value) {
            addCriterion("Y_SNP =", value, "ySnp");
            return (Criteria) this;
        }

        public Criteria andYSnpNotEqualTo(String value) {
            addCriterion("Y_SNP <>", value, "ySnp");
            return (Criteria) this;
        }

        public Criteria andYSnpGreaterThan(String value) {
            addCriterion("Y_SNP >", value, "ySnp");
            return (Criteria) this;
        }

        public Criteria andYSnpGreaterThanOrEqualTo(String value) {
            addCriterion("Y_SNP >=", value, "ySnp");
            return (Criteria) this;
        }

        public Criteria andYSnpLessThan(String value) {
            addCriterion("Y_SNP <", value, "ySnp");
            return (Criteria) this;
        }

        public Criteria andYSnpLessThanOrEqualTo(String value) {
            addCriterion("Y_SNP <=", value, "ySnp");
            return (Criteria) this;
        }

        public Criteria andYSnpLike(String value) {
            addCriterion("Y_SNP like", value, "ySnp");
            return (Criteria) this;
        }

        public Criteria andYSnpNotLike(String value) {
            addCriterion("Y_SNP not like", value, "ySnp");
            return (Criteria) this;
        }

        public Criteria andYSnpIn(List<String> values) {
            addCriterion("Y_SNP in", values, "ySnp");
            return (Criteria) this;
        }

        public Criteria andYSnpNotIn(List<String> values) {
            addCriterion("Y_SNP not in", values, "ySnp");
            return (Criteria) this;
        }

        public Criteria andYSnpBetween(String value1, String value2) {
            addCriterion("Y_SNP between", value1, value2, "ySnp");
            return (Criteria) this;
        }

        public Criteria andYSnpNotBetween(String value1, String value2) {
            addCriterion("Y_SNP not between", value1, value2, "ySnp");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andSequenceIsNull() {
            addCriterion("sequence is null");
            return (Criteria) this;
        }

        public Criteria andSequenceIsNotNull() {
            addCriterion("sequence is not null");
            return (Criteria) this;
        }

        public Criteria andSequenceEqualTo(String value) {
            addCriterion("sequence =", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceNotEqualTo(String value) {
            addCriterion("sequence <>", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceGreaterThan(String value) {
            addCriterion("sequence >", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceGreaterThanOrEqualTo(String value) {
            addCriterion("sequence >=", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceLessThan(String value) {
            addCriterion("sequence <", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceLessThanOrEqualTo(String value) {
            addCriterion("sequence <=", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceLike(String value) {
            addCriterion("sequence like", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceNotLike(String value) {
            addCriterion("sequence not like", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceIn(List<String> values) {
            addCriterion("sequence in", values, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceNotIn(List<String> values) {
            addCriterion("sequence not in", values, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceBetween(String value1, String value2) {
            addCriterion("sequence between", value1, value2, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceNotBetween(String value1, String value2) {
            addCriterion("sequence not between", value1, value2, "sequence");
            return (Criteria) this;
        }

        public Criteria andYStrIsNull() {
            addCriterion("Y_STR is null");
            return (Criteria) this;
        }

        public Criteria andYStrIsNotNull() {
            addCriterion("Y_STR is not null");
            return (Criteria) this;
        }

        public Criteria andYStrEqualTo(String value) {
            addCriterion("Y_STR =", value, "yStr");
            return (Criteria) this;
        }

        public Criteria andYStrNotEqualTo(String value) {
            addCriterion("Y_STR <>", value, "yStr");
            return (Criteria) this;
        }

        public Criteria andYStrGreaterThan(String value) {
            addCriterion("Y_STR >", value, "yStr");
            return (Criteria) this;
        }

        public Criteria andYStrGreaterThanOrEqualTo(String value) {
            addCriterion("Y_STR >=", value, "yStr");
            return (Criteria) this;
        }

        public Criteria andYStrLessThan(String value) {
            addCriterion("Y_STR <", value, "yStr");
            return (Criteria) this;
        }

        public Criteria andYStrLessThanOrEqualTo(String value) {
            addCriterion("Y_STR <=", value, "yStr");
            return (Criteria) this;
        }

        public Criteria andYStrLike(String value) {
            addCriterion("Y_STR like", value, "yStr");
            return (Criteria) this;
        }

        public Criteria andYStrNotLike(String value) {
            addCriterion("Y_STR not like", value, "yStr");
            return (Criteria) this;
        }

        public Criteria andYStrIn(List<String> values) {
            addCriterion("Y_STR in", values, "yStr");
            return (Criteria) this;
        }

        public Criteria andYStrNotIn(List<String> values) {
            addCriterion("Y_STR not in", values, "yStr");
            return (Criteria) this;
        }

        public Criteria andYStrBetween(String value1, String value2) {
            addCriterion("Y_STR between", value1, value2, "yStr");
            return (Criteria) this;
        }

        public Criteria andYStrNotBetween(String value1, String value2) {
            addCriterion("Y_STR not between", value1, value2, "yStr");
            return (Criteria) this;
        }

        public Criteria andMtdnaIsNull() {
            addCriterion("mtDNA is null");
            return (Criteria) this;
        }

        public Criteria andMtdnaIsNotNull() {
            addCriterion("mtDNA is not null");
            return (Criteria) this;
        }

        public Criteria andMtdnaEqualTo(String value) {
            addCriterion("mtDNA =", value, "mtdna");
            return (Criteria) this;
        }

        public Criteria andMtdnaNotEqualTo(String value) {
            addCriterion("mtDNA <>", value, "mtdna");
            return (Criteria) this;
        }

        public Criteria andMtdnaGreaterThan(String value) {
            addCriterion("mtDNA >", value, "mtdna");
            return (Criteria) this;
        }

        public Criteria andMtdnaGreaterThanOrEqualTo(String value) {
            addCriterion("mtDNA >=", value, "mtdna");
            return (Criteria) this;
        }

        public Criteria andMtdnaLessThan(String value) {
            addCriterion("mtDNA <", value, "mtdna");
            return (Criteria) this;
        }

        public Criteria andMtdnaLessThanOrEqualTo(String value) {
            addCriterion("mtDNA <=", value, "mtdna");
            return (Criteria) this;
        }

        public Criteria andMtdnaLike(String value) {
            addCriterion("mtDNA like", value, "mtdna");
            return (Criteria) this;
        }

        public Criteria andMtdnaNotLike(String value) {
            addCriterion("mtDNA not like", value, "mtdna");
            return (Criteria) this;
        }

        public Criteria andMtdnaIn(List<String> values) {
            addCriterion("mtDNA in", values, "mtdna");
            return (Criteria) this;
        }

        public Criteria andMtdnaNotIn(List<String> values) {
            addCriterion("mtDNA not in", values, "mtdna");
            return (Criteria) this;
        }

        public Criteria andMtdnaBetween(String value1, String value2) {
            addCriterion("mtDNA between", value1, value2, "mtdna");
            return (Criteria) this;
        }

        public Criteria andMtdnaNotBetween(String value1, String value2) {
            addCriterion("mtDNA not between", value1, value2, "mtdna");
            return (Criteria) this;
        }

        public Criteria andIsAgreeIsNull() {
            addCriterion("is_agree is null");
            return (Criteria) this;
        }

        public Criteria andIsAgreeIsNotNull() {
            addCriterion("is_agree is not null");
            return (Criteria) this;
        }

        public Criteria andIsAgreeEqualTo(String value) {
            addCriterion("is_agree =", value, "isAgree");
            return (Criteria) this;
        }

        public Criteria andIsAgreeNotEqualTo(String value) {
            addCriterion("is_agree <>", value, "isAgree");
            return (Criteria) this;
        }

        public Criteria andIsAgreeGreaterThan(String value) {
            addCriterion("is_agree >", value, "isAgree");
            return (Criteria) this;
        }

        public Criteria andIsAgreeGreaterThanOrEqualTo(String value) {
            addCriterion("is_agree >=", value, "isAgree");
            return (Criteria) this;
        }

        public Criteria andIsAgreeLessThan(String value) {
            addCriterion("is_agree <", value, "isAgree");
            return (Criteria) this;
        }

        public Criteria andIsAgreeLessThanOrEqualTo(String value) {
            addCriterion("is_agree <=", value, "isAgree");
            return (Criteria) this;
        }

        public Criteria andIsAgreeLike(String value) {
            addCriterion("is_agree like", value, "isAgree");
            return (Criteria) this;
        }

        public Criteria andIsAgreeNotLike(String value) {
            addCriterion("is_agree not like", value, "isAgree");
            return (Criteria) this;
        }

        public Criteria andIsAgreeIn(List<String> values) {
            addCriterion("is_agree in", values, "isAgree");
            return (Criteria) this;
        }

        public Criteria andIsAgreeNotIn(List<String> values) {
            addCriterion("is_agree not in", values, "isAgree");
            return (Criteria) this;
        }

        public Criteria andIsAgreeBetween(String value1, String value2) {
            addCriterion("is_agree between", value1, value2, "isAgree");
            return (Criteria) this;
        }

        public Criteria andIsAgreeNotBetween(String value1, String value2) {
            addCriterion("is_agree not between", value1, value2, "isAgree");
            return (Criteria) this;
        }

        public Criteria andResearchIsNull() {
            addCriterion("research is null");
            return (Criteria) this;
        }

        public Criteria andResearchIsNotNull() {
            addCriterion("research is not null");
            return (Criteria) this;
        }

        public Criteria andResearchEqualTo(String value) {
            addCriterion("research =", value, "research");
            return (Criteria) this;
        }

        public Criteria andResearchNotEqualTo(String value) {
            addCriterion("research <>", value, "research");
            return (Criteria) this;
        }

        public Criteria andResearchGreaterThan(String value) {
            addCriterion("research >", value, "research");
            return (Criteria) this;
        }

        public Criteria andResearchGreaterThanOrEqualTo(String value) {
            addCriterion("research >=", value, "research");
            return (Criteria) this;
        }

        public Criteria andResearchLessThan(String value) {
            addCriterion("research <", value, "research");
            return (Criteria) this;
        }

        public Criteria andResearchLessThanOrEqualTo(String value) {
            addCriterion("research <=", value, "research");
            return (Criteria) this;
        }

        public Criteria andResearchLike(String value) {
            addCriterion("research like", value, "research");
            return (Criteria) this;
        }

        public Criteria andResearchNotLike(String value) {
            addCriterion("research not like", value, "research");
            return (Criteria) this;
        }

        public Criteria andResearchIn(List<String> values) {
            addCriterion("research in", values, "research");
            return (Criteria) this;
        }

        public Criteria andResearchNotIn(List<String> values) {
            addCriterion("research not in", values, "research");
            return (Criteria) this;
        }

        public Criteria andResearchBetween(String value1, String value2) {
            addCriterion("research between", value1, value2, "research");
            return (Criteria) this;
        }

        public Criteria andResearchNotBetween(String value1, String value2) {
            addCriterion("research not between", value1, value2, "research");
            return (Criteria) this;
        }

        public Criteria andActivationCodeIsNull() {
            addCriterion("activation_code is null");
            return (Criteria) this;
        }

        public Criteria andActivationCodeIsNotNull() {
            addCriterion("activation_code is not null");
            return (Criteria) this;
        }

        public Criteria andActivationCodeEqualTo(String value) {
            addCriterion("activation_code =", value, "activationCode");
            return (Criteria) this;
        }

        public Criteria andActivationCodeNotEqualTo(String value) {
            addCriterion("activation_code <>", value, "activationCode");
            return (Criteria) this;
        }

        public Criteria andActivationCodeGreaterThan(String value) {
            addCriterion("activation_code >", value, "activationCode");
            return (Criteria) this;
        }

        public Criteria andActivationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("activation_code >=", value, "activationCode");
            return (Criteria) this;
        }

        public Criteria andActivationCodeLessThan(String value) {
            addCriterion("activation_code <", value, "activationCode");
            return (Criteria) this;
        }

        public Criteria andActivationCodeLessThanOrEqualTo(String value) {
            addCriterion("activation_code <=", value, "activationCode");
            return (Criteria) this;
        }

        public Criteria andActivationCodeLike(String value) {
            addCriterion("activation_code like", value, "activationCode");
            return (Criteria) this;
        }

        public Criteria andActivationCodeNotLike(String value) {
            addCriterion("activation_code not like", value, "activationCode");
            return (Criteria) this;
        }

        public Criteria andActivationCodeIn(List<String> values) {
            addCriterion("activation_code in", values, "activationCode");
            return (Criteria) this;
        }

        public Criteria andActivationCodeNotIn(List<String> values) {
            addCriterion("activation_code not in", values, "activationCode");
            return (Criteria) this;
        }

        public Criteria andActivationCodeBetween(String value1, String value2) {
            addCriterion("activation_code between", value1, value2, "activationCode");
            return (Criteria) this;
        }

        public Criteria andActivationCodeNotBetween(String value1, String value2) {
            addCriterion("activation_code not between", value1, value2, "activationCode");
            return (Criteria) this;
        }

        public Criteria andActivationStatusIsNull() {
            addCriterion("activation_status is null");
            return (Criteria) this;
        }

        public Criteria andActivationStatusIsNotNull() {
            addCriterion("activation_status is not null");
            return (Criteria) this;
        }

        public Criteria andActivationStatusEqualTo(Integer value) {
            addCriterion("activation_status =", value, "activationStatus");
            return (Criteria) this;
        }

        public Criteria andActivationStatusNotEqualTo(Integer value) {
            addCriterion("activation_status <>", value, "activationStatus");
            return (Criteria) this;
        }

        public Criteria andActivationStatusGreaterThan(Integer value) {
            addCriterion("activation_status >", value, "activationStatus");
            return (Criteria) this;
        }

        public Criteria andActivationStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("activation_status >=", value, "activationStatus");
            return (Criteria) this;
        }

        public Criteria andActivationStatusLessThan(Integer value) {
            addCriterion("activation_status <", value, "activationStatus");
            return (Criteria) this;
        }

        public Criteria andActivationStatusLessThanOrEqualTo(Integer value) {
            addCriterion("activation_status <=", value, "activationStatus");
            return (Criteria) this;
        }

        public Criteria andActivationStatusIn(List<Integer> values) {
            addCriterion("activation_status in", values, "activationStatus");
            return (Criteria) this;
        }

        public Criteria andActivationStatusNotIn(List<Integer> values) {
            addCriterion("activation_status not in", values, "activationStatus");
            return (Criteria) this;
        }

        public Criteria andActivationStatusBetween(Integer value1, Integer value2) {
            addCriterion("activation_status between", value1, value2, "activationStatus");
            return (Criteria) this;
        }

        public Criteria andActivationStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("activation_status not between", value1, value2, "activationStatus");
            return (Criteria) this;
        }

        public Criteria andPictureIdIsNull() {
            addCriterion("picture_id is null");
            return (Criteria) this;
        }

        public Criteria andPictureIdIsNotNull() {
            addCriterion("picture_id is not null");
            return (Criteria) this;
        }

        public Criteria andPictureIdEqualTo(Long value) {
            addCriterion("picture_id =", value, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdNotEqualTo(Long value) {
            addCriterion("picture_id <>", value, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdGreaterThan(Long value) {
            addCriterion("picture_id >", value, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdGreaterThanOrEqualTo(Long value) {
            addCriterion("picture_id >=", value, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdLessThan(Long value) {
            addCriterion("picture_id <", value, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdLessThanOrEqualTo(Long value) {
            addCriterion("picture_id <=", value, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdIn(List<Long> values) {
            addCriterion("picture_id in", values, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdNotIn(List<Long> values) {
            addCriterion("picture_id not in", values, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdBetween(Long value1, Long value2) {
            addCriterion("picture_id between", value1, value2, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdNotBetween(Long value1, Long value2) {
            addCriterion("picture_id not between", value1, value2, "pictureId");
            return (Criteria) this;
        }

        public Criteria andCreateByIsNull() {
            addCriterion("create_by is null");
            return (Criteria) this;
        }

        public Criteria andCreateByIsNotNull() {
            addCriterion("create_by is not null");
            return (Criteria) this;
        }

        public Criteria andCreateByEqualTo(String value) {
            addCriterion("create_by =", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotEqualTo(String value) {
            addCriterion("create_by <>", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByGreaterThan(String value) {
            addCriterion("create_by >", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByGreaterThanOrEqualTo(String value) {
            addCriterion("create_by >=", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLessThan(String value) {
            addCriterion("create_by <", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLessThanOrEqualTo(String value) {
            addCriterion("create_by <=", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLike(String value) {
            addCriterion("create_by like", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotLike(String value) {
            addCriterion("create_by not like", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByIn(List<String> values) {
            addCriterion("create_by in", values, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotIn(List<String> values) {
            addCriterion("create_by not in", values, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByBetween(String value1, String value2) {
            addCriterion("create_by between", value1, value2, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotBetween(String value1, String value2) {
            addCriterion("create_by not between", value1, value2, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andUpdateByIsNull() {
            addCriterion("update_by is null");
            return (Criteria) this;
        }

        public Criteria andUpdateByIsNotNull() {
            addCriterion("update_by is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateByEqualTo(String value) {
            addCriterion("update_by =", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotEqualTo(String value) {
            addCriterion("update_by <>", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByGreaterThan(String value) {
            addCriterion("update_by >", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByGreaterThanOrEqualTo(String value) {
            addCriterion("update_by >=", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLessThan(String value) {
            addCriterion("update_by <", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLessThanOrEqualTo(String value) {
            addCriterion("update_by <=", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLike(String value) {
            addCriterion("update_by like", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotLike(String value) {
            addCriterion("update_by not like", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByIn(List<String> values) {
            addCriterion("update_by in", values, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotIn(List<String> values) {
            addCriterion("update_by not in", values, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByBetween(String value1, String value2) {
            addCriterion("update_by between", value1, value2, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotBetween(String value1, String value2) {
            addCriterion("update_by not between", value1, value2, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNull() {
            addCriterion("update_date is null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNotNull() {
            addCriterion("update_date is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateEqualTo(Date value) {
            addCriterion("update_date =", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotEqualTo(Date value) {
            addCriterion("update_date <>", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThan(Date value) {
            addCriterion("update_date >", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("update_date >=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThan(Date value) {
            addCriterion("update_date <", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThanOrEqualTo(Date value) {
            addCriterion("update_date <=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIn(List<Date> values) {
            addCriterion("update_date in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotIn(List<Date> values) {
            addCriterion("update_date not in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateBetween(Date value1, Date value2) {
            addCriterion("update_date between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotBetween(Date value1, Date value2) {
            addCriterion("update_date not between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNull() {
            addCriterion("remarks is null");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNotNull() {
            addCriterion("remarks is not null");
            return (Criteria) this;
        }

        public Criteria andRemarksEqualTo(String value) {
            addCriterion("remarks =", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotEqualTo(String value) {
            addCriterion("remarks <>", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThan(String value) {
            addCriterion("remarks >", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThanOrEqualTo(String value) {
            addCriterion("remarks >=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThan(String value) {
            addCriterion("remarks <", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThanOrEqualTo(String value) {
            addCriterion("remarks <=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLike(String value) {
            addCriterion("remarks like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotLike(String value) {
            addCriterion("remarks not like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksIn(List<String> values) {
            addCriterion("remarks in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotIn(List<String> values) {
            addCriterion("remarks not in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksBetween(String value1, String value2) {
            addCriterion("remarks between", value1, value2, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotBetween(String value1, String value2) {
            addCriterion("remarks not between", value1, value2, "remarks");
            return (Criteria) this;
        }

        public Criteria andDelFlagIsNull() {
            addCriterion("del_flag is null");
            return (Criteria) this;
        }

        public Criteria andDelFlagIsNotNull() {
            addCriterion("del_flag is not null");
            return (Criteria) this;
        }

        public Criteria andDelFlagEqualTo(String value) {
            addCriterion("del_flag =", value, "delFlag");
            return (Criteria) this;
        }

        public Criteria andDelFlagNotEqualTo(String value) {
            addCriterion("del_flag <>", value, "delFlag");
            return (Criteria) this;
        }

        public Criteria andDelFlagGreaterThan(String value) {
            addCriterion("del_flag >", value, "delFlag");
            return (Criteria) this;
        }

        public Criteria andDelFlagGreaterThanOrEqualTo(String value) {
            addCriterion("del_flag >=", value, "delFlag");
            return (Criteria) this;
        }

        public Criteria andDelFlagLessThan(String value) {
            addCriterion("del_flag <", value, "delFlag");
            return (Criteria) this;
        }

        public Criteria andDelFlagLessThanOrEqualTo(String value) {
            addCriterion("del_flag <=", value, "delFlag");
            return (Criteria) this;
        }

        public Criteria andDelFlagLike(String value) {
            addCriterion("del_flag like", value, "delFlag");
            return (Criteria) this;
        }

        public Criteria andDelFlagNotLike(String value) {
            addCriterion("del_flag not like", value, "delFlag");
            return (Criteria) this;
        }

        public Criteria andDelFlagIn(List<String> values) {
            addCriterion("del_flag in", values, "delFlag");
            return (Criteria) this;
        }

        public Criteria andDelFlagNotIn(List<String> values) {
            addCriterion("del_flag not in", values, "delFlag");
            return (Criteria) this;
        }

        public Criteria andDelFlagBetween(String value1, String value2) {
            addCriterion("del_flag between", value1, value2, "delFlag");
            return (Criteria) this;
        }

        public Criteria andDelFlagNotBetween(String value1, String value2) {
            addCriterion("del_flag not between", value1, value2, "delFlag");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}