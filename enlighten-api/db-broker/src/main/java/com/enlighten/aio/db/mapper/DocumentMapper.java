package com.enlighten.aio.db.mapper;

import com.enlighten.aio.db.model.Document;
import com.enlighten.aio.db.model.DocumentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface DocumentMapper {
    long countByExample(DocumentExample example);

    int deleteByExample(DocumentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Document record);

    int insertSelective(Document record);

    List<Document> selectByExampleWithBLOBsWithRowbounds(DocumentExample example, RowBounds rowBounds);

    List<Document> selectByExampleWithBLOBs(DocumentExample example);

    List<Document> selectByExampleWithRowbounds(DocumentExample example, RowBounds rowBounds);

    List<Document> selectByExample(DocumentExample example);

    Document selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Document record, @Param("example") DocumentExample example);

    int updateByExampleWithBLOBs(@Param("record") Document record, @Param("example") DocumentExample example);

    int updateByExample(@Param("record") Document record, @Param("example") DocumentExample example);

    int updateByPrimaryKeySelective(Document record);

    int updateByPrimaryKeyWithBLOBs(Document record);

    int updateByPrimaryKey(Document record);
}