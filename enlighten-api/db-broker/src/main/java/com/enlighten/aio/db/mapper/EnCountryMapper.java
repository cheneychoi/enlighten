package com.enlighten.aio.db.mapper;

import com.enlighten.aio.db.model.EnCountry;
import com.enlighten.aio.db.model.EnCountryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface EnCountryMapper {
    long countByExample(EnCountryExample example);

    int deleteByExample(EnCountryExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(EnCountry record);

    int insertSelective(EnCountry record);

    List<EnCountry> selectByExampleWithRowbounds(EnCountryExample example, RowBounds rowBounds);

    List<EnCountry> selectByExample(EnCountryExample example);

    EnCountry selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") EnCountry record, @Param("example") EnCountryExample example);

    int updateByExample(@Param("record") EnCountry record, @Param("example") EnCountryExample example);

    int updateByPrimaryKeySelective(EnCountry record);

    int updateByPrimaryKey(EnCountry record);
}