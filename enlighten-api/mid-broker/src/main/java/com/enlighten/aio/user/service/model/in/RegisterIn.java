package com.enlighten.aio.user.service.model.in;

import org.springframework.beans.BeanUtils;

import com.enlighten.aio.db.model.User;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "注册用户-入")
public class RegisterIn {
	@ApiModelProperty(value = "用户用户名")
	private String nickname;
	@ApiModelProperty(value = "用户邮箱")
	private String email;
	@ApiModelProperty(value = "用户密码")
	private String password;
	
	public User toUser() {
		User user = new User();
		BeanUtils.copyProperties(this, user);
		return user;
	}


	public String getNickname() {
		return nickname;
	}


	public void setNickname(String nickname) {
		this.nickname = nickname;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
