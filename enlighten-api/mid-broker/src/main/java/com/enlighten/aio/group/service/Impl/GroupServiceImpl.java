package com.enlighten.aio.group.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enlighten.aio.db.mapper.CommentMapper;
import com.enlighten.aio.db.mapper.GroupMapper;
import com.enlighten.aio.db.mapper.UserGroupMapper;
import com.enlighten.aio.db.model.CommentExample;
import com.enlighten.aio.db.model.CommentWithBLOBs;
import com.enlighten.aio.db.model.Group;
import com.enlighten.aio.db.model.GroupExample;
import com.enlighten.aio.db.model.GroupWithBLOBs;
import com.enlighten.aio.db.model.out.GroupOut;
import com.enlighten.aio.group.service.GroupService;
import com.x.share.db.model.Pagination;
import com.x.share.mid.exception.HelperException;
import com.x.share.mid.utils.ToolUtil;

@Service
public class GroupServiceImpl implements GroupService{
	@Resource
	GroupMapper groupMapper;
	@Resource
	UserGroupMapper userGroupMapper;
	@Resource
	CommentMapper commentMapper;
	
	@Override
	public boolean insertGroup(GroupWithBLOBs g) {
		g.setCreateDate(ToolUtil.getTime(0));
		g.setIsAgree(2);
		int num = groupMapper.insertSelective(g);
		return num > 0 ? true : false;
	}

	@Override
	@Transactional
	public boolean agreeGroup(Long id,Integer isAgree) {
		GroupWithBLOBs g = new GroupWithBLOBs();
		g.setIsAgree(1);
		g.setId(id);
		int num = groupMapper.updateByPrimaryKeySelective(g);
		return num > 0 ? true : false;
	}

	@Override
	public Pagination<Group> getGroup(Integer pageNo, Integer pageSize) {
		Pagination<Group> page = new Pagination<>(pageNo, pageSize);
		GroupExample ge = new GroupExample();
		List<Group> list = groupMapper.selectByExample(ge);
		int count = (int) groupMapper.countByExample(ge);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

	@Override
	public boolean delGroup(Long id) {
		int num = groupMapper.deleteByPrimaryKey(id);
		if(num != 1) {
			throw new HelperException("删除失败");
		}
		return num > 0 ? true : false;
	}

	@Override
	public boolean editGroup(GroupWithBLOBs g) {
		int num =groupMapper.updateByPrimaryKeySelective(g);
		if(num !=1) {
			throw new HelperException("编辑失败");
		}
		return num > 0 ? true : false;
	}

	@Override
	public boolean RefuseGroup(Long id) {
		int num = groupMapper.deleteByPrimaryKey(id);
		if(num != 1){
			throw new HelperException("操作失败");
		}
		return num > 0 ? true : false;
	}

	@Override
	public boolean commentGroup(CommentWithBLOBs c) {
		c.setToUid(null);
		c.setCreateDate(ToolUtil.getTime(0));
		int num = commentMapper.insertSelective(c);
		if(num != 1) {
			throw new HelperException("评论失败");
		}
  		return num > 0 ? true : false;
	}

	@Override
	public boolean ReplyComment(CommentWithBLOBs c) {
		c.setCreateDate(ToolUtil.getTime(0));
		int num = commentMapper.insertSelective(c);
		if(num != 1) {
			throw new HelperException("回复失败");
		}
		return num > 0 ? true : false;
	}

	@Override
	public Pagination<Group> getAllGroup(Integer pageNo, Integer pageSize) {
		Pagination<Group> page = new Pagination<>(pageNo, pageSize);
		GroupExample ge = new GroupExample();
		List<Group> list = groupMapper.selectByExample(ge);
		int count = (int) groupMapper.countByExample(ge);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

	@Override
	public List<CommentWithBLOBs> getComment(Long groupId) {
		CommentExample ce = new CommentExample();
		ce.or().andGroupIdEqualTo(groupId);
		List<CommentWithBLOBs> list = commentMapper.selectByExampleWithBLOBs(ce);
		return list;
	}

	@Override
	public GroupOut getGroupById(Long id) {
		GroupOut groupOut = groupMapper.getGroupById(id);
		return groupOut;
	}

	@Override
	public boolean addGroup(GroupWithBLOBs g) {
		int num = groupMapper.insertSelective(g);
		if(num != 1) {
			throw new HelperException("添加失败");
		}
		return num > 0 ? true : false;
	}

}
