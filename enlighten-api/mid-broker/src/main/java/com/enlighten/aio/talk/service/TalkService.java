package com.enlighten.aio.talk.service;

import java.util.List;

import com.enlighten.aio.db.model.TalkWithBLOBs;
import com.enlighten.aio.db.model.out.TalkOut;

public interface TalkService {
	/**
	 * 根据用户id查询私信列表
	 */
	  List<TalkOut> getTalk(Long receiveId);
	/**
	 * 发送消息
	 */
	boolean sendTalk(TalkWithBLOBs t);
	/**
	 * 读取信息后更改状态
	 * sendId=发消息id
	 * receiveId=读取人、接收人id
	 */
	boolean readTalk(Long sendId,Long receiveId);
}
