package com.enlighten.aio.doc.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.enlighten.aio.db.model.Document;

public interface DocumentService {
	/**
	 * 登陆用户显示的文档
	 * Lu
	 *2018年8月17日
	 */
	List<Document> getDocumentById(Long userId);
	/**
	 * 未登录显示所有的文档
	 */
	List<Document> getDocument();
	/**
	 * 用户上传文档
	 * Lu
	 *2018年8月30日
	 */
	String uploadtDocument(MultipartFile file,Long userId,Integer authority);
	
	/**
	 * 删除指定文档
	 */
	boolean delDocument(Long id);
	
	/**
	 * 下载文档
	 */
	String getDoc(Long id);
}
