package com.enlighten.aio.group.service;

import java.util.List;

import com.enlighten.aio.db.model.CommentWithBLOBs;
import com.enlighten.aio.db.model.Group;
import com.enlighten.aio.db.model.GroupWithBLOBs;
import com.enlighten.aio.db.model.out.GroupOut;
import com.x.share.db.model.Pagination;

public interface GroupService {
	/**
	 * 创建群组
	 */
	boolean insertGroup(GroupWithBLOBs g);
	/**
	 * 后台同意申请群组
	 */
	boolean agreeGroup(Long id,Integer isAgree);
	/**
	 * 后台群组列表
	 */
	Pagination<Group> getGroup(Integer pageNo, Integer pageSize);
	/**
	 * 后台添加群组
	 * Lu
	 *2018年8月30日
	 */
	boolean addGroup(GroupWithBLOBs g);
	
	/**
	 * 后台删除群组
	 */
	boolean delGroup(Long id);
	/**
	 * 后台编辑群组
	 */
	boolean editGroup(GroupWithBLOBs g);
	/**
	 * 拒绝申请群组
	 */
	boolean RefuseGroup(Long id);
	/**
	 * 前端查看所有群组列表
	 * Lu
	 *2018年8月28日
	 */
	Pagination<Group> getAllGroup(Integer pageNo, Integer pageSize);
	/**
	 * 群组评论
	 */
	boolean commentGroup(CommentWithBLOBs c);
	/**
	 * 回复评论
	 * Lu
	 *2018年8月28日
	 */
	boolean ReplyComment(CommentWithBLOBs c);
	/**
	 * 点进群组显示评论和回复
	 * Lu
	 *2018年8月28日
	 */
	List<CommentWithBLOBs> getComment(Long groupId);
	
	/**
     * 点击群组显示群组信息
     * Lu
     *2018年8月30日
     */
	GroupOut getGroupById(Long id);
	
}
