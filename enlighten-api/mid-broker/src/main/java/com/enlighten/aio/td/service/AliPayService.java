package com.enlighten.aio.td.service;

import java.util.Map;

public interface AliPayService {
	/**
	 * 支付支付通知
	 * 
	 * @param notify
	 * @return
	 */
	int alipayNotify(Map<String, String> notify);
}
