package com.enlighten.aio.pic.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.enlighten.aio.db.mapper.PictureMapper;
import com.enlighten.aio.db.model.Picture;
import com.enlighten.aio.db.model.PictureExample;
import com.enlighten.aio.pic.service.PictureService;
import com.x.share.db.model.Pagination;
import com.x.share.mid.exception.HelperException;
import com.x.share.mid.prop.AliOSSProperties;
import com.x.share.mid.utils.AliOSSUtils;
import com.x.share.mid.utils.ToolUtil;
@Service
public class PictureServiceImpl implements PictureService{
	@Resource
	PictureMapper pictureMapper;
	@Resource
	AliOSSProperties aliOSSProperties;
	
	
	@Override
	public String uploadPicture(MultipartFile file, Long pictureId, Integer PictureType) {
		int picNum=0;
		String key = null;
		try {
			if(pictureId == 0) {
				key = AliOSSUtils.uploadToOSS(aliOSSProperties, file.getInputStream());
			} else {
				Picture pic = pictureMapper.selectByPrimaryKey(pictureId);
				key = AliOSSUtils.uploadToOSS(aliOSSProperties, file.getInputStream(), pic.getOssId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		String type = file.getContentType();
		String picUrl = aliOSSProperties.getBucket() + "." + aliOSSProperties.getGateway() + "/" + key;
		if(type.indexOf("image") != -1) {
			Picture pic = new Picture();
			String fileName = file.getOriginalFilename();
			String name = fileName.substring(0, fileName.indexOf("."));
			pic.setPictureName(name);
			pic.setOssId(key);
			pic.setPictureUrl(picUrl);
			pic.setCreateDate(ToolUtil.getTime(0));
			picNum = pictureMapper.insertSelective(pic);
		}else {
			throw new HelperException("图片类型不对");
		}
		if(picNum == 0) {
			AliOSSUtils.removeFromOSS(aliOSSProperties, key);
			throw new HelperException("图片保存失败");
		}
		return picUrl;
	}
	
	@Override
	public void uploadPic(MultipartFile file,Integer PictureType) {
		uploadPicture(file, 0L, PictureType);
	}

	@Override
	public List<Picture> getHomePic() {
		PictureExample pe = new PictureExample();
		pe.or().andPictureTypeEqualTo(3);
		List<Picture> list = pictureMapper.selectByExample(pe);
		return list;
	}


	@Override
	public List<Picture> getBottomPic() {
		PictureExample pe = new PictureExample();
		pe.or().andPictureTypeEqualTo(4);
		List<Picture> list = pictureMapper.selectByExample(pe);
		return list;
	}


	@Override
	public Pagination<Picture> getPic(Integer pageNo, Integer pageSize) {
		Pagination<Picture> page = new Pagination<>(pageNo, pageSize);
		PictureExample pe = new PictureExample();
		List<Picture> list = pictureMapper.selectByExample(pe);
		page.setItems(list);
		page.setRecords(list.size());
		return page;
	}


	@Override
	public boolean editPic(Picture p) {
		int num = pictureMapper.updateByPrimaryKeySelective(p);
		if(num != 1) {
			throw new HelperException("编辑失败");
		}
		return num > 0 ? true : false;
	}


	@Override
	public boolean delPic(Long id) {
		int num = pictureMapper.deleteByPrimaryKey(id);
		if(num != 1) {
			throw new HelperException("删除失败");
		}
		return num > 0 ? true : false;
	}

	@Override
	public Picture getPicById(Long id) {
		Picture picture = pictureMapper.selectByPrimaryKey(id);
		return picture;
	}

}
