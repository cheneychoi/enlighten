package com.enlighten.aio.user.service.model.in;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.enlighten.aio.db.model.User;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "修改用户-入")
public class UserIn {
	@ApiModelProperty(value = "用户id")
    private Long id;
	@ApiModelProperty(value = "用户姓")
    private String surname;
	@ApiModelProperty(value = "用户名")
    private String name;
	@ApiModelProperty(value = "用户头像")
    private String avator;
	@ApiModelProperty(value = "用户籍贯")
    private String jiguan;
	@ApiModelProperty(value = "用户出生年月日")
    private Date birthday;
	@ApiModelProperty(value = "用户出生地")
    private String birthPlace;
	@ApiModelProperty(value = "用户语言/方言")
	private String dialect;
	@ApiModelProperty(value = "用户父亲民族")
    private String fatherNationality;
	@ApiModelProperty(value = "用户父亲语言")
    private String fatherLanguage;
	@ApiModelProperty(value = "用户父亲国籍")
	private String fatherCountry;
	@ApiModelProperty(value = "用户母亲国籍")
	private String motherCoutry;
	@ApiModelProperty(value = "用户母亲籍贯")
    private String motherJiguan;
	@ApiModelProperty(value = "用户母亲民族")
    private String motherNationality;
	@ApiModelProperty(value = "用户母亲语言")
    private String motherLanguage;
	@ApiModelProperty(value = "用户父系Y-SNP单倍群")
    private String ySnp;
	@ApiModelProperty(value = "1 : 推测 2 : 已测SND 3：已测序")
	private Integer state;
	@ApiModelProperty(value = "用户测序id")
    private String sequence;
	@ApiModelProperty(value = "用户父系Y-STR单倍型")
    private String yStr;
	@ApiModelProperty(value = "用户母系mtDNA单倍群")
    private String mtdna;
	@ApiModelProperty(value = "1: 姓氏 2：姓氏的堂号或分支 3：籍贯")
	private String isAgree;
	@ApiModelProperty(value = "1：姓氏 2：姓氏的堂号或分支 3：籍贯 4：Y-STR/ Y Bam 5：mtDNA")
	private String research;
	@ApiModelProperty(value = "用户修改时间")
    private Date updateDate;
    
	public User toUser() {
		User user = new User();
		BeanUtils.copyProperties(this, user);
		return user;
	}
	
	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getIsAgree() {
		return isAgree;
	}

	public void setIsAgree(String isAgree) {
		this.isAgree = isAgree;
	}

	public String getResearch() {
		return research;
	}

	public void setResearch(String research) {
		this.research = research;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAvator() {
		return avator;
	}

	public void setAvator(String avator) {
		this.avator = avator;
	}

	public String getJiguan() {
		return jiguan;
	}

	public void setJiguan(String jiguan) {
		this.jiguan = jiguan;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}

	public String getFatherNationality() {
		return fatherNationality;
	}

	public void setFatherNationality(String fatherNationality) {
		this.fatherNationality = fatherNationality;
	}

	public String getFatherLanguage() {
		return fatherLanguage;
	}

	public void setFatherLanguage(String fatherLanguage) {
		this.fatherLanguage = fatherLanguage;
	}

	public String getFatherCountry() {
		return fatherCountry;
	}

	public void setFatherCountry(String fatherCountry) {
		this.fatherCountry = fatherCountry;
	}

	public String getMotherCoutry() {
		return motherCoutry;
	}

	public void setMotherCoutry(String motherCoutry) {
		this.motherCoutry = motherCoutry;
	}

	public String getMotherJiguan() {
		return motherJiguan;
	}

	public void setMotherJiguan(String motherJiguan) {
		this.motherJiguan = motherJiguan;
	}

	public String getMotherNationality() {
		return motherNationality;
	}

	public void setMotherNationality(String motherNationality) {
		this.motherNationality = motherNationality;
	}

	public String getMotherLanguage() {
		return motherLanguage;
	}

	public void setMotherLanguage(String motherLanguage) {
		this.motherLanguage = motherLanguage;
	}

	public String getySnp() {
		return ySnp;
	}

	public void setySnp(String ySnp) {
		this.ySnp = ySnp;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getyStr() {
		return yStr;
	}

	public void setyStr(String yStr) {
		this.yStr = yStr;
	}

	public String getMtdna() {
		return mtdna;
	}

	public void setMtdna(String mtdna) {
		this.mtdna = mtdna;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	
}