package com.enlighten.aio.news.service.Impl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enlighten.aio.db.mapper.NewsMapper;
import com.enlighten.aio.db.model.NewsExample;
import com.enlighten.aio.db.model.NewsWithBLOBs;
import com.enlighten.aio.news.service.NewsService;
import com.x.share.db.model.Pagination;
import com.x.share.mid.exception.HelperException;
@Service("newsService")
public class NewsServiceImpl implements NewsService{
	@Autowired
	NewsMapper newsMapper;
	
	
	@Override
	public Pagination<NewsWithBLOBs> getAllNews(Integer pageNo, Integer pageSize) {
		Pagination<NewsWithBLOBs> page = new Pagination<>(pageNo, pageSize);
		NewsExample ne = new NewsExample();
		List<NewsWithBLOBs> list = newsMapper.selectByExampleWithBLOBs(ne);
		int count=(int) newsMapper.countByExample(ne);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}


	@Override
	public boolean insertNews(NewsWithBLOBs nb) {
		int num=newsMapper.insertSelective(nb);
		if(num==0) {
			throw new HelperException("添加失败");
		}
		return num >0 ? true : false;
	}


	@Override
	public boolean delNews(Long id) {
		int num=newsMapper.deleteByPrimaryKey(id);
		if(num==0) {
			throw new HelperException("删除失败");
		}
		return num > 0 ? true : false;
	}


	@Override
	public boolean editNews(NewsWithBLOBs nb) {
		int num=newsMapper.updateByPrimaryKeySelective(nb);
		if(num==0) {
			throw new HelperException("编辑失败");
		}
		return num > 0 ? true : false;
	}


	@Override
	public NewsWithBLOBs getNewsById(Long id) {
		NewsWithBLOBs news = newsMapper.selectByPrimaryKey(id);
		return news;
	}

}
