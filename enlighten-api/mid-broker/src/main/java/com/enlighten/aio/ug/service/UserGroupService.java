package com.enlighten.aio.ug.service;

import java.util.List;

import com.enlighten.aio.db.model.UserGroup;
import com.enlighten.aio.db.model.UserGroupOut;

public interface UserGroupService {
	/**
	 * 关注的群组
	 */
	List<UserGroupOut> getUserGroup(Long userId);
	/**
	 * 关注群组
	 */
	boolean insertUserGroup(UserGroup ug);
}
