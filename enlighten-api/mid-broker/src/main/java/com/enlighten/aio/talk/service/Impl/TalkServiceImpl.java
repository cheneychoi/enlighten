package com.enlighten.aio.talk.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.enlighten.aio.db.mapper.TalkMapper;
import com.enlighten.aio.db.model.TalkWithBLOBs;
import com.enlighten.aio.db.model.out.TalkOut;
import com.enlighten.aio.talk.service.TalkService;
import com.x.share.mid.exception.HelperException;
import com.x.share.mid.utils.ToolUtil;
@Service
public class TalkServiceImpl implements TalkService{
	@Resource
	TalkMapper talkMapper;
	@Override
	public List<TalkOut> getTalk(Long receiveId) {
		List<TalkOut> list = talkMapper.getTalk(receiveId);
		return list;
	}

	@Override
	public boolean sendTalk(TalkWithBLOBs t) {
		t.setState(2);
		t.setCreateDate(ToolUtil.getTime(0));
		int num = talkMapper.insertSelective(t);
		if(num != 1) {
			throw new HelperException("发送失败");
		}
		return num > 0 ? true : false;
	}

	@Override
	public boolean readTalk(Long sendId, Long receiveId) {
		int num = talkMapper.readTalk(sendId, receiveId);
		if(num != 1) {
			throw new HelperException("程序繁忙，请稍后再试");
		}
		return num > 0 ? true : false;
	}

}
