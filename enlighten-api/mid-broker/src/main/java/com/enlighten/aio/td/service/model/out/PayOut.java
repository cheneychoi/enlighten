package com.enlighten.aio.td.service.model.out;

import com.enlighten.aio.db.model.Order;
import com.enlighten.aio.db.model.OrderItem;
import com.enlighten.aio.db.model.Payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("支付-出")
public class PayOut {
	@ApiModelProperty("订单")
	private Order order;
	@ApiModelProperty("订单条目")
	private OrderItem orderItem;
	@ApiModelProperty("支付单")
	private Payment payment;
	@ApiModelProperty("微信扫码支持")
	private String wxQrCode2Url;


	public Order getOrder() {
		return order;
	}


	public void setOrder(Order order) {
		this.order = order;
	}


	public OrderItem getOrderItem() {
		return orderItem;
	}


	public void setOrderItem(OrderItem orderItem) {
		this.orderItem = orderItem;
	}


	public Payment getPayment() {
		return payment;
	}


	public void setPayment(Payment payment) {
		this.payment = payment;
	}


	public String getWxQrCode2Url() {
		return wxQrCode2Url;
	}


	public void setWxQrCode2Url(String wxQrCode2Url) {
		this.wxQrCode2Url = wxQrCode2Url;
	}


	public static PayOut from(Order order, OrderItem item, Payment pay) {
		PayOut out = new PayOut();
		out.setOrder(order);
		out.setOrderItem(item);
		out.setPayment(pay);
		return out;
	}
}
