package com.enlighten.aio.news.service;

import com.enlighten.aio.db.model.NewsWithBLOBs;
import com.x.share.db.model.Pagination;

public interface NewsService {
	/**
	 * 后台分页查看活动和新闻列表
	 */
	Pagination<NewsWithBLOBs> getAllNews(Integer pageNo, Integer pageSize);
	/**
	 * 后台添加新闻
	 */
	boolean insertNews(NewsWithBLOBs nb);
	/**
	 * 后台删除新闻
	 */
	boolean delNews(Long id);
	/**
	 * 后台编辑新闻
	 */
	boolean editNews(NewsWithBLOBs nb);
	/**
	 * 后台编辑带入数据
	 */
	NewsWithBLOBs getNewsById(Long id);
}
