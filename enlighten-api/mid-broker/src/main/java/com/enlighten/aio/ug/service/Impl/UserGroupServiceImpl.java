package com.enlighten.aio.ug.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.enlighten.aio.db.mapper.UserGroupMapper;
import com.enlighten.aio.db.model.UserGroup;
import com.enlighten.aio.db.model.UserGroupOut;
import com.enlighten.aio.ug.service.UserGroupService;
import com.x.share.mid.exception.HelperException;
@Service
public class UserGroupServiceImpl implements UserGroupService{
	@Resource
	UserGroupMapper userGroupMapper;

	@Override
	public List<UserGroupOut> getUserGroup(Long userId) {
		List<UserGroupOut> list = userGroupMapper.getUserGroup(userId);
		return list;
	}

	@Override
	public boolean insertUserGroup(UserGroup ug) {
		System.out.println(ug);
		int num = userGroupMapper.insertSelective(ug);
		if(num != 1) {
			throw new HelperException("添加失败");
		}
		return num > 0 ? true : false;
	}
	
}
