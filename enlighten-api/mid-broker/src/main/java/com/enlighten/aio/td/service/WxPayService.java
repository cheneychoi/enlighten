package com.enlighten.aio.td.service;

import java.util.Map;

public interface WxPayService {
	/**
	 * 微信支付通知
	 * 
	 * @param notify
	 * @return
	 * @see https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_7&index=3
	 */
	boolean processNotify(Map<String, String> notify);

	/**
	 * 微信退款通知(开通该功能需要在商户平台-交易中心-退款配置中配置notify_url)
	 * 
	 * @param notify
	 * @return
	 * @see https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_16&index=10
	 */
	boolean proceeRefundNotify(Map<String, String> notify);
}
