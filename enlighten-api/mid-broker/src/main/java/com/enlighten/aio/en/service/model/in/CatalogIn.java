package com.enlighten.aio.en.service.model.in;

import org.springframework.beans.BeanUtils;

import com.enlighten.aio.db.model.Catalog;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("商品-入")
public class CatalogIn {
	
	@ApiModelProperty(value = "商品id",example = "1")
	private Long catalogId;
	
	@ApiModelProperty(value = "商品名",example = "测试")
	private String name;

	@ApiModelProperty(value = "图片")
	private String img;

	@ApiModelProperty("价格")
	private Integer price;

	@ApiModelProperty("品牌")
	private String brand;
	
	@ApiModelProperty("数量")
	private Integer num;
	
	@ApiModelProperty("收货地址id")
	private Long addressId;
	
	public Long getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(Long catalogId) {
		this.catalogId = catalogId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public Catalog toCatalog() {
		Catalog catalog = new Catalog();
		BeanUtils.copyProperties(this, catalog);
		return catalog;
	}
}
