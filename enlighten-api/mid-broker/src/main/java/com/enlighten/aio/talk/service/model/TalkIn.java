package com.enlighten.aio.talk.service.model;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.enlighten.aio.db.model.TalkWithBLOBs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "发消息-入")
public class TalkIn {
	@ApiModelProperty(value = "发送用户id")
	private Long sendId;
	@ApiModelProperty(value = "接收人id")
	private Long receiveId;
	@ApiModelProperty(value = "消息内容")
	private String message;
/*	@ApiModelProperty(value = "读的状态")
	private Integer state;*/
	@ApiModelProperty(value = "发送时间")
	private Date createDate;
	public TalkWithBLOBs toTalk() {
		TalkWithBLOBs talk = new TalkWithBLOBs();
		BeanUtils.copyProperties(this, talk);
		return talk;
	}
	public Long getSendId() {
		return sendId;
	}
	public void setSendId(Long sendId) {
		this.sendId = sendId;
	}
	public Long getReceiveId() {
		return receiveId;
	}
	public void setReceiveId(Long receiveId) {
		this.receiveId = receiveId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
/*	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}*/
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
