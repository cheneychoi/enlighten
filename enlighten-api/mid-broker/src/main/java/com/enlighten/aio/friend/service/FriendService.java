package com.enlighten.aio.friend.service;

import java.util.List;

import com.enlighten.aio.db.model.Friend;
import com.enlighten.aio.db.model.out.FriendOut;

public interface FriendService {
/*	List<Friend> getAllMyFriend(Long userId);*/
	/**
	 * 添加好友
	 */
	boolean insertFriend(Long userId,Long friendId);
	/**
	 * 是否同意好友申请
	 */
	boolean AgreeFriendApplication(Long userId,Long friendId,Integer isFriend);
	/**
	 * 关注好友
	 */
	boolean AttentionFriend(Long userId,Long friendId);
	/**
	 * 进页面查询是否有好友申请(可以加是否被查看字段 进行操作后捎带改掉)
	 */
	List<Friend> getFriendAsk(Long userId);
	/**
	 * 查看我的好友列表
	 * Lu
	 *2018年8月28日
	 */
	List<FriendOut> getAllFriend(Long userId);
	 /**
     * 查看关注我的好友列表
     * Lu
     *2018年8月28日
     */
    List<FriendOut> getAllAttentionMy(Long userId);
    /**
     * 查看我关注的列表
     * Lu
     *2018年8月28日
     */
    List<FriendOut> getAllMyAttention(Long userId);
}
