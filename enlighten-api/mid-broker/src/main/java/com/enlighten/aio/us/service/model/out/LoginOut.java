package com.enlighten.aio.us.service.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("用户登录")
public class LoginOut {
	@ApiModelProperty(value = "用户id")
	private Long id;
	@ApiModelProperty(value = "用户昵称", example = "Henry")
	private String nickName;
	@ApiModelProperty(value = "用户手机号", example = "13524038227")
	private String phone;
	@ApiModelProperty(value = "用户头像", example = "avatar.png")
	private String avatar;
	@ApiModelProperty(value = "用户性别: 0 未知，1 男，2 女", example = "1")
	private Integer gender;
	@ApiModelProperty(value = "用户token,对用户id等信息加密后返回的字符串")
	private String token;
	@ApiModelProperty("用户状态： 0 绑定且设置好用户名及密码， 2 已绑定平台手机号但未设置密码及推荐人")
	private Integer status;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public Integer getGender() {
		return gender;
	}
	public void setGender(Integer gender) {
		this.gender = gender;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

	/*public static LoginOut from(EnUser user, ShareProperties prop) throws HelperException {
			LoginOut out = new LoginOut();
			BeanUtils.copyProperties(user, out);
			String nickName = StringUtils.isNotBlank(user.getNickName()) ? user.getNickName()
					: StringUtils.isNotBlank(user.getUserName()) ? user.getUserName() : user.getPhone();
			out.setNickName(nickName);
			UToken tk = new UToken();
			tk.setTimestamp(System.currentTimeMillis());
			tk.setId(user.getId());
			tk.setEnv(prop.getEnv());
			String token = CodecUtils.encrypt(JSON.toJSONString(tk), prop.getAeskey());
			out.setToken(token);
			return out;
		}*/

	/*public static LoginOut from(User user, WxUser wu, ShareProperties prop) throws HelperException {
			LoginOut out = from(user, prop);
			String avatar = wu.getAvatar();
			avatar = StringUtils.isBlank(avatar) ? null : avatar.substring(0, avatar.lastIndexOf("/") + 1);
			if (avatar != null) {
				out.setAvatar640(String.format("%s0", avatar));
				out.setAvatar132(String.format("%s132", avatar));
				out.setAvatar96(String.format("%s96", avatar));
				out.setAvatar64(String.format("%s64", avatar));
				out.setAvatar46(String.format("%s46", avatar));
			}
			if (StringUtils.isBlank(user.getNickName())) {
				out.setNickName(wu.getNickName());
			}
			if (StringUtils.isBlank(user.getAvatar())) {
				out.setAvatar(out.getAvatar132());
			}
			out.setWxUserId(wu.getId());
			return out;
		}

		public static LoginOut from(User user, QqUser qu, ShareProperties prop) throws HelperException {
			LoginOut out = from(user, prop);
			out.setAvatar100(qu.getFromJson("avatar100"));
			out.setAvatar50(qu.getFromJson("avatar50"));
			out.setAvatar40(qu.getFromJson("avatar40"));
			out.setAvatar30(qu.getFromJson("avatar30"));
			if (StringUtils.isBlank(user.getNickName())) {
				out.setNickName(qu.getNickName());
			}
			if (StringUtils.isBlank(out.getAvatar())) {
				out.setAvatar(StringUtils.isBlank(out.getAvatar100()) ? out.getAvatar40() : out.getAvatar100());
			}
			out.setQqUserId(qu.getId());
			return out;
		}*/
}
