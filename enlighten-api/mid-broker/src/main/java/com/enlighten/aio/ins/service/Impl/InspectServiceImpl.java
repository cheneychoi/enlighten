package com.enlighten.aio.ins.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.enlighten.aio.db.mapper.InspectMapper;
import com.enlighten.aio.db.model.Inspect;
import com.enlighten.aio.db.model.InspectExample;
import com.enlighten.aio.db.model.out.InspectOut;
import com.enlighten.aio.ins.service.InspectService;
import com.x.share.db.model.Pagination;
import com.x.share.mid.exception.HelperException;

@Service
public class InspectServiceImpl implements InspectService {

	@Resource
	InspectMapper inspectMapper;
	

	@Override
	public boolean EntryInspect(Inspect i) {
		int num = inspectMapper.insertSelective(i);
		if (num != 1) {
			throw new HelperException("录入失败");
		}
		return num > 0 ? true : false;
	}

	@Override
	public Pagination<InspectOut> getInspect(Integer pageNo, Integer pageSize) {
		Pagination<InspectOut> page = new Pagination<>(pageNo, pageSize);
		InspectExample ie = new InspectExample();
		List<InspectOut> list = inspectMapper.getInspect();
		int count= (int) inspectMapper.countByExample(ie);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

}
