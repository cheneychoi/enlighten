package com.enlighten.aio.group.service.model;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.enlighten.aio.db.model.CommentWithBLOBs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("评论-入")
public class CommentIn {
	@ApiModelProperty("群组id")
	private Long groupId;
	@ApiModelProperty("评论内容")
	private String content;
	@ApiModelProperty("评论用户id")
	private Long fromUid;
	@ApiModelProperty("评论目标用户id")
	private Long toUid;
	@ApiModelProperty("创建时间")
	private Date createDate;
    
	public CommentWithBLOBs toComment() {
		CommentWithBLOBs comment = new CommentWithBLOBs();
		BeanUtils.copyProperties(this, comment);
		return comment;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getFromUid() {
		return fromUid;
	}

	public void setFromUid(Long fromUid) {
		this.fromUid = fromUid;
	}

	public Long getToUid() {
		return toUid;
	}

	public void setToUid(Long toUid) {
		this.toUid = toUid;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
