package com.enlighten.aio.fav.service;

import com.enlighten.aio.db.model.Favorite;
import com.x.share.db.model.Pagination;

public interface FavoriteService {
	/**
	 * 收藏群组列表
	 * Lu
	 *2018年8月16日
	 */
	Pagination<Favorite> getFavorit(Long userId,Integer pageNo,Integer pageSize);
	/**
	 * 点击群组添加收藏
	 * Lu
	 *2018年8月16日
	 */
	boolean insertFavorit(Long userId,Long groupId);
}
