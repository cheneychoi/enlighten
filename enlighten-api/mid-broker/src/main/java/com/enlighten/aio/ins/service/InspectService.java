package com.enlighten.aio.ins.service;

import com.enlighten.aio.db.model.Inspect;
import com.enlighten.aio.db.model.out.InspectOut;
import com.x.share.db.model.Pagination;

public interface InspectService {
	/**
	 * 前台录入检测数据
	 * Lu
	 *2018年9月3日
	 */
	boolean EntryInspect(Inspect i);
	
	/**
     * 后台看用户录入检测数据
     * Lu
     *2018年9月3日
     */
    Pagination<InspectOut> getInspect(Integer pageNo, Integer pageSize);
}
