package com.enlighten.aio.pic.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.enlighten.aio.db.model.Picture;
import com.x.share.db.model.Pagination;

public interface PictureService {
	/**
	 * 上传图片
	 * Lu
	 *2018年8月29日
	 */
	String uploadPicture(MultipartFile file,Long pictureId,Integer picType);
	/**
	 * 获得首页图片
	 * Lu
	 *2018年8月29日
	 */
	List<Picture> getHomePic();
	/**
	 * 底部banner
	 * Lu
	 *2018年8月29日
	 */
	List<Picture> getBottomPic();
	/**
	 * 后台查看图片列表
	 */
	Pagination<Picture> getPic(Integer pageNo, Integer pageSize);
	/**
	 * 编辑图片
	 * Lu
	 *2018年8月30日
	 */
	boolean editPic(Picture p);
	/**
	 * 上传图片
	 * Lu
	 *2018年8月30日
	 */
	void uploadPic(MultipartFile file,Integer PictureType);
	/**
	 * 删除图片
	 * Lu
	 *2018年8月30日
	 */
	boolean delPic(Long id);
	/**
	 * 编辑时带入数据
	 * Lu
	 *2018年8月30日
	 */
	Picture getPicById(Long id);
}
