package com.enlighten.aio.user.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import com.enlighten.aio.db.model.User;
import com.enlighten.aio.db.model.out.UserOut;
import com.x.share.db.model.Pagination;

public interface UserService {
	Pagination<User> getAllUser(Integer pageNo, Integer pageSize);
	
	boolean insertUser(User u);
	
	boolean editUser(User u);
	
	boolean delUser(Long id);
	
	User getUserById(Long id);
	
	/**
	 * 前台用户个人信息更新
	 */
	boolean updateUser(User u,List<MultipartFile> files);
	/**
	 * 注册
	 */
	boolean RegisterUser(User u);
	/**
	 * 验证码
	 * Lu
	 *2018年8月27日
	 * @return 
	 */
	public String getCode(HttpServletRequest request);
	/**
	 * 登陆
	 * Lu
	 *2018年8月27日
	 */
	boolean login(String nickName,String password,String code,HttpServletRequest request);
	
	List<UserOut> getUserInformation(Long id);
}
