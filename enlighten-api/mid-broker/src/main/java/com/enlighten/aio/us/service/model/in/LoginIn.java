package com.enlighten.aio.us.service.model.in;

import org.apache.commons.lang3.StringUtils;

import com.x.share.mid.utils.ShareUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("用户登录-入")
public class LoginIn {
	@ApiModelProperty("手机")
	private String phone;
	@ApiModelProperty("验证码,密码或验证码二选一")
	private String code;
	@ApiModelProperty("密码,密码或验证码二选一")
	private String password;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPassword() {
		if (StringUtils.isNotBlank(password)) {
			return ShareUtils.encryptPassword(password);
		}
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
