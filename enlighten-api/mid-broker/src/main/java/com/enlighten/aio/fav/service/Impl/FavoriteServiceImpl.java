package com.enlighten.aio.fav.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.enlighten.aio.db.mapper.FavoriteMapper;
import com.enlighten.aio.db.model.Favorite;
import com.enlighten.aio.db.model.FavoriteExample;
import com.enlighten.aio.fav.service.FavoriteService;
import com.x.share.db.model.Pagination;
import com.x.share.mid.exception.HelperException;

@Service
public class FavoriteServiceImpl implements FavoriteService{
	@Resource
	FavoriteMapper favoriteMapper; 
	
	
	/**
	 * 收藏群组列表
	 */
	@Override
	public Pagination<Favorite> getFavorit(Long userId, Integer pageNo, Integer pageSize) {
		FavoriteExample example = new FavoriteExample();
		example.or().andUserIdEqualTo(userId);
		List<Favorite> list = favoriteMapper.selectByExampleWithBLOBs(example);
		int count = (int) favoriteMapper.countByExample(example);
		Pagination<Favorite> page = new Pagination<>(pageNo, pageSize);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}
	/**
	 * 点击群组添加收藏
	 * Lu
	 *2018年8月16日
	 */
	@Override
	public boolean insertFavorit(Long userId,Long groupId) {
		Favorite favorite = new Favorite();
		favorite.setUserId(userId);
		favorite.setGroupId(groupId);
		int num=favoriteMapper.insert(favorite);
		if(num==0) {
			throw new HelperException("收藏失败");
		}
		return num > 0 ? true : false;
	}
	
}
