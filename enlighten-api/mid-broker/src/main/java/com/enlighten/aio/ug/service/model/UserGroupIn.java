package com.enlighten.aio.ug.service.model;

import org.springframework.beans.BeanUtils;

import com.enlighten.aio.db.model.UserGroup;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("收藏群组-入")
public class UserGroupIn {
	@ApiModelProperty("用户id")
	private Long userId;
	@ApiModelProperty("群组id")
    private Long groupId;
	
	public UserGroup toUserGroup() {
		UserGroup ug = new UserGroup();
		BeanUtils.copyProperties(this, ug);
		return ug;
	}
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
}
