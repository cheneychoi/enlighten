package com.enlighten.aio.td.service;

import com.enlighten.aio.td.service.model.in.OrderIn;
import com.enlighten.aio.td.service.model.out.OrderOut;
import com.enlighten.aio.td.service.model.out.PayOut;
import com.x.share.db.model.Pagination;
import com.x.share.mid.exception.HelperException;

public interface OrderService {
	
	PayOut createOrder(OrderIn in);
	
	Pagination<OrderOut> listMyOrders(Long userId, String status, Integer pageNo,
			Integer pageSize);

	Integer removeOrder(String orderId , Long userId);
	
	OrderOut getOrder(String orderId);
	
	PayOut createOrderWithWxQrCode2(OrderIn in);
	
	String createOrderWithAliPagePay(OrderIn in) throws HelperException;
}
