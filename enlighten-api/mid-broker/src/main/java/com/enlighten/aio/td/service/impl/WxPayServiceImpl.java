package com.enlighten.aio.td.service.impl;

import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.enlighten.aio.db.Enum.PaymentVendor;
import com.enlighten.aio.td.service.AbstractTdPaymentService;
import com.enlighten.aio.td.service.WxPayService;
import com.x.share.mid.prop.WxProperties;

@Service
public class WxPayServiceImpl extends AbstractTdPaymentService implements WxPayService {

	Logger logger = LoggerFactory.getLogger(WxPayServiceImpl.class);

	@Resource
	WxProperties wxProps;

	@Override
	public boolean processNotify(Map<String, String> notify) {
		Date now = new Date();
		logger.info(">>>> weixin payment: {}", JSON.toJSONString(notify));
		if (notify.get("return_code").equals("SUCCESS") && notify.get("result_code").equals("SUCCESS")) {
			String no = notify.get("out_trade_no");// e采订单id
			String tradeNo = notify.get("transaction_id");// 交易流水号
			String payMethod = notify.get("bank_type");// 支付方式
			update4ConfirmPay(no, tradeNo, PaymentVendor.WX_PAY.getCode(), payMethod, now);
			return true;
		}
		return false;
	}

	@Override
	public boolean proceeRefundNotify(Map<String, String> notify) {
		// TODO Auto-generated method stub
		return false;
	}

	/*@Override
	public boolean proceeRefundNotify(Map<String, String> notify) {
		Date now = new Date();
		logger.info(">>>> weixin refund: {}", JSON.toJSONString(notify));
		if (notify.get("return_code").equals("SUCCESS")) {
			String key = DigestUtils.md5Hex(wxProps.getMerchantKey());
			String reqInfo = CodecUtils.decrypt(notify.get("req_info"), key);
			Map<String, Object> reqInfoMap = WxPayUtils.xml2Map(reqInfo);
			logger.info(">>>> decrypt info: {}", reqInfo);
			String no = (String) reqInfoMap.get("out_trade_no");// e采订单id
			String refundNo = (String) reqInfoMap.get("out_refund_no");// e采订退款单id
			String transId = (String) reqInfoMap.get("transaction_id");// 微信订单号
			String refundId = (String) reqInfoMap.get("refund_id");// 微信退款单号
			// 订单总金额，单位为分，只能为整数
			Integer totalFee = Integer.valueOf(String.format("%s", reqInfoMap.get("total_fee")));
			// 当该订单有使用非充值券时，返回此字段。应结订单金额=订单金额-非充值代金券金额，应结订单金额<=订单金额
			Integer settlementTotalFee = Integer.valueOf(String.format("%s", reqInfoMap.get("settlement_total_fee")));
			// 申请退款金额,单位分
			Integer refundFee = Integer.valueOf((String) reqInfoMap.get("refund_fee"));
			// 退款金额=申请退款金额-非充值代金券退款金额，退款金额<=申请退款金额
			Integer settlementRefundFee = Integer.valueOf(String.format("%s", reqInfoMap.get("settlement_refund_fee")));
			// SUCCESS-退款成功 CHANGE-退款异常 REFUNDCLOSE—退款关闭
			String refundStatus = String.format("%s", reqInfoMap.get("refund_status"));
			// 资金退款至用户帐号的时间，格式2017-12-15 09:46:01
			String successTime = String.format("%s", reqInfoMap.get("success_time"));
			// 取当前退款单的退款入账方
			String refundRecvAccout = String.format("%s", reqInfoMap.get("refund_recv_accout"));
			// 退款资金来源 REFUND_SOURCE_RECHARGE_FUNDS 可用余额退款/基本账户
			// REFUND_SOURCE_UNSETTLED_FUNDS 未结算资金退款
			String refundAccount = String.format("%s", reqInfoMap.get("refund_account"));
			// 退款发起来源 API接口 VENDOR_PLATFORM商户平台
			String refundRequestSource = String.format("%s", reqInfoMap.get("refund_request_source"));
			update4RefundWithOrderId(Long.valueOf(no), now);
			return true;
		}
		return false;
	}*/

}
