package com.enlighten.aio.pro.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.enlighten.aio.db.mapper.ProductMapper;
import com.enlighten.aio.db.model.Product;
import com.enlighten.aio.db.model.ProductExample;
import com.enlighten.aio.pro.service.ProductService;
import com.x.share.db.model.Pagination;
import com.x.share.mid.exception.HelperException;
@Service
public class ProductServiceImpl implements ProductService{
	@Resource
	ProductMapper productMapper;
	
	@Override
	public Pagination<Product> getAllProduct(Integer pageNo, Integer pageSize) {
		Pagination<Product> page = new Pagination<>(pageNo, pageSize);
		ProductExample pe = new ProductExample();
		List<Product> list = productMapper.selectByExampleWithBLOBs(pe);
		int count = (int) productMapper.countByExample(pe);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

	@Override
	public boolean insertProduct(Product p) {
		int num = productMapper.insert(p);
		if(num == 0) {
			throw new HelperException("添加失败");
		}
		return num > 0 ? true : false;
	}

	@Override
	public boolean editProduct(Product p) {
		int num = productMapper.updateByPrimaryKeySelective(p);
		if(num == 0) {
			throw new HelperException("编辑失败");
		}
		return num > 0 ? true : false;
	}

	@Override
	public boolean delProduct(Long id) {
		int num = productMapper.deleteByPrimaryKey(id);
		if(num == 0) {
			throw new HelperException("删除失败");
		}
		return num > 0 ? true : false;
	}

	@Override
	public Product getProductById(Long id) {
		Product pro = productMapper.selectByPrimaryKey(id);
		return pro;
	}
	
}
