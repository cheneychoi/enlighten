package com.enlighten.aio.td.service.model.out;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.enlighten.aio.db.model.Order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("订单-出")
public class OrderOut {

	@ApiModelProperty("订单id")
	private String id;

	@ApiModelProperty("商品id")
	private Long catalogId;

	@ApiModelProperty("用户id")
	private Long userId;

	@ApiModelProperty("商品名")
	private String productName;

	@ApiModelProperty("商品购买数量")
	private Integer productNum;

	@ApiModelProperty("价格")
	private Integer price;

	@ApiModelProperty("收货地址id")
	private Long addressId;

	@ApiModelProperty("操作状态")
	private Integer operationState;

	@ApiModelProperty("状态")
	private Integer state;

	@ApiModelProperty("发票id")
	private Long invoiceId;

	@ApiModelProperty("创建时间")
	private Date createDate;

	@ApiModelProperty("更新时间")
	private Date updateDate;

	@ApiModelProperty("备用字段")
	private String data;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(Long catalogId) {
		this.catalogId = catalogId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getProductNum() {
		return productNum;
	}

	public void setProductNum(Integer productNum) {
		this.productNum = productNum;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public Integer getOperationState() {
		return operationState;
	}

	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public static OrderOut from(Order order) {
		OrderOut out = new OrderOut();
		BeanUtils.copyProperties(order, out);
		//TODO  价格要不要/100
		return out;
	}
}
