package com.enlighten.aio.pro.service;

import com.enlighten.aio.db.model.Product;
import com.x.share.db.model.Pagination;

public interface ProductService {
	Pagination<Product> getAllProduct(Integer pageNo, Integer pageSize);
	boolean insertProduct(Product p);
	boolean editProduct(Product p);
	boolean delProduct(Long id);
	Product getProductById(Long id);
}
