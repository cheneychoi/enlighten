package com.enlighten.aio.group.service.model;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.enlighten.aio.db.model.GroupWithBLOBs;

public class GroupsIn {
    private String groupName;

    private String background;

    private String administrator;

    private String adminEmail;

    private String teamPlayer;
    
    private String qq;
    
    private String wx;

    private String result;

    private Long pictureId;

    private Date createDate;
    
    private String pictureUrl;
    
    public GroupWithBLOBs toGroup() {
		GroupWithBLOBs group = new GroupWithBLOBs();
		BeanUtils.copyProperties(this, group);
		return group;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public String getAdministrator() {
		return administrator;
	}

	public void setAdministrator(String administrator) {
		this.administrator = administrator;
	}

	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	public String getTeamPlayer() {
		return teamPlayer;
	}

	public void setTeamPlayer(String teamPlayer) {
		this.teamPlayer = teamPlayer;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWx() {
		return wx;
	}

	public void setWx(String wx) {
		this.wx = wx;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Long getPictureId() {
		return pictureId;
	}

	public void setPictureId(Long pictureId) {
		this.pictureId = pictureId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}
}
