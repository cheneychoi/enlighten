package com.enlighten.aio.td.service.model.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Cheney
 *
 */
@ApiModel("订单-入")
public class OrderIn {
	@ApiModelProperty("商品id")
	private Long catalogId;
	@ApiModelProperty("用户id")
	private Long userId;
	@ApiModelProperty("数量")
	private Integer quantity;
	@ApiModelProperty("收货地址")
	private Long addressId;//TODO
	@ApiModelProperty("支付方式")
	private String payMethod;
	@ApiModelProperty("支付成功后的回退地址")
	private String returnUrl;

	public Long getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(Long catalogId) {
		this.catalogId = catalogId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public String getPayMethod() {
		return payMethod;
	}

	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}
}
