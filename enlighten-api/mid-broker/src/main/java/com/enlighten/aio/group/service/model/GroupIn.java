package com.enlighten.aio.group.service.model;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.enlighten.aio.db.model.Group;
import com.enlighten.aio.db.model.GroupWithBLOBs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "添加群组-入")
public class GroupIn {
	@ApiModelProperty(value = "用户id")
	private Long userId;
	@ApiModelProperty(value = "申请原因")
	private String reason;
	@ApiModelProperty(value = "创建时间")
	private Date createDate;
/*	@ApiModelProperty(value = "是否同意（1：是  2： 不是）")
	private Integer isAgree;
*/	public GroupWithBLOBs toGroup() {
		GroupWithBLOBs group = new GroupWithBLOBs();
		BeanUtils.copyProperties(this, group);
		return group;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
/*	public Integer getIsAgree() {
		return isAgree;
	}
	public void setIsAgree(Integer isAgree) {
		this.isAgree = isAgree;
	}*/
	
}
