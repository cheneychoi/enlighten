package com.x.share.mid.utils;


import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

public class ShareUtilsTest {

	@Test
	public void testEncryptPassword() {
		String pass = ShareUtils.encryptPassword("123456");
		Assert.assertEquals("8653f914a1c037fa1164cc93d5a9c8d5", pass);
	}

	@Test
	public void test2Power() {

		long rlt = (1 << 0) + (1 << 1) + (1 << 2) + (1 << 3) + (1 << 4) + (1 << 5) + (1 << 6) + (1 << 7) + (1 << 8)
				+ (1 << 9);
		Assert.assertEquals(1023, rlt);
		rlt = (1 << 0) + (1 << 1) + (1 << 2) + (1 << 3) + (1 << 4) + (1 << 5) + (1 << 6) + (1 << 7) + (1 << 8)
				+ (1 << 9) + (1 << 10) + (1 << 11) + (1 << 12) + +(1 << 13) + (1 << 14) + (1 << 15) + (1 << 16)
				+ (1 << 17) + (1 << 18);
		Assert.assertEquals(524287, rlt);
		rlt = (1 << 0) + (1 << 1) + (1 << 2) + (1 << 3) + (1 << 4) + (1 << 5) + (1 << 6) + (1 << 7) + (1 << 8)
				+ (1 << 9) + (1 << 10) + (1 << 11) + (1 << 12) + +(1 << 13) + (1 << 14) + (1 << 15) + (1 << 16)
				+ (1 << 17) + (1 << 18) + (1 << 19) + (1 << 20) + (1 << 21) + +(1 << 22) + (1 << 23) + (1 << 24)
				+ (1 << 25) + (1 << 26) + (1 << 27);
		Assert.assertEquals(268435455, rlt);
		rlt = (1 << 0) + (1 << 1) + (1 << 2) + (1 << 3) + (1 << 4) + (1 << 5) + (1 << 6) + (1 << 7) + (1 << 8)
				+ (1 << 9) + (1 << 10) + (1 << 11) + (1 << 12) + +(1 << 13) + (1 << 14) + (1 << 15) + (1 << 16)
				+ (1 << 17) + (1 << 18) + (1 << 19) + (1 << 20) + (1 << 21) + +(1 << 22) + (1 << 23) + (1 << 24)
				+ (1 << 25) + (1 << 26) + (1 << 27) + (1 << 28) + (1 << 29) + (1 << 30);
		Assert.assertEquals(2147483647, rlt);
	}
	
	@Test
	public void testDate() {
		DateTime startDate = DateTime.parse(DateTime.now().toString("yyyy-MM-dd"));
		DateTime endDate = startDate.plusDays(1);
		System.out.println(startDate.toDate());
		System.out.println(endDate.toDate());
	}
	
	@Test
	public void testFormat() {
		System.out.println(String.format("%.2f", (float)10/100));
	}
}
