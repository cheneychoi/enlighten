package com.x.share.mid.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.lang.StringUtils;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.x.share.mid.prop.AlipayProperties;

public class AliPayUtils {
	static AlipayClient alipayClient;

	public static AlipayClient getAlipayClient(AlipayProperties props) {
		if (alipayClient == null) {
			Lock lock = new ReentrantLock();
			lock.lock();
			try {
				alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do", props.getAppId(),
						props.getRsa2PrivateKey(), "json", "UTF-8", props.getAliRsa2PublicKey(), "RSA2");
			} finally {
				lock.unlock();
			}
		}
		return alipayClient;
	}
	
	public static Map<String, String> notifyMap(Map<String, String[]> paramMap) {
		Map<String, String> nm = new HashMap<String, String>();
		for (Entry<String, String[]> en : paramMap.entrySet()) {
			String val = StringUtils.join(en.getValue(), ",");
			nm.put(en.getKey(), val);
		}
		return nm;
	}
}
