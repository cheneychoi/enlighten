/*package com.x.share.api.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.x.share.mid.model.FileIn;
import com.x.share.mid.prop.AliOSSProperties;
import com.x.share.mid.prop.ShareProperties;
import com.x.share.mid.utils.AliOSSUtils;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

//@Api("阿里云存储上传服务")
//@Controller
//@RequestMapping("/share/")
public class UploadController extends BaseController {
	Logger logger = LoggerFactory.getLogger(UploadController.class);
	@Resource
	AliOSSProperties ossProps;

	@Resource
	ShareProperties shareProps;

	@ApiOperation(value = "上传文件（可多个)并保存到阿里云oss", response = FileIn.class)
	@RequestMapping(value = "upload/multiple", method = { RequestMethod.POST })
	public @ResponseBody Map<String, Object> multipleUpload(
			@ApiParam(value = "选择本地要上传的文件") @RequestParam(value = "files[]", required = true) MultipartFile[] files,
			@ApiParam(value = "应用id") @RequestHeader("appId") String appId,
			@ApiParam(value = "应用accessToken") @RequestHeader("atk") String atk) {
		if (files == null) {
			return fail("请选择要上传的文件!");
		}
		logger.debug(">>>> upload files length: {}", files.length);
		List<FileIn> ins = new ArrayList<FileIn>();
		for (MultipartFile file : files) {
			try {
				String key = AliOSSUtils.uploadToOSS(ossProps, file.getInputStream());
				FileIn in = new FileIn(key, file.getOriginalFilename());
				ins.add(in);
			} catch (IOException e) {
				return fail("IO错误，请稍后再试!");
			}
		}
		return success(ins);
	}

	@ApiOperation(value = "上传文件并保存到阿里云oss", notes = "", response = FileIn.class)
	@RequestMapping(value = "upload", method = { RequestMethod.POST })
	public @ResponseBody Map<String, Object> uploadWithKey(
			@ApiParam(value = "选择本地要上传的文件") @RequestParam(value = "file", required = true) MultipartFile file,
			@ApiParam(value = "指定文件上传成功后的key(阿里存储文件的key,不传时自动生成)", example = "2017090512490035682") @RequestParam(value = "key", required = false) String key,
			@ApiParam(value = "应用id") @RequestHeader("appId") String appId,
			@ApiParam(value = "应用accessToken") @RequestHeader("atk") String atk) {
		if (file == null) {
			return fail("请选择要上传的文件!");
		}
		try {
			if (StringUtils.isEmpty(key)) {
				key = AliOSSUtils.uploadToOSS(ossProps, file.getInputStream());
			} else {
				key = AliOSSUtils.uploadToOSS(ossProps, file.getInputStream(), key);
			}
			String fileName = file.getOriginalFilename();
			logger.info(">>>> update files to ali oss: {}-{}", fileName, key);
			FileIn in = new FileIn(key, fileName);
			return success(in);
		} catch (IOException e) {
			return fail("IO错误，请稍后再试!");
		}
	}

	@ApiOperation(value = "上传文件（可多个)并保存到服务器", response = FileIn.class)
	@RequestMapping(value = "upload/multiple/local", method = { RequestMethod.POST })
	public @ResponseBody Map<String, Object> multipleUploadLocal(
			@ApiParam(value = "选择本地要上传的文件") @RequestParam(value = "files[]", required = true) MultipartFile[] files,
			@ApiParam(value = "应用id") @RequestHeader("appId") String appId,
			@ApiParam(value = "应用accessToken") @RequestHeader("atk") String atk) {
		if (files == null) {
			return fail("请选择要上传的文件!");
		}
		logger.debug(">>>> upload files length: {}", files.length);
		List<FileIn> ins = new ArrayList<FileIn>();
		for (MultipartFile file : files) {
			DateTime now = new DateTime();
			String dynFolder = now.toString("YYYYMM");
			String uploadPath = String.format("%s/%s", shareProps.getUploadPath(), dynFolder);
			File path = new File(uploadPath);
			if (!path.exists()) {
				path.mkdirs();
			}
			String oid = AliOSSUtils.generateOid();
			FileIn in = new FileIn(oid, file.getOriginalFilename());
			in.setExt(in.getFileName().substring(in.getFileName().lastIndexOf(".")));
			in.setPath(String.format("/upload/%s", dynFolder));
			File newFile = new File(String.format("%s/%s%s", uploadPath, oid, in.getExt()));
			try (FileOutputStream fop = new FileOutputStream(newFile)) {
				if (!newFile.exists()) {
					newFile.createNewFile();
				}
				fop.write(file.getBytes());
				fop.flush();
				fop.close();
				logger.debug(">>>> upload file success: {}", JSON.toJSONString(in));
			} catch (IOException e) {
				logger.error("!!!! upload file failed: {}", JSON.toJSONString(in));
				return fail("IO错误，请稍后再试!");
			}
			ins.add(in);
		}
		return success(ins);
	}

	@ApiOperation(value = "上传文件并保存到服务器", notes = "", response = FileIn.class)
	@RequestMapping(value = "upload/local", method = { RequestMethod.POST })
	public @ResponseBody Map<String, Object> uploadWithKeyLocal(
			@ApiParam(value = "选择本地要上传的文件") @RequestParam(value = "file", required = true) MultipartFile file,
			@ApiParam(value = "指定文件上传成功后的key(阿里存储文件的key,不传时自动生成)", example = "2017090512490035682") @RequestParam(value = "key", required = false) String key,
			@ApiParam(value = "应用id") @RequestHeader("appId") String appId,
			@ApiParam(value = "应用accessToken") @RequestHeader("atk") String atk) {
		if (file == null) {
			return fail("请选择要上传的文件!");
		}
		DateTime now = new DateTime();
		String dynFolder = now.toString("YYYYMM");
		String uploadPath = String.format("%s/%s", shareProps.getUploadPath(), dynFolder);
		File path = new File(uploadPath);
		if (!path.exists()) {
			path.mkdirs();
		}
		String oid = AliOSSUtils.generateOid();
		FileIn in = new FileIn(oid, file.getOriginalFilename());
		in.setExt(in.getFileName().substring(in.getFileName().lastIndexOf(".")));
		in.setPath(String.format("/upload/%s", dynFolder));
		File newFile = new File(String.format("%s/%s%s", uploadPath, oid, in.getExt()));
		try (FileOutputStream fop = new FileOutputStream(newFile)) {
			if (!newFile.exists()) {
				newFile.createNewFile();
			}
			fop.write(file.getBytes());
			fop.flush();
			fop.close();
			logger.debug(">>>> upload file success: {}", JSON.toJSONString(in));
		} catch (IOException e) {
			logger.error("!!!! upload file failed: {}", JSON.toJSONString(in));
			return fail("IO错误，请稍后再试!");
		}
		return success(in);
	}

	@ApiOperation(value = "从阿里云oss删除object", notes = "成功删除时返回code=0", response = Void.class)
	@RequestMapping(value = "upload/{key}", method = { RequestMethod.DELETE })
	public @ResponseBody Map<String, Object> remove(
			@ApiParam(value = "阿里存储文件的key", example = "2017090512490035682") @PathVariable("key") String key,
			@ApiParam(value = "应用id") @RequestHeader("appId") String appId,
			@ApiParam(value = "应用accessToken") @RequestHeader("atk") String atk) {
		AliOSSUtils.removeFromOSS(ossProps, key);
		logger.info(">>>> remove object from ali oss: {}", key);
		return success();
	}

}
*/