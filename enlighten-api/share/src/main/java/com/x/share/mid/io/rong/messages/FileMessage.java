package com.x.share.mid.io.rong.messages;

import com.x.share.mid.io.rong.util.GsonUtil;

/**
 *
 * 文件消息。
 *
 */
public class FileMessage extends BaseMessage {
	private String name;
	private Float size;
	private String type;
	private String fileUrl;
	private String extra = "";
	private transient static final String TYPE = "RC:FileMsg";

	public FileMessage(String name, Float size, String type, String fileUrl, String extra) {
		this.name = name;
		this.size = size;
		this.type = type;
		this.fileUrl = fileUrl;
		this.extra = extra;
	}

	public String getType() {
		return TYPE;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getSize() {
		return size;
	}

	public void setSize(Float size) {
		this.size = size;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return GsonUtil.toJson(this, FileMessage.class);
	}
}