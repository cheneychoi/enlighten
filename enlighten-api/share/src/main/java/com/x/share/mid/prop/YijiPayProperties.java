package com.x.share.mid.prop;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("yijipay")
public class YijiPayProperties {
	private String partnerId;
	private String secretKey;
	private String serviceGateway;
	private String siteDomain;
	private String notifyUrl;
	/**
	 * 单位:秒
	 */
	private String httpConnectTimeout;
	/**
	 * 单位:秒
	 */
	private String httpReadTimeout;
	/**
	 * 易极付支付 商家id 
	 */
	private String merchantId;

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getServiceGateway() {
		return serviceGateway;
	}

	public void setServiceGateway(String serviceGateway) {
		this.serviceGateway = serviceGateway;
	}

	public String getSiteDomain() {
		return siteDomain;
	}

	public void setSiteDomain(String siteDomain) {
		this.siteDomain = siteDomain;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getHttpConnectTimeout() {
		return httpConnectTimeout;
	}

	public void setHttpConnectTimeout(String httpConnectTimeout) {
		this.httpConnectTimeout = httpConnectTimeout;
	}

	public String getHttpReadTimeout() {
		return httpReadTimeout;
	}

	public void setHttpReadTimeout(String httpReadTimeout) {
		this.httpReadTimeout = httpReadTimeout;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
}
