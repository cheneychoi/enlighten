package com.x.share.mid.invoker.umeng.push;

import org.springframework.stereotype.Component;

@Component
public interface UmengPushInvoker {

	void sendAndroidBroadcast() throws Exception;

	void sendAndroidUnicast() throws Exception;

	void sendAndroidGroupcast() throws Exception;

	void sendAndroidCustomizedcast() throws Exception;

	void sendAndroidCustomizedcastFile() throws Exception;

	void sendAndroidFilecast() throws Exception;

	void sendIOSBroadcast() throws Exception;

	void sendIOSUnicast() throws Exception;

	void sendIOSGroupcast() throws Exception;

	void sendIOSCustomizedcast() throws Exception;

	void sendIOSFilecast() throws Exception;
}
