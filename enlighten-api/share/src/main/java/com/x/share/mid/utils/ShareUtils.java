package com.x.share.mid.utils;

import java.util.Map.Entry;
import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;

public class ShareUtils {

	static Logger logger = LoggerFactory.getLogger(ShareUtils.class);

	/**
	 * 对密码里德md5加密
	 * 
	 * @param rawPassword
	 * @return
	 */
	public static String encryptPassword(String rawPassword) {
		return DigestUtils.md5Hex(String.format("%s-jindun", rawPassword));
	}

	/**
	 * 是否是直辖市
	 * 
	 * @param cityName
	 * @return
	 */
	public static boolean isZXS(String cityName) {
		if (StringUtils.isBlank(cityName)) {
			return false;
		}
		if (cityName.contains("北京") || cityName.contains("上海") || cityName.contains("天津") || cityName.contains("重庆")) {
			return true;
		}
		return false;
	}

	/**
	 * 运营商号段如下：
	 * 中国联通号码：130、131、132、145（无线上网卡）、155、156、185（iPhone5上市后开放）、186、176（4G号段）、
	 *               175（2015年9月10日正式启用，暂只对北京、上海和广东投放办理）
	 * 中国移动号码：134、135、136、137、138、139、147（无线上网卡）、150、151、152、157、158、159、182、183、187、188、178
	 * 中国电信号码：133、153、180、181、189、177、173、149 虚拟运营商：170、1718、1719 
	 * 手机号前3位的数字包括：
	 * 1 :1
	 * 2 :3,4,5,7,8
	 * 3 :0,1,2,3,4,5,6,7,8,9 
	 * 总结： 目前java手机号码正则表达式有：
	 * a :"^1[3|4|5|7|8][0-9]\\d{4,8}$"    一般验证情况下这个就可以了
	 * b :"^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[013678])|(18[0,5-9]))\\d{8}$"
	 */
	public static boolean isChinaMobile(String phone) {
		String regex = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[0-9])|(18[0-9]))\\d{8}$";
		if (phone.length() != 11) {
			return false;
		} else {
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(phone);
			return m.matches();
		}
	}
	
	/**
	 * 合并json
	 * @param base
	 * @param update
	 * @return
	 */
	public static JSONObject mergeJSON(String base, String update) {
		if (StringUtils.isBlank(base)) {
			if (StringUtils.isBlank(update)) {
				return new JSONObject();
			} else {
				return JSONObject.parseObject(update);
			}
		} else {
			if (StringUtils.isBlank(update)) {
				return JSONObject.parseObject(base);
			} else {
				JSONObject bj = JSONObject.parseObject(base);
				JSONObject uj = JSONObject.parseObject(update);
				for (Entry<String, Object> entry : uj.entrySet()) {
					if (! bj.containsKey(entry.getKey())) {
						bj.put(entry.getKey(), entry.getValue());
					} else {
						bj.merge(entry.getKey(), entry.getValue(), new BiFunction<Object, Object, Object>() {
							@Override
							public Object apply(Object t, Object u) {
								return u;
							}
						});
					}
				}
				return bj;
			}
		}
	}
}