package com.x.share.mid.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("文件")
public class FileIn {
	@ApiModelProperty(value = "存储文件id", example = "2017090512490035682")
	private String oid;
	@ApiModelProperty(value = "存储文件路径", example = "/upload/201801")
	private String path;
	@ApiModelProperty(value = "文件扩展名", example = ".png")
	private String ext;
	@ApiModelProperty(value = "本地文件名(原文件名)", example = "我是一个测试文件.txt")
	private String fileName;
	@ApiModelProperty(value = "服务端文件路径", example = "/upload/201801/2017090512490035682.png")
	private String uri;

	public FileIn() {

	}

	public FileIn(String oid, String fileName) {
		this.oid = oid;
		this.fileName = fileName;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getUri() {
		uri = String.format("%s/%s%s", path, oid, ext);
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
}
