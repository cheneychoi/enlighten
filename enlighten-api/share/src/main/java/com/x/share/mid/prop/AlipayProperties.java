package com.x.share.mid.prop;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("alipay")
public class AlipayProperties {
	private String appId;
	private String md5Key;
	private String rsa2PrivateKey;
	private String aliRsa2PublicKey;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getMd5Key() {
		return md5Key;
	}

	public void setMd5Key(String md5Key) {
		this.md5Key = md5Key;
	}

	public String getRsa2PrivateKey() {
		return rsa2PrivateKey;
	}

	public void setRsa2PrivateKey(String rsa2PrivateKey) {
		this.rsa2PrivateKey = rsa2PrivateKey;
	}

	public String getAliRsa2PublicKey() {
		return aliRsa2PublicKey;
	}

	public void setAliRsa2PublicKey(String aliRsa2PublicKey) {
		this.aliRsa2PublicKey = aliRsa2PublicKey;
	}
	
}
