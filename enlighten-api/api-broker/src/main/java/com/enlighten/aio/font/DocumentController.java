package com.enlighten.aio.font;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.enlighten.aio.db.model.Document;
import com.enlighten.aio.doc.service.DocumentService;
import com.x.share.api.controller.BaseController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value="/enlighten/font")
public class DocumentController extends BaseController{
	@Resource
	DocumentService documentService;
	
	@ApiOperation(value="登陆用户查看文档列表", notes="" )
	@RequestMapping(value="/getDocumentById",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getDocumentById(@ApiParam(value="用户id") @RequestParam(value="userId") Long userId){
		List<Document> list = documentService.getDocumentById(userId);
		return success(list);
	}
	@ApiOperation(value="未登录查看文档列表", notes="" )
	@RequestMapping(value="/getDocument",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getDocument(){
		List<Document> list = documentService.getDocument();
		return success(list);
	}
	
	@ApiOperation(value="删除指定文档", notes="" )
	@RequestMapping(value="/delDocument",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> delDocument(@ApiParam(value="文档id") @RequestParam(value="id") Long id){
		return success(documentService.delDocument(id));
	}
	
	@ApiOperation(value = "上传文档", notes = "上传Banner图")
	@RequestMapping(value = "/uploadtDocument", method = { RequestMethod.POST })
	@ResponseBody
	public Map<String, Object> uploadtDocument(MultipartFile file,
			@ApiParam(value="用户id") @RequestParam(value="userId")Long userId,
			@ApiParam(value="权限") @RequestParam(value="authority")Integer authority) {
		System.out.println("file============="+file);
		documentService.uploadtDocument(file, userId, authority);
		return success();
	}
	
	@ApiOperation(value="下载文档", notes="" )
	@RequestMapping(value="/getDoc",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getDoc(
			@ApiParam(value="文档id") @RequestParam(value="id") Long id){
		return success(documentService.getDoc(id));
	}
}
