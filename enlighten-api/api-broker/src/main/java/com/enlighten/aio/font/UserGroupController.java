package com.enlighten.aio.font;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.enlighten.aio.db.model.UserGroupOut;
import com.enlighten.aio.ug.service.UserGroupService;
import com.enlighten.aio.ug.service.model.UserGroupIn;
import com.x.share.api.controller.BaseController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
@Controller
@RequestMapping(value="/enlighten/font")
public class UserGroupController extends BaseController{
	@Resource
	UserGroupService userGroupService;
	
	@ApiOperation(value="关注群组列表", notes="" )
	@RequestMapping(value="/getUserGroup",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getUserGroup(
			@ApiParam(value="用户id") @RequestParam(value="userId") Long userId){
		List<UserGroupOut> list = userGroupService.getUserGroup(userId);
		return success(list);
	}
	
	@ApiOperation(value="收藏群组", notes="" )
	@RequestMapping(value="/insertUserGroup",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> insertUserGroup(
			@RequestBody UserGroupIn in){
		return success(userGroupService.insertUserGroup(in.toUserGroup()));
	}
}
