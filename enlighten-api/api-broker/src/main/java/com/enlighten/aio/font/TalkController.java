package com.enlighten.aio.font;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.enlighten.aio.db.model.out.TalkOut;
import com.enlighten.aio.talk.service.TalkService;
import com.enlighten.aio.talk.service.model.TalkIn;
import com.x.share.api.controller.BaseController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
public class TalkController extends BaseController{
	@Resource
	TalkService talkService;
	
	@ApiOperation(value="私信列表", notes="" )
	@RequestMapping(value="/geTalks",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> geTalks(
			@ApiParam(value="用户id") @RequestParam(value="receiveId") Long receiveId
			){
		List<TalkOut> list = talkService.getTalk(receiveId);
		return success(list);
	}
	
	@ApiOperation(value="发私信", notes="" )
	@RequestMapping(value="/sendTalk",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> sendTalk(
			@RequestBody TalkIn in
			){
		return success(talkService.sendTalk(in.toTalk()));
	}
	@ApiOperation(value="阅读私信修改状态", notes="" )
	@RequestMapping(value="/readTalk",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> readTalk(
			@ApiParam(value="发消息人id") @RequestParam(value="sendId") Long sendId,
			@ApiParam(value="读取人、接收人id") @RequestParam(value="receiveId") Long receiveId
			){
		return success(talkService.readTalk(sendId, receiveId));
	}
}
