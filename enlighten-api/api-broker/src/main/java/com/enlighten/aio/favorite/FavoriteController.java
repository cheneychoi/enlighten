package com.enlighten.aio.favorite;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.enlighten.aio.db.model.Favorite;
import com.enlighten.aio.fav.service.FavoriteService;
import com.x.share.api.controller.BaseController;
import com.x.share.db.model.Pagination;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
@Controller
@RequestMapping(value="/favorite")
public class FavoriteController extends BaseController{
	@Resource
	FavoriteService favoriteService;
	
	@ApiOperation(value="收藏群组列表", notes="" )
	@RequestMapping(value="/getFavorit",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getFavorit(
			@ApiParam("用户id") @RequestParam(value="id") Long userId, 
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize){
		Pagination<Favorite> favorit = favoriteService.getFavorit(userId, pageNo, pageSize);
		return success(favorit);
	}
	
	@ApiOperation(value="点击群组添加收藏", notes="" )
	@RequestMapping(value="/insertFavorit",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> insertFavorit(
			@ApiParam("userId") @RequestParam(value="userId")Long userId,
			@ApiParam("groupId") @RequestParam(value="groupId")Long groupId){
		return success(favoriteService.insertFavorit(userId, groupId));
	}
}
