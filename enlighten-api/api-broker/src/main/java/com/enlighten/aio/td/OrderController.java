package com.enlighten.aio.td;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.enlighten.aio.en.service.model.in.CatalogIn;
import com.enlighten.aio.td.service.OrderService;
import com.enlighten.aio.td.service.model.in.OrderIn;
import com.enlighten.aio.td.service.model.out.OrderOut;
import com.enlighten.aio.td.service.model.out.PayOut;
import com.x.share.api.controller.BaseController;
import com.x.share.db.model.Pagination;
import com.x.share.mid.exception.HelperException;
import com.x.share.mid.model.UToken;
import com.x.share.mid.prop.ShareProperties;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "订单接口")
@Controller
@RequestMapping("/td")
public class OrderController extends BaseController {

	@Resource
	ShareProperties shareProps;

	@Resource
	OrderService orderService;

	@ApiOperation(value = "我的订单", notes = "", response = OrderOut.class)
	@RequestMapping(value = "orders", method = { RequestMethod.GET })
	public 	@ResponseBody Map<String, Object> listMyOrder(
			@ApiParam(value = "订单状态: 0 待支付，200 待发货， 300 待收货, 400 待评价, 399 退款，501 售后", required = false)@RequestParam(value = "status", required = false) String status,
			@ApiParam(value = "当前页,默认第1页", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "5") @RequestParam("ps") Integer pageSize,
			@ApiParam(value = "用户token(通过登录接口获得)") @RequestHeader("utk") String utk,
			@ApiParam(value = "应用id") @RequestHeader("appId") String appId,
			@ApiParam(value = "应用accessToken") @RequestHeader("atk") String atk) throws HelperException {
		UToken token = UToken.from(utk, shareProps.getAeskey());
		Pagination<OrderOut> out = orderService.listMyOrders(token.getId(), status, pageNo, pageSize);
		return success(out);
	}

	@ApiOperation(value = "创建订单", notes = "(快速购买)创建订单", response = OrderOut.class)
	@RequestMapping(value = "orders/quick/create", method = { RequestMethod.POST })
	public @ResponseBody Map<String, Object> createOrder(
			@RequestBody CatalogIn in,
			@ApiParam(value = "用户token(通过登录接口获得)") @RequestHeader("utk") String utk,
			@ApiParam(value = "应用id") @RequestHeader("appId") String appId,
			@ApiParam(value = "应用accessToken") @RequestHeader("atk") String atk) throws HelperException {
//		OrderOut out = orderService.createOrder(in, token.getId());
		return success();
	}
	
	@ApiOperation(value = "删除订单", response = Void.class)
	@RequestMapping(value = "orders/{id}/remove", method = { RequestMethod.PUT })
	public @ResponseBody Map<String, Object> removeOrder(@ApiParam(value = "订单号") @PathVariable("id") String orderId,
			@ApiParam(value = "用户token(通过登录接口获得)") @RequestHeader("utk") String utk,
			@ApiParam(value = "应用id") @RequestHeader("appId") String appId,
			@ApiParam(value = "应用accessToken") @RequestHeader("atk") String atk) throws HelperException {
		UToken token = UToken.from(utk, shareProps.getAeskey());
		Integer out = orderService.removeOrder(orderId, token.getId());
		return success(out);
	}

	@ApiOperation(value = "订单详情", response = OrderOut.class)
	@RequestMapping(value = "orders/{id}", method = { RequestMethod.GET })
	public @ResponseBody Map<String, Object> getOrder(@ApiParam(value = "订单号") @PathVariable("id") String orderId,
			@ApiParam(value = "用户token(通过登录接口获得)") @RequestHeader("utk") String utk,
			@ApiParam(value = "应用id") @RequestHeader("appId") String appId,
			@ApiParam(value = "应用accessToken") @RequestHeader("atk") String atk) throws HelperException {
		UToken.from(utk, shareProps.getAeskey());
		OrderOut out = orderService.getOrder(orderId);
		return success(out);
	}
	
	@ApiOperation(value = "创建订单, 并生成微信支付二维码(订单)")
	@RequestMapping(value = "/td/orders/wxcode2", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveOrder(@RequestBody OrderIn in, @RequestHeader("utk") String tk,
			@ApiParam(value = "应用id") @RequestHeader("appId") String appId,
			@ApiParam(value = "应用accessToken") @RequestHeader("atk") String atk) throws HelperException {
		UToken token = UToken.from(tk, shareProps.getAeskey());
		in.setUserId(token.getId());
		PayOut out = orderService.createOrderWithWxQrCode2(in);
		return success(out);
	}

	@ApiOperation(value = "创建订单,并调用支付宝电脑网站支付")
	@RequestMapping(value = "/alipay/page", method = RequestMethod.GET)
	public void saveOrder(
			@RequestParam("utk") String utk,
			@RequestParam("catalogId") Long catalogId,
			@RequestParam("rUrl") String returnUrl,
			@RequestParam("quantity") Integer quantity,
			@ApiParam(value = "应用id") @RequestHeader("appId") String appId,
			@ApiParam(value = "应用accessToken") @RequestHeader("atk") String atk,
			HttpServletResponse response) throws IOException, HelperException {
		UToken token = UToken.from(utk, shareProps.getAeskey());
		OrderIn in = new OrderIn();
		in.setCatalogId(catalogId);
		in.setUserId(token.getId());
		in.setPayMethod("ALI_PAY");
		in.setReturnUrl(returnUrl);
		in.setQuantity(quantity);
		String rlt = orderService.createOrderWithAliPagePay(in);
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter writer = response.getWriter();
		writer.write(rlt);
		writer.flush();
		writer.close();
	}
}

