package com.enlighten.aio.back;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.enlighten.aio.db.model.User;
import com.enlighten.aio.user.service.UserService;
import com.enlighten.aio.user.service.model.in.UserAddIn;
import com.x.share.api.controller.BaseController;
import com.x.share.db.model.Pagination;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value="/enlighten/back")
public class UserBackController extends BaseController{
	@Resource
	UserService userService;
	
	@ApiOperation(value="用户列表", notes="" )
	@RequestMapping(value="/getAllUser",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getAllUser(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize){
		Pagination<User> user = userService.getAllUser(pageNo, pageSize);
		return success(user);
	}
	@ApiOperation(value="添加用户", notes="" )
	@RequestMapping(value="/insertUser",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> insertUser(@RequestBody UserAddIn u){
		return success(userService.insertUser(u.toUser()));
	}
	@ApiOperation(value="编辑用户", notes="" )
	@RequestMapping(value="/editUser",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> editUser(@RequestBody User u){
		return success(userService.editUser(u));
	}
	@ApiOperation(value="删除用户", notes="" )
	@RequestMapping(value="/delUser",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> delUser(@ApiParam(value="id") @RequestParam(value="id") Long id){
		return success(userService.delUser(id));
	}
	@ApiOperation(value="编辑用户代入数据", notes="" )
	@RequestMapping(value="/getUserById",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getUserById(@ApiParam(value="id") @RequestParam(value="id") Long id){
		return success(userService.getUserById(id));
	}
}
