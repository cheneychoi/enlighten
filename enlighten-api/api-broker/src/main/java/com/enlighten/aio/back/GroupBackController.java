package com.enlighten.aio.back;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.enlighten.aio.db.model.Group;
import com.enlighten.aio.db.model.GroupWithBLOBs;
import com.enlighten.aio.group.service.GroupService;
import com.enlighten.aio.group.service.model.GroupsIn;
import com.x.share.api.controller.BaseController;
import com.x.share.db.model.Pagination;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
@Controller
@RequestMapping(value="/enlighten/back")
public class GroupBackController extends BaseController{
	@Resource
	GroupService groupService;
	
	@ApiOperation(value="后台群组列表", notes="" )
	@RequestMapping(value="/getGroup",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getGroup(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize){
		Pagination<Group> group = groupService.getGroup(pageNo, pageSize);
		return success(group);
	}
	
	
	@ApiOperation(value="后台同意申请", notes="" )
	@RequestMapping(value="/agreeGroup",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> agreeGroup(
			@ApiParam(value="群组id") @RequestParam(value="id") Long id,
			@ApiParam(value="是否同意") @RequestParam(value="isAgree",required=false) Integer isAgree){
		return success(groupService.agreeGroup(id, isAgree));
	}
	
	@ApiOperation(value="后台删除群组", notes="" )
	@RequestMapping(value="/delGroup",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> delGroup(
			@ApiParam(value="群组id") @RequestParam(value="id") Long id){
		return success(groupService.delGroup(id));
	}
	
	@ApiOperation(value="编辑群组", notes="" )
	@RequestMapping(value="/editGroup",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> editGroup(
			@RequestBody GroupWithBLOBs g){
		return success(groupService.editGroup(g));
	}
	
	@ApiOperation(value="后台拒绝申请群组", notes="" )
	@RequestMapping(value="/RefuseGroup",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> RefuseGroup(
			@ApiParam(value="群组id") @RequestParam(value="id") Long id){
		return success(groupService.RefuseGroup(id));
	}
	
	@ApiOperation(value="后台添加群组", notes="" )
	@RequestMapping(value="/addGroup",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> addGroup(
			@RequestBody GroupsIn in){
		return success(groupService.addGroup(in.toGroup()));
	}
}
