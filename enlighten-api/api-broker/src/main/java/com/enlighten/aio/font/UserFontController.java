package com.enlighten.aio.font;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.enlighten.aio.user.service.UserService;
import com.enlighten.aio.user.service.model.in.RegisterIn;
import com.enlighten.aio.user.service.model.in.UserIn;
import com.x.share.api.controller.BaseController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
@Controller
@RequestMapping(value="/enlighten/font")
public class UserFontController extends BaseController{
	@Resource
	UserService userService;
	
	@ApiOperation(value="更新用户信息", notes="" )
	@RequestMapping(value="/updateUser",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> updateUser(
			@RequestBody UserIn u,List<MultipartFile> files){
		return success(userService.updateUser(u.toUser(),files));
	}
	
	@ApiOperation(value="注册用户", notes="" )
	@RequestMapping(value="/RegisterUser",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> RegisterUser(
			@RequestBody RegisterIn in){
		return success(userService.RegisterUser(in.toUser()));
	}
	@ApiOperation(value = "管理员登陆验证码", notes = "")
	@RequestMapping(value = "/getCode", method = { RequestMethod.GET })
	@ResponseBody
	public Map<String, Object> getCode(HttpServletRequest request) {
		return success(userService.getCode(request));
	}
	
	@ApiOperation(value = "管理员登陆匹配", notes = "")
	@RequestMapping(value = "/login", method = { RequestMethod.POST })
	@ResponseBody
	public Map<String, Object> matchAdmin(@ApiParam("用户名") @RequestParam(value = "nickName") String nickName,
			@ApiParam("密码") @RequestParam(value = "password") String password,@ApiParam("验证码") @RequestParam(value = "code") String code,
			HttpServletRequest request) {
		return success(userService.login(nickName, password, code, request));
	}
	
	@ApiOperation(value = "个人信息", notes = "")
	@RequestMapping(value = "/getUserInformation", method = { RequestMethod.GET })
	@ResponseBody
	public Map<String, Object> getUserInformation(
			@ApiParam(value="用户id") @RequestParam(value="id") Long id) {
		return success(userService.getUserInformation(id));
	}
}
