package com.enlighten.aio.td;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.enlighten.aio.td.service.AliPayService;
import com.x.share.mid.exception.HelperException;
import com.x.share.mid.utils.AliPayUtils;

import io.swagger.annotations.ApiOperation;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Henry
 * @since 2018-07-28
 */
@RestController
@RequestMapping("/td/alipay")
public class AlipayController {
	Logger logger = LoggerFactory.getLogger(AlipayController.class);	
	
	@Resource
	AliPayService aliPayService;
	
	@ApiOperation(value = "支付宝支付异步通知消息")
	@RequestMapping(value = "notify", method = { RequestMethod.POST })
	public void alipayNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, String> notifyMap = AliPayUtils.notifyMap(request.getParameterMap());
		PrintWriter writer = response.getWriter();
		try {
			aliPayService.alipayNotify(notifyMap);
			writer.println("success");
		} catch (HelperException e) {
			logger.warn(">>>> helper exception: {}", e.getMessage());
			writer.println("fail");
		}
		writer.flush();
		writer.close();
	}
}