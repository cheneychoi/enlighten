package com.enlighten.aio.font;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.enlighten.aio.db.model.Friend;
import com.enlighten.aio.db.model.out.FriendOut;
import com.enlighten.aio.friend.service.FriendService;
import com.x.share.api.controller.BaseController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value="/enlighten/font")
public class FriendController extends BaseController{
	@Resource
	FriendService friendService;
	
	@ApiOperation(value="用户查看所有好友", notes="" )
	@RequestMapping(value="/getAllMyFriend",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getAllMyFriend(
			@ApiParam(value="用户id") @RequestParam(value="userId") Long userId){
		List<FriendOut> list = friendService.getAllFriend(userId);
		return success(list);
	}
	@ApiOperation(value="用户查看所有关注我的好友", notes="" )
	@RequestMapping(value="/getAllAttentionMy",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getAllAttentionMy(
			@ApiParam(value="用户id") @RequestParam(value="userId") Long userId){
		List<FriendOut> list = friendService.getAllAttentionMy(userId);
		return success(list);
	}
	@ApiOperation(value="用户查看所有我关注的好友", notes="" )
	@RequestMapping(value="/getAllMyAttention",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getAllMyAttention(
			@ApiParam(value="用户id") @RequestParam(value="userId") Long userId){
		List<FriendOut> list = friendService.getAllMyAttention(userId);
		return success(list);
	}
	@ApiOperation(value="用户加好友", notes="" )
	@RequestMapping(value="/insertFriend",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> insertFriend(
			@ApiParam(value="用户id") @RequestParam(value="userId") Long userId,
			@ApiParam(value="好友id") @RequestParam(value="friendId") Long friendId){
		return success(friendService.insertFriend(userId, friendId));
	}
	@ApiOperation(value="用户是否同意加好友申请", notes="" )
	@RequestMapping(value="/AgreeFriendApplication",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> AgreeFriendApplication(
			@ApiParam(value="用户id") @RequestParam(value="userId") Long userId,
			@ApiParam(value="好友id") @RequestParam(value="friendId") Long friendId,
			@ApiParam(value="是否同意") @RequestParam(value="isFriend") Integer isFriend){
		return success(friendService.AgreeFriendApplication(userId, friendId, isFriend));
	}
	@ApiOperation(value="关注好友", notes="" )
	@RequestMapping(value="/AttentionFriend",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> AttentionFriend(
			@ApiParam(value="用户id") @RequestParam(value="userId") Long userId,
			@ApiParam(value="好友id") @RequestParam(value="friendId") Long friendId){
		return success(friendService.AttentionFriend(userId, friendId));
	}
	@ApiOperation(value="用户登陆后调用 查看好友请求", notes="" )
	@RequestMapping(value="/getFriendAsk",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getFriendAsk(
			@ApiParam(value="用户id") @RequestParam(value="userId") Long userId){
		List<Friend> list = friendService.getFriendAsk(userId);
		return success(list);
	}
}
