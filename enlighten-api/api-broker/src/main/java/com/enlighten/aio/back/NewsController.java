package com.enlighten.aio.back;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.enlighten.aio.db.model.NewsWithBLOBs;
import com.enlighten.aio.news.service.NewsService;
import com.x.share.api.controller.BaseController;
import com.x.share.db.model.Pagination;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value="/enlighten/back")
public class NewsController extends BaseController{
	@Resource
	NewsService newsService;
	
	@ApiOperation(value="新闻列表", notes="" )
	@RequestMapping(value="/getAllNews",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getAllNews(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize){
		Pagination<NewsWithBLOBs> news = newsService.getAllNews(pageNo, pageSize);
		return success(news);
	}
	
	@ApiOperation(value="添加新闻",notes="")
	@RequestMapping(value="/insertNews",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> insertNews(@RequestBody NewsWithBLOBs nb){
		return success(newsService.insertNews(nb));
	}
	
	@ApiOperation(value="编辑新闻",notes="")
	@RequestMapping(value="/editNews",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> editNews(@RequestBody NewsWithBLOBs nb){
		return success(newsService.editNews(nb));
	}
	
	@ApiOperation(value="删除新闻",notes="")
	@RequestMapping(value="/delNews",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> delNews(@ApiParam(value="id") @RequestParam(value="id") Long id){
		return success(newsService.delNews(id));
	}
	
	@ApiOperation(value="后台编辑新闻带入数据",notes="")
	@RequestMapping(value="/getNewsById",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getNewsById(@ApiParam(value="id") @RequestParam(value="id") Long id){
		return success(newsService.getNewsById(id));
	}
}
