package com.enlighten.aio.back;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.enlighten.aio.db.model.out.InspectOut;
import com.enlighten.aio.ins.service.InspectService;
import com.enlighten.aio.pic.service.PictureService;
import com.x.share.api.controller.BaseController;
import com.x.share.db.model.Pagination;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value="/enlighten/back")
public class InspectBackController extends BaseController{
	@Resource
	InspectService inspectService;
	
	@Resource
	PictureService pictureService; 
	
	@ApiOperation(value="后台看录入检测数据", notes="" )
	@RequestMapping(value="/getInspect",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getAllNews(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize){
		Pagination<InspectOut> inspect = inspectService.getInspect(pageNo, pageSize);
		return success(inspect);
	}
	
	@ApiOperation(value = "后台上传检测报告图", notes = "上传检测报告图")
	@RequestMapping(value = "/uploadtReport", method = { RequestMethod.POST })
	@ResponseBody
	public Map<String, Object> uploadtDocument(List<MultipartFile> files) {
		for (MultipartFile file : files) {
			pictureService.uploadPicture(file, 0L, 5);
		}
		return success();
	}
}
