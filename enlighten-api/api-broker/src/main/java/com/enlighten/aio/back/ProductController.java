package com.enlighten.aio.back;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.enlighten.aio.db.model.Product;
import com.enlighten.aio.pro.service.ProductService;
import com.x.share.api.controller.BaseController;
import com.x.share.db.model.Pagination;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value="/enlighten/back/")
public class ProductController extends BaseController{
	@Resource
	ProductService productService;
	
	@ApiOperation(value="产品列表", notes="" )
	@RequestMapping(value="/getAllProduct",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getAllProduct(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize){
		Pagination<Product> product = productService.getAllProduct(pageNo, pageSize);
		return success(product);
	}
	
	@ApiOperation(value="添加产品", notes="" )
	@RequestMapping(value="/insertProduct",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> insertProduct(
			@RequestBody Product p){
		return success(productService.insertProduct(p));
	}
	
	@ApiOperation(value="编辑产品", notes="" )
	@RequestMapping(value="/editProduct",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> editProduct(
			@RequestBody Product p){
		return success(productService.editProduct(p));
	}
	
	@ApiOperation(value="删除产品", notes="" )
	@RequestMapping(value="/delProduct",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> delProduct(
			@ApiParam(value="产品id") @RequestParam(value="id") Long id){
		return success(productService.delProduct(id));
	}
	
	@ApiOperation(value="编辑产品带入数据", notes="" )
	@RequestMapping(value="/getProductById",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getProductById(
			@ApiParam(value="产品id") @RequestParam(value="id") Long id){
		return success(productService.getProductById(id));
	}
}
