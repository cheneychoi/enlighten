package com.enlighten.aio.font;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.enlighten.aio.db.model.CommentWithBLOBs;
import com.enlighten.aio.db.model.Group;
import com.enlighten.aio.db.model.out.GroupOut;
import com.enlighten.aio.group.service.GroupService;
import com.enlighten.aio.group.service.model.CommentIn;
import com.enlighten.aio.group.service.model.GroupIn;
import com.x.share.api.controller.BaseController;
import com.x.share.db.model.Pagination;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value="/enlighten/font")
public class GroupController extends BaseController{
	@Resource
	GroupService groupService;
	@ApiOperation(value="申请创建群组", notes="" )
	@RequestMapping(value="/insertGroup",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> insertGroup(@RequestBody GroupIn g){
		return success(groupService.insertGroup(g.toGroup()));
	}
	@ApiOperation(value="评论群组", notes="" )
	@RequestMapping(value="/commentGroup",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> commentGroup(@RequestBody CommentIn in){
		return success(groupService.commentGroup(in.toComment()));
	}
	@ApiOperation(value="评回复论群组", notes="" )
	@RequestMapping(value="/ReplyComment",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> ReplyComment(@RequestBody CommentIn in){
		return success(groupService.ReplyComment(in.toComment()));
	}
	@ApiOperation(value="前台群组列表", notes="" )
	@RequestMapping(value="/getAllGroup",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getAllGroup(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize){
		Pagination<Group> group = groupService.getAllGroup(pageNo, pageSize);
		return success(group);
	}
	@ApiOperation(value="点进群组显示评论", notes="" )
	@RequestMapping(value="/getComment",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getComment(
			@ApiParam(value ="群组id" ) @RequestParam("groupId")Long groupId){
		List<CommentWithBLOBs> list = groupService.getComment(groupId);
		return success(list);
	}
	
	@ApiOperation(value="点击群组显示信息", notes="" )
	@RequestMapping(value="/getGroupById",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getGroupById(
			@ApiParam(value ="群组id" ) @RequestParam("id")Long id){
		GroupOut groupOut = groupService.getGroupById(id);
		return success(groupOut);
	}
}
