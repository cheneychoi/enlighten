package com.enlighten.aio.back;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.enlighten.aio.db.model.Picture;
import com.enlighten.aio.pic.service.PictureService;
import com.x.share.api.controller.BaseController;
import com.x.share.db.model.Pagination;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value="/enlighten/back")
public class PictureBackController extends BaseController{
	@Resource
	PictureService pictureService;
 	
	@ApiOperation(value="图片列表", notes="" )
	@RequestMapping(value="/getPic",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getPic(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize){
		Pagination<Picture> pic = pictureService.getPic(pageNo, pageSize);
		return success(pic);
	}
	
	@ApiOperation(value = "上传Banner图", notes = "上传Banner图")
	@RequestMapping(value = "/uploadBanner", method = { RequestMethod.POST })
	@ResponseBody
//	public Map<String, Object> uploadBanner(HttpServletRequest request) {
	/*	Integer PictureType = Integer.parseInt(request.getParameter("PictureType"));
	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	List<MultipartFile> files = multipartRequest.getFiles("file");
	MultipartFile file = files.get(0);
	 System.out.println("file==============="+files);
	pictureService.uploadPic(file, PictureType);*/
		public Map<String, Object> uploadBanner(MultipartFile file,
				@ApiParam(value="图片类型") @RequestParam(value="picType")Integer picType) {
		pictureService.uploadPicture(file, 0L, picType);
		return success();
	}
	
	@ApiOperation(value="删除图片", notes="" )
	@RequestMapping(value="/delPic",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> delPic(
			@ApiParam(value="图片id") @RequestParam(value="id") Long id){
		return success(pictureService.delPic(id));
	}
	@ApiOperation(value="编辑图片", notes="" )
	@RequestMapping(value="/editPic",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> editPic(
			@RequestBody Picture p){
		return success(pictureService.editPic(p));
	}
	
	@ApiOperation(value="编辑图片带入数据", notes="" )
	@RequestMapping(value="/getPicById",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getPicById(
			@ApiParam(value="图片id") @RequestParam(value="id") Long id){
		Picture pic = pictureService.getPicById(id);
		return success(pic);
	}
}
