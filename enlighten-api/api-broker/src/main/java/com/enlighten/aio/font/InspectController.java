package com.enlighten.aio.font;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.enlighten.aio.db.model.Inspect;
import com.enlighten.aio.ins.service.InspectService;
import com.x.share.api.controller.BaseController;

import io.swagger.annotations.ApiOperation;
@Controller
@RequestMapping(value="/enlighten/font")
public class InspectController extends BaseController{
	@Resource
	InspectService inspectService;
	
	@ApiOperation(value="前台录入数据", notes="" )
	@RequestMapping(value="/EntryInspect",method= {RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> EntryInspect(@RequestBody Inspect i){
		inspectService.EntryInspect(i);
		return success();
	}
}
