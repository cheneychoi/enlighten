package com.enlighten.aio.font;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.enlighten.aio.db.model.Picture;
import com.enlighten.aio.pic.service.PictureService;
import com.x.share.api.controller.BaseController;

import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("/enlighten/font")
public class PictureFontController extends BaseController{
	@Resource
	PictureService pictureService;
	
	@ApiOperation(value="前台首页轮播图", notes="" )
	@RequestMapping(value="/getHomePic",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getHomePic(){
		List<Picture> list = pictureService.getHomePic();
		return success(list);
	}
	
	
	@ApiOperation(value="前台底部图", notes="" )
	@RequestMapping(value="/getBottomPic",method= {RequestMethod.GET})
	@ResponseBody
	public Map<String, Object> getBottomPic(){
		List<Picture> list = pictureService.getBottomPic();
		return success(list);
	}
	
}
