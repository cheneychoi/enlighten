/*package com.enlighten.aio.us;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.enlighten.aio.us.service.UsUserService;
import com.enlighten.aio.us.service.model.in.LoginIn;
import com.enlighten.aio.us.service.model.out.LoginOut;
import com.x.share.api.controller.BaseController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/us")
public class UserController extends BaseController{
	
	@Resource
	UsUserService usUserService;
	*//**
	 * @description : 手机号+验证码找回密码 <br>
	 *              ---------------------------------
	 * @author : Henry
	 * @since : Create in 2017-12-26
	 *//*
	@ApiOperation(value = "手机号+验证码找回密码", notes = "请使用检查手机号的方式获取验证码,成功后返回用户token等凭证信息", response = LoginOut.class)
	@RequestMapping(value = "/usUsers/forgot", method = RequestMethod.POST)
	public Map<String, Object> forgot(@RequestBody LoginIn param,
			@ApiParam(value = "应用id") @RequestHeader("appId") String appId,
			@ApiParam(value = "应用accessToken") @RequestHeader("atk") String atk) {
		return success(usUserService.login(param));
	}

	*//**
	 * @description : 用户注册(UsUser) <br>
	 *              ---------------------------------
	 * @author : Henry
	 * @since : Create in 2017-12-26
	 *//*
	@ApiOperation(value = "用户注册", notes = "注册成功后返回用户token等凭证信息", response = LoginOut.class)
	@RequestMapping(value = "/usUsers/register", method = RequestMethod.POST)
	public Map<String, Object> register(@RequestBody RegisterIn param,
			@ApiParam(value = "应用id") @RequestHeader("appId") String appId,
			@ApiParam(value = "应用accessToken") @RequestHeader("atk") String atk) {
		return success(usUserService.register(param));
	}
}
*/